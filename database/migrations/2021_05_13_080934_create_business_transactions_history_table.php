<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessTransactionsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_transactions_history', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->default(0);
            $table->float('transaction_amount')->nullable();
            $table->text('tansaction_details')->nullable();
            $table->integer('request_type')->comment('1 for subscription')->nullable();
            $table->integer('request_id')->default(0)->comment('user_subscription_id');
            $table->integer('is_active')->default(0)->comment('0 for pending, 1 for completed, 2 for processing,3 for others');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_transactions_history');
    }
}
