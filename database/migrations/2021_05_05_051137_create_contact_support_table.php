<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactSupportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_support', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('buyer_id')->comment('buyer id as foreign key on users table');
            $table->foreign('buyer_id')->references('id')->on('users');
            $table->unsignedBigInteger('reason_id')->comment('reason id as foreign key on reasons table');
            $table->foreign('reason_id')->references('id')->on('reasons');
            $table->text('description');
            $table->integer('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_support');
    }
}
