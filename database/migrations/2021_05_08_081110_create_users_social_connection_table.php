<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersSocialConnectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_social_connection', function (Blueprint $table) {
            $table->id();
            $table->integer('influencer_id')->nullable();
            $table->integer('social_network_type')->nullable()->comment('1 for facebook,2 for twitter, 3 for instagram, 4 for Youtube');
            $table->string('social_id')->nullable();
            $table->text('request_token')->nullable();
            $table->text('details')->nullable()->change();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_social_connection');
    }
}
