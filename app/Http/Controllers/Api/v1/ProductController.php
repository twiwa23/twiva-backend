<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\PushNotificationController;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductStock;
use App\Models\ProductColor;
use App\Models\ProductSize;
use App\Models\ProductCategory;
use App\Models\ProductReviews;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Calculation\Category;
use App\Models\ProductSpecifications;
// use Illuminate\Support\Collection;
// use Maatwebsite\Excel\Concerns\ToCollection;
// use Maatwebsite\Excel\Concerns\WithStartRow;
// use Maatwebsite\Excel\Facades\Excel;
// use Illuminate\Support\Facades\Storage;
use DB;
use App\User;
use App\Models\UserDetail;
use App\Models\OrderDetails;
use App\Models\ShippingAddress;
use App\Models\Order;
use App\Models\BuyerDetails;
use App\Models\InfluencerShopProducts;
use App\Models\InfluencerShopProductImages;
use App\Models\UserSubscription;
use App\Models\Notification;
use Mail;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Hash;
use App\Services\UserService;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;



class ProductController extends ApiController{

    public $successStatus = 200;
    public $unauth = 401;
    public $nodata = 404;
    public $badRequest = 422;
    public $validateInputs= 400;
    /**
     * product controller
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function __construct()
    {
        $this->user = Auth::user();
    }

     /**
     * product controller
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function index(Request $request)
    {

    }

    public function deleteData()
    {
        //DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // DB::table('product_image')->truncate();
        // DB::table('product_category')->truncate();
        // DB::table('product_colors')->truncate();
        // DB::table('product_size')->truncate();
        // DB::table('product_specifications')->truncate();
        // DB::table('product_stock')->truncate();
        // DB::table('influencer_shop_product_images')->truncate();
        // DB::table('influencer_shop_product')->truncate();
        // DB::table('influencer_shop')->truncate();
        // DB::table('products')->truncate();
        //DB::table('categories')->truncate();
        //DB::table('orders_details')->truncate();
        //DB::table('orders')->truncate();
        //DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return 'success';
    }

    /**
     * product controller
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function addProduct(Request $request){
        try{
            $free_product_limit = 10;
            $date = \Carbon\Carbon::now();
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'price' => 'required',
                'category' => 'required',
                'stock' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()], 404);
            }
            $input = $request->all();
            $userdetails = Auth::user();
            if(!$userdetails){
                $error_message = array('message'=>"Unauthorized",'status'=>false);
                return response()->json($error_message,401);
            }
            // $user_subscription = UserSubscription::with('subscriptionDetails')->where('user_id',$userdetails->id)->get();
            // $product_limit = 10;   // static data for existing users
            // $purchased_product_limit = "";
            // if($user_subscription){
            //     foreach($user_subscription as $value){
            //         if($value->subscriptionDetails->type !== 1){
            //             $purchased_product_limit = $value->subscriptionDetails->max_products;
            //         }else{
            //             $free_product_limit = $value->subscriptionDetails->max_products;
            //         }
            //     }
            // }
            // if($purchased_product_limit !== ""){
            //     $product_limit = $purchased_product_limit;
            // }else{
            //     $product_limit = $free_product_limit;
            // }
            $user_product_count = Product::where('user_id',$userdetails->id)->count();
            $user_old_subscription = UserSubscription::with('subscriptionDetails')->where('user_id',$userdetails->id)->where('is_expiry',0)->first();
            $product_limit = $user_old_subscription->subscriptionDetails->max_products;
            $user_old_end_date = $user_old_subscription->end_date;
            $date1 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user_old_end_date);
            $today = \Carbon\Carbon::now();
            $date2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $today);
            if($date1->gt($date2) && ($user_product_count < $product_limit && $user_product_count !== $product_limit)){
                $input['status'] = 1;
            }else{
                $input['status'] = 0;
            }
            $input['user_id']  =$userdetails['id'];
            $input['color']   = $input['color_ids'];
            $input['size']    = $input['size_ids'];
            $product = Product::create($input);
            if(isset($input['product_images'])){
                $productImgArray = array();
                $productImages = $input['product_images'];
                foreach ($productImages as $productImage) {
                    $productImgArray[]= array("product_id" => $product->id,'image'=>$productImage['image'],'is_cover_pic'=>$productImage['is_cover_pic'],'created_at'=>$date,'updated_at'=>$date);
                }
                if(count($productImgArray)>0){
                    ProductImage::insert($productImgArray);
                }
            }
            return response()->json(['status'=>true,'message'=>'Product added successfully'] ,200);

            /*
            $productColors=explode(",",$input['color_ids']);

            for($i=0;$i<count($productColors);$i++){
		        $colors['product_id'] = $product->id;
        	    $colors['color_id'] = $productColors[$i];
            	$colors['status'] = 1;
            	$stockData = ProductColor::create($colors);
            }

	        $productSizes=explode(",",$input['size_ids']);
            for($i=0;$i<count($productSizes);$i++){
                    $sizes['product_id'] = $product->id;
                    $sizes['size_id'] = $productSizes[$i];
                    $sizes['status'] = 1;
                    $stockData = ProductSize::create($sizes);
            }

	        $productCategory['product_id']=$product->id;
	        $productCategory['category_id']=$product['category_id'];
	        $stockData = ProductCategory::create($sizes);	 */

	        /*
            if(isset($input['specification'])){
	            $productSpecifications = explode(",",$input['specification']);
                for($i=0;$i<count($productSpecifications);$i++){
                    $sizes['product_id'] = $product->id;
                    $sizes['title'] = $productSpecifications[$i];
                    $sizes['status'] = 1;
                    $stockData = ProductSpecifications::create($sizes);
                }
            }
            */

        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }

    }

    public function addProductCopy(Request $request){
        try{

            $free_product_limit = 10;
            $date = \Carbon\Carbon::now();
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'price' => 'required',
                'category' => 'required',
                'sub_category' => 'required',
                'stock' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()], 404);
            }
            $input = $request->all();
            $userdetails = Auth::user();
            if(!$userdetails){
                $error_message = array('message'=>"Unauthorized",'status'=>false);
                return response()->json($error_message,401);
            }
            $user_subscription = UserSubscription::with('subscriptionDetails')->where('user_id',$userdetails->id)->get();
            $product_limit = 10;   // static data for existing users
            $purchased_product_limit = "";
            if($user_subscription){
                foreach($user_subscription as $value){
                    if($value->subscriptionDetails->type !== 1){
                        $purchased_product_limit = $value->subscriptionDetails->max_products;
                    }else{
                        $free_product_limit = $value->subscriptionDetails->max_products;
                    }
                }
            }
            if($purchased_product_limit !== ""){
                $product_limit = $purchased_product_limit;
            }else{
                $product_limit = $free_product_limit;
            }
            $user_product_count = Product::where('user_id',$userdetails->id)->count();
            if($user_product_count < $product_limit && $user_product_count !== $product_limit){
                $input['user_id']  =$userdetails['id'];
                $input['color']   = $input['color_ids'];
                $input['size']    = $input['size_ids'];
                $input['status'] = 1;
                $product = Product::create($input);

                if(isset($input['product_images'])){
                    $productImgArray = array();
                    $productImages = $input['product_images'];
                    foreach ($productImages as $productImage) {
                        $productImgArray[]= array("product_id" => $product->id,'image'=>$productImage['image'],'is_cover_pic'=>$productImage['is_cover_pic'],'created_at'=>$date,'updated_at'=>$date);
                    }
                    if(count($productImgArray)>0){
                        ProductImage::insert($productImgArray);
                    }
                }
                return response()->json(['status'=>true,'message'=>'Product added successfully'] ,200);

            }else{
                return response(['status'=>false,"message"=>'You cannot add more products. Your product limit has exceeded'], 403);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }

    }


    public function uploadImage(Request $request){

        $response = array();
        $folderPath = storage_path();
	    $server_url = $_SERVER['HTTP_HOST'];
	    $input = $request->all();
        if($_FILES['image']){

            $avatar_name = $_FILES["image"]["name"];
            $avatar_tmp_name = $_FILES["image"]["tmp_name"];
            $error = $_FILES["image"]["error"];
            if($error > 0){
                $response = array(
                    "status" => "error",
                    "error" => true,
                    "message" => "Error uploading the file!"
                );
            }else{

                //$type = $request['type'];

                $tmpFilePath = $_FILES['image']['tmp_name'];
                $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                $newname = time().".".$ext;
                $thumbFilePath = $_SERVER['DOCUMENT_ROOT'].'/storage/images/thumb/' . $_FILES['image']['name'];
                $newFilePath = $_SERVER['DOCUMENT_ROOT'].'/storage/images/products/'. $newname = time().".".$ext;
                move_uploaded_file($tmpFilePath,$newFilePath);

                return response(['code'=>200,'image'=>$newname,"message"=>'stored successfully'], 200);
            }
        }else{
            return response(['code'=>404,"message"=>'No file sent'], 404);
        }
    }

    public function uploadImages(Request $request){

        $data = $request->all();

        $response = array();
        $folderPath = storage_path();
        $server_url = $_SERVER['HTTP_HOST'];
        $files = $_FILES['image'];
        $total = count($_FILES['image']['name']);
        $returnImages=[];
        for( $i=0 ; $i < $total ; $i++ ) {
	        $newname ='';
            $tmpFilePath = $_FILES['image']['tmp_name'][$i];
            $ext = pathinfo($_FILES['image']['name'][$i], PATHINFO_EXTENSION);
            $newname = rand().time().".".$ext;
            $newFilePath = $_SERVER['DOCUMENT_ROOT'].'/storage/images/products/' .  $newname;
            move_uploaded_file($tmpFilePath,$newFilePath);
            $returnImages[]=$newname;
        }
        if($files) {
            return response(['code'=>200,'images'=>$returnImages,"message"=>'stored successfully'], 200);
        }else{
            return response(['code'=>400,"message"=>'Error uploading file'], 400);
        }
    }

    public function addProductImages(Request $request){
         try{
            $input = $request->all();
            $name  = $input['name'];
            $title = $input['title'];
            $allImage = $input['image'];
            $uploadFolder = 'product';
            $rules = array(
                'name' => 'required|array',
                'title' => 'required|array',
                'image' => 'required|array',
                'product_id' => 'required'
            );
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                //$messages = $validator->messages();
                return response()->json(['error'=>$validator->errors()], 401);
            }
            $imageRules = array(
                'image' => 'image:jpeg,png,jpg,gif,svg|max:2048'
            );

            foreach($input['image'] as $key=>$image)
            {
                $imageValidate = array('image' => $image);

                $imageValidator = Validator::make($imageValidate, $imageRules);

                if ($imageValidator->fails()) {
                    //$messages = $imageValidator->messages();
                    return response()->json(['error'=>$imageValidator->errors()], 401);

                }else{
                    //if(array_key_exists($key,$allImage)){
                    //$image = $request->file($image);
                    $image_uploaded_path = $image->store($uploadFolder, 'public');
                    $inputSingle['user_id'] = Auth::id();
                    $inputSingle['image'] = $image_uploaded_path;
                    $inputSingle['name']  = array_key_exists($key, $name) ? $name[$key]  : NULL;
                    $inputSingle['title'] = array_key_exists($key, $title) ? $title[$key] : NULL;
            //        $productImage = ProductImage::create($inputSingle);
                    //}
                }
            }
            $success = 'Images has been uploaded successfully.';
            return response()->json(['success'=>$success], $this-> successStatus);
         } catch (\Exception $e) {
            return response()->json(['failure'=>500, 'error' => $e->getMessage()]);
         }
    }

    public function publishUnpublish(Request $request){
        try{
           $input = $request->all();
            $rules = array(
                'status' => 'required|integer',
                'product_id'=>'required'

            );
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->messages()->first()], 401);
            }
            $user_id = Auth::id();
            $product = Product::where('id',$input['product_id'])->where('user_id',$user_id)->first();
            $user_product_count = Product::where('user_id',$user_id)->where('status',1)->count();
            $user_old_subscription = UserSubscription::with('subscriptionDetails')->where('user_id',$user_id)->where('is_expiry',0)->first();
            $product_limit = $user_old_subscription->subscriptionDetails->max_products;
            $user_old_end_date = $user_old_subscription->end_date;
            $date1 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user_old_end_date);
            $today = \Carbon\Carbon::now();
            $date2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $today);
            if(!$date1->gt($date2)){
                return response()->json(['success'=>false,'message'=> 'Your Subscription has expired. You cannot publish/unpublish product'],400);
            }
            if($date1->gt($date2) && ($user_product_count > $product_limit || $user_product_count == $product_limit) && $input['status'] == 1){
                return response()->json(['success'=>false,'message'=> 'Your Product limit has reached'],400);
            }

            if($product ){
                if($input['status'] == 1 || $input['status'] == 0){
                    $product->status =  $input['status'];
                    $product->save();
                }else{
                    return response()->json(['success'=>false,'message'=> 'Only  0,1 values are allowed for status'],400);
                }
            }else{
                return response()->json(['success'=>false,'message'=> 'product not found'],400);
            }
            if($input['status'] == 1){
                $success = 'Product published successfully';
            }else{
                $success = 'Product unpublished successfully';
            }
            return response()->json(['success'=>true,'message'=> $success],200);
        } catch (\Exception $e) {
           return response()->json(['failure'=>500, 'error' => $e->getMessage()]);
        }
   }

    /*public function product_detail(Request $request,$id){

	      $input = $request->all();
	      $userdetails = Auth::user();
	      $product=Product::where('id','=',$request->id)->get();

          if(isset($product[0]->id)){
		      $images['short_image']=$product[0]->main_image;
		      $images['full_image']= "https://twiva-api.apparrant.com/storage/images/products/".$product[0]->main_image;
		      $product[0]->images=$images;
              $product_categories = DB::table('product_category')
                ->leftjoin("categories","categories.id","=","product_category.category_id")->where('product_category.product_id','=',$request->id)->get();

                $product[0]->categories = $product_categories;
                $ProductSpecifications = DB::table('product_specifications') ->where('product_specifications.product_id','=',$request->id)->get();
        		$product[0]->specifications = $ProductSpecifications;
        		$ProductStock= ProductStock::where('product_id','=',$request->id)->get();
        		$product->stocks=$ProductStock;
        		$ProductColor = DB::table('product_colors')
                        ->leftjoin("colors","colors.id","=","product_colors.color_id")
                        ->where('product_colors.product_id','=',$request->id)->get();
	           $product[0]->ProductColor= $ProductColor;

        		$ProductSize = DB::table('product_size')
                        ->leftjoin("sizes","sizes.id","=","product_size.size_id")
                        ->where('product_size.product_id','=',$request->id)
                        ->get();
		        $product[0]->sizes = $ProductSize;
	   }

        return response(['code'=>200,'details'=>$product,"message"=>'stored successfully'], 200);
    } */

    public function downloadProductSample(Request $request){
        try{
            $sampleProduct = url('importProduct/sample/product/products.xlsx');
            $success = 'Success.';
            return response()->json(['success'=>$success,'sample_url' => $sampleProduct], $this-> successStatus);
         } catch (\Exception $e) {
            return response()->json(['failure'=>500, 'error' => $e->getMessage()]);
         }
    }

    public function product_detail(Request $request,$id){
        try{
            $userdetails = Auth::user();
            if(!$userdetails){
                $error_message = array('message'=>"Unauthorized",'status'=>false);
                return response()->json($error_message,401);
            }
            $input = $request->all();
            $validator = Validator::make($input,[
                'limit' => 'numeric | nullable',
                'page'  => 'numeric | nullable',
            ]);

            if($validator->fails()) {
                $ret = array('success'=>0, 'message'=> $validator->messages()->first());
                return response()->json($ret);
            }
            $limit = $request->limit ? $request->limit : 10;
            $products =  Product::with('productImages')->where('id',$id)->first();
            if($products){
                $product_reviews_and_ratings = ProductReviews::with('buyerName:buyer_id,name')->where('product_id',$id)->orderBy('id','desc')->paginate($limit)->toArray();
                $products->product_reviews_and_ratings = $product_reviews_and_ratings['data'];
            }else{
                $products->product_reviews_and_ratings = [];
            }
            return response()->json(array('success'=>true,'message'=>"Product Details",'data'=>$products) ,200,[],JSON_NUMERIC_CHECK);

        }catch (\Exception $e) {
            return response()->json(['failure'=>500, 'error' => $e->getMessage()]);
        }
    }

    public function deleteProduct(Request $request,$id){
        try{
            $input = $request->all();
            $userdetails = Auth::user();
            if(!$userdetails){
                $error_message = array('message'=>"Unauthorized",'status'=>false);
                return response()->json($error_message,401);
            }
            $products =  Product::where('user_id',$userdetails->id)->where('id',$id)->first();
            if($products){
                $products->delete();
                return response()->json(array('success'=>true,'message'=>"Product successfully deleted") ,200,[],JSON_NUMERIC_CHECK);
            }
            return response()->json(array('success'=>false,'message'=>"Invalid Productid") ,404,[],JSON_NUMERIC_CHECK);

        }catch (\Exception $e) {
            return response()->json(['failure'=>500, 'error' => $e->getMessage()]);
        }
    }

    public function editProduct(Request $request,$id){
        try{

            $date = \Carbon\Carbon::now();

            $validator = Validator::make($request->all(), [
                'name'     => 'required',
                'price'    => 'required',
                'category' => 'required',
                'stock'    => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()], 404);
                //return response()->json(['status'=>$this->validateInputs,'error'=>$validator->errors()], $this->validateInputs);
            }

            $input = $request->all();
            $userdetails = Auth::user();

            if(!$userdetails){
                $error_message = array('message'=>"Unauthorized",'status'=>false);
                return response()->json($error_message,401);
            }

            $input = $request->all();
            $userdetails = Auth::user();
            $products =  Product::where('user_id',$userdetails->id)->where('id',$id)->first();
            if($products){

                $products->name   = $input['name'];
                $products->price  = $input['price'];
                $products->sub_category = $input['sub_category'];
                $products->category = $input['category'];
                $products->stock    = $input['stock'];
                $products->description   = $input['description'];
                $products->sku      = $input['sku'];
                $products->specification = $input['specification'];
                $products->color   = $input['color_ids'];
                $products->size    = $input['size_ids'];
                $products->save();

                /*$productColors=explode(",",$input['color_ids']);
                // delete before add
                ProductColor::where('product_id',$products->id)->delete();
                for($i=0;$i<count($productColors);$i++){
                    $colors['product_id'] = $products->id;
                    $colors['color_id'] = $productColors[$i];
                    $colors['status'] = 1;
                    $stockData = ProductColor::create($colors);
                } */

                /*$productSizes=explode(",",$input['size_ids']);
                // delete before add
                ProductSize::where('product_id',$products->id)->delete();
                for($i=0;$i<count($productSizes);$i++){
                        $sizes['product_id'] = $products->id;
                        $sizes['size_id'] = $productSizes[$i];
                        $sizes['status'] = 1;
                        $stockData = ProductSize::create($sizes);
                } */

                /*$productCategory['product_id']  = $products->id;
                $productCategory['category_id'] = $products->category_id;
                // delete before add
                ProductCategory::where('product_id',$products->id)->delete();
                $stockData = ProductCategory::create($sizes);   */

                /*if(isset($input['specification'])){
                    // delete before add
                    ProductSpecifications::where('product_id',$products->id)->delete();
                    $productSpecifications = explode(",",$input['specification']);
                    for($i=0;$i<count($productSpecifications);$i++){
                        $sizes['product_id'] = $products->id;
                        $sizes['title'] = $productSpecifications[$i];
                        $sizes['status'] = 1;
                        $stockData = ProductSpecifications::create($sizes);
                    }
                } */

                if(isset($input['product_images'])){
                    ProductImage::where('product_id',$products->id)->delete();
                    $productImgArray = array();
                    $productImages = $input['product_images'];
                    foreach ($productImages as $productImage) {
                        $productImgArray[]= array("product_id" => $products->id,'image'=>$productImage['image'],'is_cover_pic'=>$productImage['is_cover_pic'],'created_at'=>$date,'updated_at'=>$date);
                    }
                    if(count($productImgArray)>0){
                        ProductImage::insert($productImgArray);
                    }
                }

                return response()->json(array('success'=>true,'message'=>"Product successfully updated") ,200,[],JSON_NUMERIC_CHECK);
            }

            return response()->json(array('success'=>false,'message'=>"Invalid Productid") ,404,[],JSON_NUMERIC_CHECK);

        }catch (\Exception $e) {
            return response()->json(['failure'=>500, 'error' => $e->getMessage()]);
        }
    }

    public function fetchProducts(Request $request){
        try{
            $input = $request->all();
            $userdetails = Auth::user();

            if(!$userdetails){
                $error_message = array('message'=>"Unauthorized",'status'=>false);
                return response()->json($error_message,401);
            }

            $validator = Validator::make($input,[
                'limit' => 'numeric | nullable',
                'page'  => 'numeric | nullable',
            ]);

            if($validator->fails()) {
                $ret = array('success'=>0, 'message'=> $validator->messages()->first());
                return response()->json($ret);
            }
            $limit = $request->limit ? $request->limit : 10;
            $products = Product::where('user_id',$userdetails->id);
            if($request->has('categories') && $input['categories'] != ""){
                $categories = explode(',',$input['categories']);
                $products->whereIn('products.category',$categories);
            }
            if(($request->has('categories') && $input['categories'] != "") && ($request->has('sub_categories') && $input['sub_categories'] != "")){
                $sub_categories = explode(',',$input['sub_categories']);
                $products->whereRaw("FIND_IN_SET($sub_categories[0],products.sub_category)");
                foreach($sub_categories as $value){
                    $products->orWhereRaw("FIND_IN_SET($value,products.sub_category)");
                }
            }
            // if(($request->has('price_min') && $request->has('price_max')) && ($input['price_min'] !== "" && $input['price_max'] !== "")){
            //     $products->whereBetween('price',[$input['price_min'],$input['price_max']]);
            // }
            if($request->has('search') && $input['search'] !== ""){
                $products->where('name','like','%'.$input['search'].'%');
            }
            $products->with(['productImages']);
            if(($request->has('sort_by') && $input['sort_by'] !== "")){
                if($input['sort_by'] == 0){
                    $products->orderBy('price','asc');
                }else{
                    $products->orderBy('price','desc');
                }
            }else{
                $products->orderBy('id','desc');
            }
            $products = $products->paginate($limit)->toArray();

            return response()->json(array('success'=>true,'data'=>$products['data'],'current_page'=>$products['current_page'],'last_page'=>$products['last_page'],'total_results'=>$products['total'],'message'=>"Product Listing") ,200,[],JSON_NUMERIC_CHECK);

        }catch (\Exception $e) {
            return response()->json(['failure'=>500, 'error' => $e->getMessage()]);
         }
    }

    public function getOrderList(Request $request){
        try{
            $userdetails = Auth::user();
            if(!$userdetails){
                $error_message = array('message'=>"Unauthorized",'status'=>false);
                return response()->json($error_message,401);
            }
            $input = $request->all();
            $validator = Validator::make($input,[
                'limit' => 'numeric | nullable',
                'page'  => 'numeric | nullable',
            ]);

            if($validator->fails()) {
                $ret = array('status'=>false, 'message'=> $validator->messages()->first());
                return response()->json($ret);
            }
            $limit = $request->limit ? $request->limit : 10;
            $page = $request->page ? $request->page : 1;
            $orderList = OrderDetails::with('Order:id,order_status')->where('business_id',$userdetails->id);

            if($request->has('search') && $input['search'] !== ""){
                $orderList->where('order_id','like', '%'.$input['search'].'%')
                          ->orWhere(function ($query) use ($input) {
                                $query->whereHas('userInfo', function ($query) use ($input) {
                                    $query->where('email','like','%'.$input['search'].'%');
                                });
                            });
            }

            $orderList =  $orderList->with('userInfo')->whereRaw('id IN (select MAX(id) FROM orders_details GROUP BY order_id)')->orderBy('id','desc')->paginate($limit)->toArray();

            return response()->json(array('success'=>true,'message'=>"Order Listing",'data'=>$orderList['data'],'current_page'=>$orderList['current_page'],'last_page'=>$orderList['last_page'],'total_results'=>$orderList['total']) ,200,[],JSON_NUMERIC_CHECK);

        }catch (\Exception $e) {
            return response()->json(['failure'=>500, 'error' => $e->getMessage()]);
        }
    }

    public function getOrderDetails(Request $request, $order_id){
        try{
            $user = Auth::user();
            if($user){
                $order_details =  OrderDetails::with('prductDetails','userInfo')->where('order_id',$order_id)->get();
                foreach($order_details as $value){
                    $shop_product_id = $value->product_id;
                    $business_product_id =  $value->prductDetails->product_id;
                    //$product_images = InfluencerShopProductImages::where('shop_product_id',$shop_product_id)->get();
                    $product_images = ProductImage::where('product_id',$business_product_id)->get();
                    $value['product_images'] = $product_images;
                    $address_id = $value->address_id;
                }
                $order = Order::with('buyerInfo:buyer_id,name')->where('id',$order_id)->select('id','buyer_id','address_id','order_status')->first();
                //print'<pre>';print_r($order);exit;
                $address_id = $order->address_id;

                $address = ShippingAddress::with('country:id,name','state:id,name','city:id,name')->where('id',$address_id)->first();
                if($address){
                    $address = $address->toArray();
                }

                $data['order_details'] = $order_details;
                $data['address'] = $address;
                $data['order'] = $order;
                return response()->json(array('success'=>true,'message'=>"Order detail fetched",'data'=>$data) ,200,[],JSON_NUMERIC_CHECK);
            }else{
                $error_message = array('message'=>"Unauthorized",'status'=>false);
                return response()->json($error_message,401);
            }

        }catch (\Exception $e) {
            return response()->json(['failure'=>500, 'error' => $e->getMessage()]);
        }
    }

    public function changeOrderStatus(Request $request){
        try{
            $user = Auth::user();
            if(!$user){
                $error_message = array('message'=>"Unauthorized",'status'=>false);
                return response()->json($error_message,401);
            }
            $input = $request->all();
            $validator = Validator::make($input,[
                'status' => 'numeric | required',
                'order_details_id'  => 'required | exists:orders_details,id',
            ]);
            if($validator->fails()) {
                $ret = array('status'=>false, 'message'=> $validator->messages()->first());
                return response()->json($ret);
            }
            $order_details = OrderDetails::where('id',$input['order_details_id'])->where('business_id',$user->id)->first();
            if(!$order_details){
                $ret = array('status'=>false, 'message'=> 'Invalid order id');
                return response()->json($ret,400);
            }
            $order_details->status = $input['status'];
            $order_details->save();

            if($input['status'] == 3){
                $quantity = $order_details->quantity;
                $infl_product_id = $order_details->product_id;
                $prod_id = InfluencerShopProducts::where('id',$infl_product_id)->value('product_id');
                $product = Product::where('id',$prod_id)->first();
                $product->total_sale += $quantity;
                $product->save();

                $quantity = $order_details->quantity;
                $infl_product_id = $order_details->product_id;
                $product = InfluencerShopProducts::where('id',$infl_product_id)->first();
                $product->total_sale += $quantity;
                $product->save();
            }

            // Notification start
            if($input['status'] == 1){
                $notification['user_id'] = $order_details->influencer_id;
                $notification['request_type'] = 1;
                $notification['request_id'] = $order_details->order_id;
                $notification['description'] = 'New order placed by your reference';
                Notification::create($notification);

                $push = new PushNotificationController;
                $push->pushNotificationHandler($order_details,2);
            }
            // Notification end

            //Email to buyer
            $order_id = $order_details->order_id;
            $buyer_id = Order::where('id',$order_id)->value('buyer_id');
            $buyer_email = User::where('id',$buyer_id)->value('email');
            if(isset($buyer_email) && $buyer_email !== ''){
                if($input['status'] == 1){
                    $msg = 'Recieved';
                }elseif($input['status'] == 2){
                    $msg = 'Dipatched';
                }elseif($input['status'] == 3){
                    $msg = 'Delivered';
                }
                Mail::send('emails.order_status', [ 'status' => $msg ,'msg'=>'Order status updated'],
                    function ($m) use ($buyer_email){
                    $m->from(config('mail.from.address'),config('app.name'));
                    $m->to($buyer_email)->subject('Order Status');
                });
            }

            return response()->json(array('success'=>true,'message'=>"Order status changed successfully") ,200,[],JSON_NUMERIC_CHECK);

        }catch (\Exception $e) {
            return response()->json(['failure'=>500, 'error' => $e->getMessage()]);
        }
    }


    /*public function fetchProducts(Request $request){

	    $input = $request->all();
	    $user = Auth::user();
            $input['user_id']  = $user['id'];

	     $products = Product::where('user_id','=',$input['user_id'])->get();
	     if($products) {
		     foreach($products as $product){
			     $products->main_image = "https://twiva-api.apparrant.com/storage/images/products/".$product->main_image;
		     }
        return response(['code'=>200,'products'=>$products,"message"=>'fetched successfully'], 200);
        }else
        {
        return response(['code'=>400,"message"=>'Error uploading file'], 400);
        }


    } */

    /// Import product
    // public function productDataImport(Request $request)
    // {
    //     //try {
    //         $validator = Validator::make(
    //             [
    //                 'file'      => $request->file,
    //                 'extension' => strtolower($request->file->getClientOriginalExtension()),
    //             ],
    //             [
    //                 'file'          => 'required',
    //                 'extension'      => 'required|in:xlsx,xls',
    //             ]
    //           );
    //         if ($validator->fails()) {
    //             return response()->json(['error'=>$validator->errors()], 401);
    //         }
    //         if($request->hasFile('file')) {
    //             $path = $request->file('file')->getRealPath();
    //             $data = Excel::import(new ProductImport, $request->file('file'));
    //         } else {
    //                $data = '';
    //         }

    //         if ($data) {
    //             $success = 'Product import successfull.';
    //             return response()->json(['success'=>$success,], $this-> successStatus);
    //         } else {
    //             return $this->returnJsonResponse('failure', 2, 'No file found.', 'bad_request');
    //         }
    //     //} catch (\Exception $e) {
    //         return $this->returnJsonResponse('failure', 500, $e->getMessage(), 'bad_request');
    //     //}
    // }

    // /// Import product
    // public function productImageImport(Request $request)
    // {
    //     //try {
    //         $validator = Validator::make(
    //             [
    //                 'file'      => $request->file,
    //                 'extension' => strtolower($request->file->getClientOriginalExtension()),
    //             ],
    //             [
    //                 'file'          => 'required',
    //                 'extension'      => 'required|in:xlsx,xls',
    //             ]
    //           );
    //         if ($validator->fails()) {
    //             return response()->json(['error'=>$validator->errors()], 401);
    //         }
    //         if($request->hasFile('file')) {
    //             $path = $request->file('file')->getRealPath();
    //             $data = Excel::import(new ProductImportImage, $request->file('file'));
    //         } else {
    //                $data = '';
    //         }

    //         if ($data) {
    //             $success = 'Product import successfull.';
    //             return response()->json(['success'=>$success,], $this-> successStatus);
    //         } else {
    //             return $this->returnJsonResponse('failure', 2, 'No file found.', 'bad_request');
    //         }
    //     //} catch (\Exception $e) {
    //         return $this->returnJsonResponse('failure', 500, $e->getMessage(), 'bad_request');
    //     //}
    // }


}

//  maatwebsite

// class ProductImport implements ToCollection, WithStartRow
// {
//     /**
//      * @param array $row
//      *
//      * @return Customer|null
//      */
//     public function collection(Collection $rows)
//     {

//         foreach ($rows as $row)
//         {

//             $imageFrom = public_path().'/importProduct/images/'.$row[9];
//             if(file_exists($imageFrom)){
//             $newImageName = time().$row[9];
//             $imageTo = public_path().'/storage/Product/'.$newImageName;
//             rename($imageFrom,$imageTo);
//             }else{
//                 $newImageName = Null;
//             }
//             $customerData[] = array(
//                 'user_id'   =>  Auth::id(),
//                 'name'     => $row[0],
//                 'sku'    => $row[1],
//                 'title' => $row[2],
//                 'category' => $row[3],
//                 'price' => $row[4],
//                 'stock' => $row[5],
//                 'size' => $row[6],
//                 'color' => $row[7],
//                 'description' => $row[8],
//                 'main_image' => $newImageName,
//                 'rating' => $row[10],
//                 'status' => $row[11],
//                 'created_by' => Auth::id(),
//                 'created_at' => date('Y-m-d H:i:s'),
//                 'updated_at' => date('Y-m-d H:i:s')
//             );

//         }
//         $prod = Product::insert($customerData);
//     }
//     public function startRow(): int
//     {
//         return 2;
//     }
// }
//     // import product image
// class ProductImportImage implements ToCollection, WithStartRow
// {
//     /**
//      * @param array $row
//      *
//      * @return Customer|null
//      */
//     public function collection(Collection $rows)
//     {

//         foreach ($rows as $row)
//         {

//             $imageFrom = public_path().'/importProduct/images/productImage/'.$row[3];
//             if(file_exists($imageFrom)){
//             $newImageName = time().$row[3];
//             $imageTo = public_path().'/storage/Product/productImage/'.$newImageName;
//             rename($imageFrom,$imageTo);
//             }else{
//                 $newImageName = Null;
//             }
//             $product = Product::where('sku',$row[1])->first();
//             if($product && $product->id){
//             $productId = $product->id;
//             $customerData[] = array(
//                 'user_id' => Auth::id(),
//                 'product_id'   =>  $productId,
//                 'name'     => $row[0],
//                 'sku'    => $row[1],
//                 'title' => $row[2],
//                 'image' => $row[3],
//                 'created_by' => Auth::id(),
//                 'created_at' => date('Y-m-d H:i:s'),
//                 'updated_at' => date('Y-m-d H:i:s')
//             );
//             }else{
//                 $productId = Null;
//             }

//         }
//         $prod = ProductImage::insert($customerData);
//     }
//     public function startRow(): int
//     {
//         return 2;
//     }
// }
