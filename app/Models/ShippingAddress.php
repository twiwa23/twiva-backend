<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Country;
use App\Models\Sizes;

class ShippingAddress extends Model
{
    //
    protected $table = 'shipping_address';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['buyer_id','first_name','last_name','apartment','locality','city','country','state','postal_code','phone_number'];

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country', 'id');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state', 'id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city', 'id');
    }
}
