<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserStoryPost extends Model
{

    protected $table = "user_story_post";
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $dates = ['created_at','updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    /**
     * The attributes that should be mass assignable.
     *
     * @var array
     */

    protected $fillable = ['influencer_id','product_id','platform','is_schedule','image','color','caption','start_date','start_time','repeat_type','repeated_days','end_after_type','end_date','number_of_time','script_run_count','is_completed'];

    protected $appends = [
        'color_details','size_details','category_details','subcategory_details'
    ];

    public function productDetails() {
        return $this->hasOne('App\Models\InfluencerShopProducts', 'id', 'product_id');
    }

    public function getColorDetailsAttribute(){
        if($this->color){
            $color = explode(',',$this->color);
            return Colors::whereIn( 'id',$color )->select('id', 'name', 'hexaval')->get();
        }else{
            return array();
        }
    }

    public function getSizeDetailsAttribute(){
        if($this->size){
            $size = explode(',',$this->size);
            return Sizes::whereIn('id',$size)->select('id', 'name')->get();
        }else{
            return array();
        }
    }

    public function getCategoryDetailsAttribute(){
        return Categories::where('id',$this->category)->first();
    }

    public function getSubcategoryDetailsAttribute(){
        if($this->sub_category){
            $sub_cat = explode(',',$this->sub_category);
            return SubCategories::whereIn('id',$sub_cat)->get();
        }else{
            return array();
        }
    }
    
}