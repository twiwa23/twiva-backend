<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSocialConnection extends Model
{

    protected $table = "user_social_connection";
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $dates = ['created_at','updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    /**
     * The attributes that should be mass assignable.
     *
     * @var array
     */
    protected $fillable = ['influencer_id','social_network_type','social_id','request_token','details'];
    
}
