<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddChangesToOrdersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_details', function (Blueprint $table) {
            $table->unsignedBigInteger('influencer_id')->default(0)->after('product_id')->comment('Influencers id as foreign key on Users table');
            $table->foreign('influencer_id')->references('id')->on('users');
            $table->unsignedBigInteger('business_id')->default(0)->after('product_id')->comment('Business id as foreign key on Users table');
            $table->foreign('business_id')->references('id')->on('users');
            $table->integer('status')->default(1)->comment('1 for order placed, 2 for dispatched, 3 for shipped,4 for delivered');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_details', function (Blueprint $table) {
            //
        });
    }
}
