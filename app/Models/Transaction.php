<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $table = 'transactions';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'user_id', 'request_type', 'request_id', 'phone_number', 'transaction_id', 'transaction_amount', 'transaction_details', 'payment_gateway_type', 'status','order_id'];
}
