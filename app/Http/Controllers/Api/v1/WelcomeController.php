<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use Validator;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\AllImage;
use App\Models\Categories;
use App\Models\SubCategories;
use App\Models\Sizes;
use App\Models\Colors;

class WelcomeController extends ApiController
{
    public $successStatus = 200;
    public $unauth = 401;
    public $nodata = 404;
    public $badRequest = 422;
    public $validateInputs= 400; 
    //Country list
    public function countryList(){
        $country = Country::all();
        if(count($country) > 0){
            $countryData['status'] = 200;
            $countryData['message'] = "Success.";
            $countryData['countries'] = $country; 
            return response()->json($countryData);
        }else{
            $error['status'] = $this->badRequest;
            $error['message'] = "Country list not found.";
            return response()->json($error);
        }
    }
    // City list

    public function cityList(){
        $City = City::all();
        if(count($City) > 0){
            $cityData['status'] = 200;
            $cityData['message'] = "Success.";
            $cityData['cities'] = $City; 
            return response()->json($cityData);
        }else{
            $error['status'] = $this->badRequest;
            $error['message'] = "City list not found.";
            return response()->json($error);
        }
    }
    // state list

    public function stateList(){
        $state = State::all();
        if(count($state) > 0){
            $stateData['status'] = 200;
            $stateData['message'] = "Success.";
            $stateData['states'] = $state; 
            return response()->json($stateData);
        }else{
            $error['status'] = $this->badRequest;
            $error['message'] = "State list not found.";
            return response()->json($error);
        }
    }
    // select

    // City list

    public function citySelect(Request $request){
        $City = City::where('state_id',$request->state_id)->get();
        if(count($City) > 0){
            $cityData['status'] = 200;
            $cityData['message'] = "Success.";
            $cityData['cities'] = $City; 
            return response()->json($cityData);
        }else{
            $error['status'] = $this->badRequest;
            $error['message'] = "City list not found.";
            return response()->json($error);
        }
    }
    // state list

    public function stateSelect(Request $request){
        $state = State::where('country_id', $request->country_id)->get();
        if(count($state) > 0){
            $stateData['status'] = 200;
            $stateData['message'] = "Success.";
            $stateData['states'] = $state; 
            return response()->json($stateData);
        }else{
            $error['status'] = $this->badRequest;
            $error['message'] = "State list not found.";
            return response()->json($error);
        }
    }

    // ad product image
    public function addImages(Request $request){
        try{
            $validator = Validator::make($request->all(), [ 
                'image' => 'image:jpeg,png,jpg,gif,svg|max:2048',
                'type' => 'required|numeric'
            ]);
            if ($validator->fails()) { 
                return response()->json(['status'=>$this->validateInputs,'error'=>$validator->errors()], $this->validateInputs);
            }
            $input = $request->all();
            $image = $request->file('image');
            $input['user_id']  = 1;
            if($input['type'] == 1){
                $type = 'Logo';
            }else if($input['type'] == 2){
                $type = 'Product';
            }else if($input['type'] == 3){
                $type = 'Multi Product';
            }else{
                $type = 'Other';
            }
            $uploadFolder = 'media/'.$type;
            $image_uploaded_path = $image->store($uploadFolder, 'public');
            $input['image'] = $image_uploaded_path;
            $input['product_id'] = 1;
            try{
                $productImage = AllImage::create($input);
                $response['status']     = $this->successStatus;
                $response['message']    = 'Image uploaded.';
                $response['image_path'] =  $image_uploaded_path;
                $response['type']       =     $input['type'];
                return response()->json($response);
            } catch (\Exception $e) {
                $error['status'] = $this->badRequest;
                $error['message']  = 'Invalid Inputs.';
                return response()->json(['failure'=>$error]);
            }
        } catch (\Exception $e) {
            $error['status'] = $this->badRequest;
            $error['message']  = $e->getMessage();
            return response()->json(['failure'=>$error]);
        }
    }
 
    public function categoryListing(){
        
        $categories = Categories::all();
        return response()->json(['status'=>true,'message'=>'Categories Listing','data'=>$categories ] ,200);
        
    }

    public function categorySubCategoryListing(){
        
        $categories = Categories::with('subcategories')->get();
        return response()->json(['status'=>true,'message'=>'Categories And Subcategories Listing','data'=>$categories ] ,200);
        
    }

    public function subCategoryListing($id){
        
        $categories = SubCategories::where('category_id',$id)->get();
        return response()->json(['status'=>true,'message'=>'Sub categories Listing','data'=>$categories ] ,200);
        
    }


    public function SizeAndColorList(Request $request){

        $data = $request->all();
        $validator = Validator::make($data, [ 
            'category_id'      => 'required|exists:categories,id'
        ]);
        if ($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()], 404);
        }
        $sizes = Sizes::where('category_id','=',1)->get();
        //$sizes = Sizes::where('category_id','=',$data['category_id'])->get();
            $colours = Colors::all();

        return response()->json(['status'=>true,'message'=>'Size And Color Listing','sizes'=>$sizes,'colors'=>$colours ] ,200);

    }


}
