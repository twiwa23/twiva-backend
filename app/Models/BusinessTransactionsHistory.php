<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Colors;
use App\Models\Sizes;
use App\Models\Categories;

class BusinessTransactionsHistory extends Model
{

    protected $table = 'business_transactions_history';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'user_id', 'transaction_amount', 'tansaction_details', 'request_type', 'request_id', 'is_active', 'created_at', 'updated_at'];

    protected $hidden = ['updated_at','deleted_at'];

    public function userSubscriptionInfo(){
        return $this->hasMany('App\Models\UserSubscription', 'id', 'request_id');
    }
}
