<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([

            [
                'name' => 'Pet Supplies'
            ],
            [
                'name' => 'Clothing'
            ],
            [
                'name' => 'Clothing Accessories'
            ],
            [
                'name' => 'Costumes & Accessoriesr'
            ],
            [
                'name' => 'Jewelry'
            ],
            [
                'name' => 'Hobbies & Creative Arts'
            ],
            [
                'name' => 'Party & Celebration'
            ],
            [
                'name' => 'Baby Toys & Activity Equipment'
            ],
            [
                'name' => 'Advertising & Marketing'
            ],
            [
                'name' => 'Agriculture'
            ],
            [
                'name' => 'Dentistry'
            ],
            [
                'name' => 'Food Service'
            ],
            [
                'name' => 'Medical'
            ],
            [
                'name' => 'Retail'
            ],
            [
                'name' => 'Science & Laboratory'
            ],
            [
                'name' => 'Work Safety Protective Gear'
            ],
            [
                'name' => 'Cameras'
            ],
            [
                'name' => 'Electronics'
            ],
            [
                'name' => 'Beverages'
            ],
            [
                'name' => 'Food Items'
            ],
            [
                'name' => 'Furniture'
            ],
            [
                'name' => 'Hardware'
            ],
            [
                'name' => 'Power & Electrical Supplies'
            ],
            [
                'name' => 'Health Care'
            ],
            [
                'name' => 'Home & Garden'
            ],
            [
                'name' => 'Luggage & Bags'
            ],
            [
                'name' => 'Erotic'
            ],
            [
                'name' => 'Weapons'
            ],
            [
                'name' => 'Books'
            ],
            [
                'name' => 'Office Supplies'
            ],
            [
                'name' => 'Software'
            ],
            [
                'name' => 'Athletics'
            ],
            [
                'name' => 'Toys'
            ],
            [
                'name' => 'Vehicles & Parts'
            ]

        ]);
    }
}
