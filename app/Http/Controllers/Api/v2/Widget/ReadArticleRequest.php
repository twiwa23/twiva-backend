<?php

namespace App\Http\Requests\API\v2\Widget;

use App\Http\Requests\API\ApiFormRequest;

//use Illuminate\Foundation\Http\FormRequest;

class ReadArticleRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->id;
        
        return [
            'domain' => 'required',
            'credit_type' => 'required_if:purchase,1',
            'category'=> 'required_if:price_id,""',
            'price_id'=> 'required_if:category,""',
            'post_id' => 'required',
            'post_url' => 'required',
            'purchase' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'credit_type.required_if' => 'The credit type field is required when purchase is true.'
        ];
    }
}
