<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class OrderDetails extends Model
{
    use SoftDeletes;

    protected $table = "orders_details";
    protected $dates = ['deleted_at','created_at','updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = ['created_at','updated_at','deleted_at'];

    /**
     * The attributes that should be mass assignable.
     *
     * @var array
     */
    protected $fillable = ['order_id','product_id','business_id','influencer_id','quantity','size','color','amount','status'];

    public function userInfo() {
        return $this->hasOne('App\User', 'id', 'influencer_id');
    }

    public function prductDetails() {
        return $this->hasOne('App\Models\InfluencerShopProducts', 'id', 'product_id');
    }

    public function productImages() {
        return $this->hasMany('App\Models\InfluencerShopProductImages', 'shop_product_id', 'product_id');
    }

    public function Order() {
        return $this->hasOne('App\Models\Order', 'id', 'order_id');
    }

    public function orderCount() {
        return $this->hasMany('App\Models\Order', 'id', 'order_id');
    }

    public function earnings() {
        return $this->hasMany('App\Models\InfluencerTransactionsHistory', 'request_id', 'id');
    }

    public function businessDeviceToken() {
        return $this->hasMany('App\Models\UserDeviceToken', 'user_id', 'business_id');
    }

    public function influencerDeviceToken() {
        return $this->hasMany('App\Models\UserDeviceToken', 'user_id', 'influencer_id');
    }
}