<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Support\Arr;
use App\User;
use App\Models\Company;
use App\Models\UserDeviceToken;
use App\Models\OrderDetails;

class PushNotificationController extends TestCase
{

    public function pushNotificationHandler($detail,$type)
    {
        if($type == 1){
            $device_token = [];
            $orderList = OrderDetails::where('order_id',$detail->id)->with('businessDeviceToken')->whereRaw('id IN (select MAX(id) FROM orders_details GROUP BY business_id)')->get();
            foreach($orderList as $order){
                if(count($order->businessDeviceToken)>0){
                    $deviceToken=$order->businessDeviceToken->pluck('device_token')->toArray();
                    $device_token=array_merge($device_token,$deviceToken);
                }
            }
            if(count($device_token) > 0){
                $notificationDetailsArray=array();
                $notificationDetailsArray['title']='You recieved a new order';
                $notificationDetailsArray['message']= 'You recieved a new order';
                $notificationDetailsArray['requestId']=$detail->id;
                $notificationDetailsArray['requestType']=1;
                $this->sendPushNotications($device_token,$notificationDetailsArray);
            }
            return true;
        }

        if($type == 2){
            $device_token = [];
            $orderList = OrderDetails::where('order_id',$detail->order_id)->with('influencerDeviceToken')->get();
            foreach($orderList as $order){
                if(count($order->influencerDeviceToken)>0){
                    $deviceToken=$order->influencerDeviceToken->pluck('device_token')->toArray();
                    $device_token=array_merge($device_token,$deviceToken);
                }
            }
            if(count($device_token) > 0){
                $notificationDetailsArray=array();
                $notificationDetailsArray['title']='Order in processing';
                $notificationDetailsArray['message']= 'Order in processing';
                $notificationDetailsArray['requestId']=$detail->order_id;
                $notificationDetailsArray['requestType']=1;
                $this->sendPushNotications($device_token,$notificationDetailsArray);
            }
            return true;
        }

        if($type == 3){
            $device_token = [];
            $users= UserDeviceToken::where('user_id',$detail['user_id'])->get();
            if(count($users) > 0){
                foreach($users as $user){
                    $deviceToken=$user->device_token;
                    array_push($device_token, $deviceToken);
                }
            }
            if(count($device_token) > 0){
                $notificationDetailsArray=array();
                $notificationDetailsArray['title']='Your subscription will expire in 7 days';
                $notificationDetailsArray['message']= 'Your subscription will expire in 7 days';
                $notificationDetailsArray['requestId']=$detail['subscription_id'];
                $notificationDetailsArray['requestType']=2;
                $this->sendPushNotications($device_token,$notificationDetailsArray);
            }
            return true;
        }
    }

    public function sendPushNotications($device_token,$notificationDetailsArray){        
        $badge=1;
        try {
            $notificationData = [
                'title' => $notificationDetailsArray['message'],
                'sound' => 'default',
                'mutable-content'=> 1,
                'content-available'=> 1,
                'badge' => 1
            ];
            $extraNotificationData = [
                    "message"    => $notificationData,
                    'title'      => $notificationDetailsArray['message'],
                    'sound'      => 'default',
                    'badge'       => 1,
                    'message'     => $notificationDetailsArray['message'],
                    'request_id'   => $notificationDetailsArray['requestId'],
                    'request_type' => $notificationDetailsArray['requestType'],
                    'content-available'=> 1
            ];
            $fcmNotification = [
                'registration_ids' => $device_token,
                'notification' => $notificationData,
                'data' => $extraNotificationData
            ];
            $headers = [
                'Authorization: key=' . env('FCM_API_KEY'),
                'Content-Type: application/json'
            ];
            $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$fcmUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $result = curl_exec($ch);
            curl_close($ch);
            $result=json_decode($result);
            return true;
        }catch (Exception $e) {
            Log::info(json_encode($e));
        }
    }
}
