<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfluencerBankDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_bank_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('influencer_id')->comment('Influencer\'s id as foreign key on Users table');
            $table->foreign('influencer_id')->references('id')->on('users');
            $table->string('bank_name');
            $table->integer('account_number');
            $table->string('recipient_name')->comment('influencer\'s name');
            $table->string('ifsc_code');
            $table->softdeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_bank_details');
    }
}
