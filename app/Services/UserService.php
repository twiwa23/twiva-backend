<?php

namespace App\Services;

use App\User;
use App\Models\City;
use App\Models\State;
use App\Models\Company;
use App\Models\Country;
use App\Models\TempTokens;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


/**
 * Class NotificationService.
 */
class UserService extends BaseService {
    public $successStatus = 200;
    public $unauth = 401;
    public $nodata = 404;
    public $badRequest = 422;
    /**
     * User constructor.
     *
     * @param  Login  $user
     */
    private $responseData;

    /**
     * Login constructor.
     *
     * @param  Login  $user
     */
    public function login(){
        try{
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
                $user = Auth::user();
                $response['status'] = $this->successStatus;
                $response['message'] = 'Login Success!';
                $response['token'] =  $user->createToken(config('api.TOKEN_NAME'))-> accessToken;

                $company = Company::where('user_id',Auth::user()->id)->first();
                if($company){

                $compInfo['bussiness_name'] = $company->busi_name;
                $compInfo['contact_name'] = $company->busi_name;
                $compInfo['business_number'] = $company->busi_number;
                $compInfo['description'] = $company->description;

                $city = City::where('id',$company->city)->select('name','state_id')->first();
                $state = State::where('id',$company->state)->select('name','country_id')->first();
                $country = Country::where('id',$company->country)->select('name','id')->first();
                }else{
                    $user['bussiness_name'] = NULL;
                    $user['contact_name'] = NULL;
                    $user['business_number'] = NULL;
                    $user['description'] = NULL;
                    $country = NULL;
                    $state = NULL;
                    $city = NULL;

                }
                $user['city'] = $city;
                $user['country'] = $country;
                $user['state'] = $state;
                $user['email_verified_at'] = Auth::user()->email_verified_at;
                $response['user'] = $user;
                return $response;
            }else{
                $response['status'] = $this->badRequest;
                $response['message'] = "Invalid credentials.";
                return $response;
            }
        } catch (\Exception $e) {
            $error['status'] = $this->badRequest;
            $error['message'] = $e->getMessage();
            return response()->json(['failure'=>$error]);
        }
    }

    public function getTemp($decoded_id)
    {
        return TempTokens::find($decoded_id);
    }

    /**
     * Login constructor.
     *
     * @param  Login  $user
     */
    public function loginViaAdmin($id, $params) {
        try {

            $decoded_id = base64_decode(base64_decode($id));
            $decoded_user_id = base64_decode(base64_decode($params));
            $temp = $this->getTemp($decoded_id);
            if(!$temp || ($temp->user_id != $decoded_user_id)) {
                $response['status'] = $this->badRequest;
                $response['message'] = "Invalid Id.";
                return $response;
            }

            $user = User::find($temp->user_id);
            if (!$user) {
                $response['status'] = $this->badRequest;
                $response['message'] = "Invalid User Id.";
                return $response;
            }

            if (!$user->is_active && $user->is_verified != 1) {
                return response()->json(array('status'=>false,'error'=>__('auth.verify_email')) ,401);
            }

            $token =  $user->createToken('MyApp')->accessToken;
            if($user->type == '1') {
                $user = $this->getBusinessUserDetails($user);
            } else if( $user->type == '2'){
                $user->load(['influencer_details']);
                if(isset($user->influencer_details)){
                    $interests = explode(',',$user->influencer_details->interests);
                    $interests = DB::table('categories')->whereIn('id', $interests)->get();
                    $user->influencer_details->interests = $interests;
                }else{
                    $interests = array();
                }
                

            } else{
                $response['status'] = $this->badRequest;
                $response['message'] = "Invalid credentials.";
                return $response;
            }

            $temp->delete();
            return response()->json(
                ['status' => true,
                'data' => $user,
                'message'=>'Successfully logged in',
                'token'=> $token],
                200,[],JSON_NUMERIC_CHECK);
        } catch (\Exception $e) {
            $error['status'] = $this->badRequest;
            $error['message'] = $e->getMessage();
            return response()->json(['failure'=>$error]);
        }
    }

    public function getBusinessUserDetails($user)
    {
        $company = Company::where('user_id',$user->id)->first();
        if (!$company) {
            $user['bussiness_name'] = null;
            $user['contact_name'] = null;
            $user['business_number'] = null;
            $user['description'] = null;
            $user['country'] = null;
            $user['state'] = null;
            $user['city'] = null;
            return $user;
        }
        if($company) {
            $country = Country::where('id',$company->country)->select('name','id')->first();
            $state = State::where('id',$company->state)->select('id','name','country_id')->first();
            $city = City::where('id',$company->city)->select('id','name','state_id')->first();
            if($city) {
                $user['city'] = $city;
            } else {
                $user['city'] = null;
            }
            $user['country'] = $country;
            $user['state'] = $state;
        }
        $user['bussiness_name'] = $company->busi_name;
        $user['contact_name'] = $company->busi_name;
        $user['business_number'] = $company->busi_number;
        $user['description'] = $company->description;
        $user['comp_email'] = $company->comp_email;
        $user['logo'] = $company->logo;
        $user['building'] = $company->building;
        $user['area'] = $company->area;
        $user['pincode'] = $company->pincode;

        return $user;
    }

    /**
     * User Register.
     *
     * @param  Registration  $user
     */
    public function register($input){
        try{
            $array = config('constants.user_type');
            $userType = $this->searchForUserType($input['type'], $array);
            $input['password'] = bcrypt($input['password']);
            $input['type']     = ( int ) $userType;
            $input['profile_completed'] = 0;
            $user = User::create($input);
            $response['status'] = $this->successStatus;
            $response['message'] = 'Registration process has been initiated.';
            $hide['token'] = $user->createToken(config('api.TOKEN_NAME'))-> accessToken;
            $user['user_type'] = $userType;
            $user['id'] = $user->id;
            $user['email'] = $input['email'];
            $user['referal_code'] = $input['referal_code'];
            $user['profile_completed'] = $user->profile_completed;
            $user['email_verified_at'] = NULL;
            $user['bussiness_name'] = NULL;
            $user['contact_name'] = NULL;
            $user['business_number'] = NULL;
            $user['description'] = NULL;
            $user['country'] = NULL;
            $user['city'] = NULL;
            $user['state'] = NULL;
            $user['area'] = NULL;
            $user['building'] = NULL;
            $response['user'] = $user;
            return $response;
        } catch (\Exception $e) {
            $error['status'] = $this->badRequest;
            $error['message'] = 'Inputs not valid.';
            return response()->json(['failure'=>$error]);
        }
    }

    /**
     * User Register.
     *
     * @param  Registration  $user
     */
    public function compInfoService($input){
        try{
            $array = config('constants.user_type');
            $user = User::find($input['id']);
            if($user && $user->id){
                $company = Company::where('user_id',$user->id)->first();
                if($user && $user->id && $company && $company->user_id == $user->id){
                    $response['status'] = $this->badRequest;
                    $response['message'] = 'Company found with user details';
                    return $response;
                }else{
                //$image = $request->file('logo');
                //$input = $request->all();
                $input['user_id']  = $user->id;
                //$uploadFolder = 'company';
                //$image = $request->file('logo');
                //$image_uploaded_path = $image->store($uploadFolder, 'public');
                //$input['logo'] = $image;
                $userComp = Company::create($input);
                $user->profile_completed = 1;
                $user->save();
                $response['token'] = $user->createToken(config('api.TOKEN_NAME'))-> accessToken;
                $response['status'] = $this->successStatus;
                $response['message'] = 'Registration process has been completed.';
                $compInfo['user_type'] = $user->type;
                //$compInfo['name'] = $user->name;
                $compInfo['email'] = $user->email;
                $compInfo['referal_code'] = $user->referal_code;
                $compInfo['profile_completed'] = $user->profile_completed;
                $compInfo['email_verified_at'] = $user->email_verified_at;
                $compInfo['bussiness_name'] = $userComp->busi_name;
                $compInfo['contact_name'] = $userComp->busi_name;
                $compInfo['business_number'] = $userComp->busi_number;
                $compInfo['description'] = $userComp->description;
                $compInfo['country'] = $userComp->country;
                $compInfo['city'] = $userComp->city;
                $compInfo['state'] = $userComp->state;
                $compInfo['area'] = $userComp->area;
                $compInfo['building'] = $userComp->building;
                $response['user'] = $userComp->logo;
                $response['user'] = $compInfo;
                return $response;
            }
            }else{
                $response['status'] = $this->badRequest;
                $response['message'] = 'User not found.';
                return $response;
            }
        } catch (\Exception $e) {
            $error['status'] = $this->badRequest;
            $error['message'] = 'Input not valid';
            return response()->json(['failure'=>$error]);
        }
    }


    /**
     * User Register.
     *
     * @param  Registration  $user
     */
    public function basicInfoService($input){
        try{
            $user = Auth::user();
            $company = Company::where('user_id',$user->id)->first();
            if($user && $user->id && $company && $company->user_id){
                $input['user_id']  = $user->id;
                $input['comp_id']  = $company->id;
                $userBasic = UserDetail::firstOrCreate(['id' => $company->id],$input);
                $hide['token'] = $user->createToken(config('api.TOKEN_NAME'))-> accessToken;
                $basicInfo['user_type'] = $user->type;
                //$basicInfo['name'] = $user->name;
                $basicInfo['email'] = $user->email;
                $basicInfo['referal_code'] = $user->referal_code;
                $basicInfo['email_verified_at'] = $user->email_verified_at;
                $basicInfo['bussiness_name'] = $userBasic->busi_name;
                $basicInfo['contact_name'] = $userBasic->busi_name;
                $basicInfo['business_number'] = $userBasic->busi_number;
                $basicInfo['description'] = $userBasic->description;
                $basicInfo['country'] = NULL;
                $basicInfo['city'] = NULL;
                $basicInfo['state'] = NULL;
                $basicInfo['area'] = NULL;
                $basicInfo['building'] = NULL;
                $response['user'] = $basicInfo;
                return $response;
            }else{
                $response['status'] = 401;
                $response['message'] = 'Company found with user details';
            }
        } catch (\Exception $e) {
            $error['status'] = $this->badRequest;
            $error['message'] = $e->getMessage();
            return response()->json(['failure'=>$error]);
        }
    }
    /*
    *Search id from type
    *
    * @return id
    */
    public function searchForUserType($typeVal, $array) {
        foreach ($array as $key => $val) {
            if ($val['name'] === $typeVal) {
                return $val['id'];
            }
        }
        return 3;
    }
}
