<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAppendScheduleParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_story_post', function (Blueprint $table) {
           
            $table->date('start_date')->nullable();
            $table->string('start_time')->nullable();
            $table->tinyInteger('repeat_type')->default(0)->comment('1 for Daily,2 for specific day');
            $table->string('repeated_days')->nullable()->comment('monday,tuesday,wednesday,thrusday,friday,saturday,sunday');
            $table->tinyInteger('end_after_type')->default(0)->comment('1 On End Date, 2 after specific number of tym');
            $table->date('end_date')->nullable();
            $table->integer('number_of_time')->default(0);
            $table->integer('script_run_count')->default(0)->comment('Manage this count as number of time script run');
            $table->integer('is_completed')->default(1)->comment('when schedule done, mark it as complete');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_story_post', function (Blueprint $table) {
            //
        });
    }
}
