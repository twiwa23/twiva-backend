<?php

use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([

            [
                'bank_name' => 'Equity Bank Kenya'
            ],
            [
                'bank_name' => 'Standard Chartered Bank Kenya'
            ],
            [
                'bank_name' => 'Barclays Bank Kenya'
            ],
            [
                'bank_name' => 'Kenya Commercial Bank'
            ],
            [
                'bank_name' => 'Cooperative Bank of Kenya'
            ],
            [
                'bank_name' => 'Diamond Trust Bank of Kenya'
            ],
            [
                'bank_name' => 'National Bank of Kenya'
            ]

        ]);
    }
}
