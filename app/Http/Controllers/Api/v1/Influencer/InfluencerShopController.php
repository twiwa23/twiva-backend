<?php

namespace App\Http\Controllers\Api\v1\Influencer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InfluencerShop;
use App\Models\InfluencerShopProducts;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\UserStoryPost;
use App\Models\ProductReviews;
use App\Models\InfluencerShopProductImages;
use Illuminate\Support\Facades\Auth;
use Validator,Mail,File,URL;

class InfluencerShopController extends Controller
{
    /**
     * Display a listing of the eShops.
     *
     * @return \Illuminate\Http\Response
     */
    public function getListableShops()
    {
        $user = Auth::user();

        if( $user ){
            try{
                $shops = InfluencerShop::where('user_id',$user->id)->get();
                $success_message = array('message'=>"Shops listing.","data"=>$shops,'status'=>true);
                return response()->json($success_message,200);
            }
            catch( \Exception $e)
            {
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }
        else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }

    }


    /**
     * Add a influencer's Shop
     *
     * @return \Illuminate\Http\Response
     */
    public function addShop( Request $request )
    {
        $user = Auth::user();

        if( $user ){
            $user_shop = InfluencerShop::where('user_id',$user->id)->first();
            if(empty($user_shop)){
                try{
                    $date = \Carbon\Carbon::now();
                    $data = $request->all();
                    $messages = [];
                    $validator = Validator::make($data, [
                        'shop'          => 'required | array',
                        'shop.name'     => 'required | string',
                        'shop.image'    => 'required | string',
                        'shop.logo'     => 'required | string',
                        'products'      => 'required | array | min:1',
                        'products[*].product_id'    => 'required | exists:products,id',
                        'products[*].product_images'    => 'required | array',
                        'products[*].title'    => 'required | string',
                        'products[*].price'    => 'required | integer',
                        'products[*].description'    => 'required | string'
                    ],$messages);
                    if ($validator->fails()) {
                        return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
                    }

                    $shop     = $data['shop'];
                    $products = $data['products'];

                    //create shop
                    $shop_details['user_id'] = $user->id;
                    $shop_details['shop_cover_image_path'] = $shop['image'];
                    $shop_details['shop_logo_path'] = $shop['logo'];
                    $shop_details['shop_name'] = $shop['name'];
                    $shop_details['shop_description'] = $shop['description'];
                    $shop_details['shop_font'] = $shop['font'];
                    $shop_details['shop_color'] = $shop['color'];
                    $shop = InfluencerShop::create($shop_details);

                    // dd($shop->toArray());

                    foreach($products as $product){
                        
                        $product['shop_id'] = $shop->id;  
                        $product['influencer_id'] = $user->id;  
                        $productImages = $product['product_images'];
                        unset($product['product_images']);
                        $productImgArray = array();
                        $influencerShopProduct = InfluencerShopProducts::create($product);

                        foreach ($productImages as $productImage) {
                        $productImgArray[]= array("shop_product_id" => $influencerShopProduct->id ,'image_path'=>$productImage['image'],'is_cover_pic'=>$productImage['is_cover_pic'],'created_at'=>$date,'updated_at'=>$date);
                        }
                        if(count($productImgArray)>0){
                        InfluencerShopProductImages::insert($productImgArray);
                        }                    
                    }
                    //save products of this shop
                    

                    $success_message = array('status'=>true,'message'=>"Shop created successfully");
                    return response()->json($success_message,200);
                }
                catch( \Exception $e)
                {
                    $error_message = array('error'=>$e->getMessage(),'status'=>false);
                    return response()->json($error_message,500);
                }
            }else{
                $error_message = array('status'=>false,'message'=>"You Cannot add more than one shop");
            return response()->json($error_message,401);
            }
        }
        else{
            $error_message = array('status'=>false,'message'=>"Unauthorized");
            return response()->json($error_message,401);
        }

    }

    public function editShop(Request $request)
    {
        $userId = Auth::id();
        try{
            $input = $request->all();               
            $validator = Validator::make($input,[
                'shop_id'  => 'required | exists:influencer_shop,id',
                'shop_name'     => 'required | string',
                'shop_cover_image_path'    => 'required | string',
                'shop_logo_path'     => 'required | string',
                'shop_description'     => 'required',
                'shop_font'    => 'required',
                'shop_color'     => 'required',
            ]);
            if($validator->fails()) {
                $ret = array('success'=>0, 'message'=> $validator->messages()->first());
                return response()->json($ret,40);
            }

            $user_shop = InfluencerShop::find($input['shop_id']);
            if($user_shop){
                $user_shop->shop_name = $input['shop_name'];
                $user_shop->shop_cover_image_path = $input['shop_cover_image_path'];
                $user_shop->shop_logo_path = $input['shop_logo_path'];
                $user_shop->shop_description = $input['shop_description'];
                $user_shop->shop_font = $input['shop_font'];
                $user_shop->shop_color = $input['shop_color'];
                $user_shop->save();

                $success_message = array('status'=>true,'message'=>"Shop updated successfully");
                return response()->json($success_message,200);
            }else{
                $error_message = array('status'=>false,'message'=>"Shop not found");
                return response()->json($error_message,404);
            }
        }catch( \Exception $e)
        {
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }
    }

    public function editCoverImage(Request $request, $shop_id)
    {
        $user = Auth::user();
        if($user){
            try{
                $input = $request->all();
                // print_r($input);die;            
                $validator = Validator::make($input,[
                    'image' => 'required'
                ]);
                if($validator->fails()) {
                    $ret = array('success'=>0, 'message'=> $validator->messages()->first());
                    return response()->json($ret);
                }
                $shop = InfluencerShop::where('user_id',$user->id)->first();
                $shop->shop_cover_image_path = $input['image'];
                $shop->save();
                $success_message = array('status'=>true,'message'=>"Shop cover image updated successfully");
                return response()->json($success_message,200);
            }catch(\Exception $e)
            {
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function viewShopDetails()
    {
        $userId = Auth::id();
        try
        {
            $userShopDetails = InfluencerShop::where('user_id',$userId)->first();
            $success_message = array('message'=>"Shops detail fetched.",'shop_details' => $userShopDetails,'status'=>true);
            return response()->json($success_message,200);
        }
        catch( \Exception $e)
        {
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }
    }

    public function viewShopProducts(Request $request, $shop_id)
    {
        $userId = Auth::id();
        try{
            $input = $request->all();               
            $validator = Validator::make($input,[
                'limit' => 'numeric | nullable',
                'page'  => 'numeric | nullable',
            ]);
            if($validator->fails()) {
                $ret = array('success'=>0, 'message'=> $validator->messages()->first());
                return response()->json($ret);
            }
            $limit = $request->limit ? $request->limit : 10;
            $products = InfluencerShopProducts::where('shop_id',$shop_id)->where('influencer_id',$userId);
            $products->with(['influencerProductImages','productDetails']);
            if($request->has('search') && $input['search'] != ""){
                $products->where('product_title','like','%'.$input['search'].'%');
            }
            if($request->has('categories') && $input['categories'] != ""){
                $categories = explode(',',$input['categories']);
                $products->whereHas('productDetails', function ($query) use ($categories) {
                    $query->whereIn('category', $categories);
                });               
            }
            if(($request->has('categories') && $input['categories'] != "") && ($request->has('sub_categories') && $input['sub_categories'] != "")){
                $sub_categories = explode(',',$input['sub_categories']);
                foreach($sub_categories as $value){
                    $products->whereHas('productDetails', function ($query) use ($value) {
                        $query->whereRaw("FIND_IN_SET($value,products.sub_category)");
                    });
                }                    
            }
            if(($request->has('sort_by') && $input['sort_by'] != "")){
                if($input['sort_by'] == 0){
                    $products->orderBy('product_price','asc');
                }else{
                    $products->orderBy('product_price','desc');
                }
            }else{
                $products->orderBy('id','desc');
            }
            $products = $products->paginate($limit)->toArray();

            foreach ($products['data'] as $key => $product) {
                $product_reviews_and_ratings = ProductReviews::with('buyerName:buyer_id,name')->where('product_id',$product['product_id'])->orderBy('id','desc')->paginate($limit)->toArray();
                $products['data'][$key]['product_reviews_and_ratings'] = $product_reviews_and_ratings['data'];
            }
            // return response()->json(array('status'=>true,'message'=>"Shops detail fetched",'shop_product_details'=>$products['data'],'current_page'=>$products['current_page'],'last_page'=>$products['last_page'],'total_results'=>$products['total']) ,200,[],JSON_NUMERIC_CHECK);

            $success_message = array('message'=>"Shops detail fetched.",'shop_product_details' => $products,'status'=>true);
            return response()->json($success_message,200);
        }catch( \Exception $e)
        {
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }
    }

    public function userStoryPost(Request $request){
        $userId = Auth::id();
        try{

            $input = $request->all();
            $is_schedule = 0;

            if (array_key_exists("is_schedule",$input)){
                if($input['is_schedule']=='1'){
                    $is_schedule = 1;
                }
            }
            if($is_schedule=='0'){
                $validator = Validator::make($input,[
                    'product_id'  => 'required',
                    'platform'  => 'required',
                    'image'  => 'required',
                    'color'  => 'required',
                    'caption'  => 'required',
                ]);
            }else{
                $validator = Validator::make($input,[
                    'product_id'  => 'required',
                    'platform'  => 'required',
                    'image'  => 'required',
                    'color'  => 'required',
                    'caption'  => 'required',
                    'start_date' => 'required',
                    'start_time' => 'required',
                    'repeat_type' => 'required',
                    'end_after_type'=> 'required'
                ]);
            }

            if($validator->fails()) {
                $ret = array('success'=> false, 'message'=> $validator->messages()->first());
                return response()->json($ret,400);
            }

            if($is_schedule=='1'){
                
                if($input['repeat_type']=='2' && $input['repeated_days']==""){
                    $ret = array('success'=> false, 'message'=> 'Repeated days is required');
                    return response()->json($ret,400);
                }

                if($input['end_after_type']=='1' && $input['end_date']==""){
                    $ret = array('success'=> false, 'message'=> 'End Date is required');
                    return response()->json($ret,400);
                }

                if($input['end_after_type']=='2' && $input['number_of_time']<0 ){
                    $ret = array('success'=> false, 'message'=> 'Number of time post share is required');
                    return response()->json($ret,400);
                }

                $input['is_schedule']  = 1;
                $input['is_completed'] = 0;
            }

            $input['influencer_id'] = $userId;
            UserStoryPost::create($input);

            $success_message = array('message'=>"Posted story saved successfully",'status'=>true);
            return response()->json($success_message,200);
            
        }catch( \Exception $e)
        {
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }
    }

    public function fetchPostStories(Request $request){
        $userId = Auth::id();
        try{
            $input = $request->all();               
            $validator = Validator::make($input,[
                'limit' => 'numeric | nullable',
                'page'  => 'numeric | nullable',
            ]);

            $is_schedule = 0;
            if (array_key_exists("is_schedule",$input)){
                if($input['is_schedule']=='1'){
                    $is_schedule = 1;
                }
            }
            
            if($validator->fails()) {
                $ret = array('success'=> false, 'message'=> $validator->messages()->first());
                return response()->json($ret,400);
            }

            $page  = $input['page'];
            $limit = isset($input['limit'])? $input['limit'] : 10;

            if($is_schedule=='0'){
                $storyListCount = UserStoryPost::where('influencer_id',$userId)->where('is_schedule',0)->count();
                $storyList= UserStoryPost::with('productDetails')->where('influencer_id',$userId)->where('is_schedule',0)->orderBy('created_at','DESC')->paginate($limit);
            }else{
                $storyListCount = UserStoryPost::where('influencer_id',$userId)->where('is_schedule',1)->count();
                $storyList= UserStoryPost::with('productDetails')->where('influencer_id',$userId)->orderBy('created_at','DESC')->where('is_schedule',1)->paginate($limit);
            }

            $storyList = $storyList->toArray();

            return response()->json(array('success'=>true,'data'=>$storyList['data'],'current_page'=>$storyList['current_page'],'last_page'=>$storyList['last_page'],'total_results'=>$storyList['total'],'message'=>"Post stories history") ,200,[],JSON_NUMERIC_CHECK);

        }catch( \Exception $e)
        {
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }
    }

}
