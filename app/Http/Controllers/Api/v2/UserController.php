<?php
namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\MpesaController;
use App\Http\Controllers\PushNotificationController;
use Illuminate\Http\Request;
use App\User;
use App\Models\Otp;
use App\Models\UserTemp;
use App\Models\Company;
use App\Models\UserCompany;
use App\Models\Subscription;
use App\Models\UserSubscription;
use App\Models\UserDetail;
use App\Models\City;
use App\Models\State;
use App\Models\Country;
use App\Models\Notification;
use App\Models\OrderDetails;
use App\Models\InfluencerShopProductsImages;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Support\Facades\Redirect;
use App\Models\BusinessTransactionsHistory;
use App\Models\InfluencerTransactionsHistory;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Hash;
use Mail;
use Exception;
use DB;
use App\Services\UserService;
use Illuminate\Support\Facades\Log;


class UserController extends ApiController
{
    public $successStatus = 200;
    public $unauth = 401;
    public $nodata = 404;
    public $badRequest = 422;
    public $validateInputs= 400;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(UserService $userService, Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>$this->validateInputs,'error'=>$validator->errors()], $this->validateInputs);
            }
            $responseData = $userService->login();
            return response()->json($responseData);
        } catch (\Exception $e) {
            $error['status'] = $this->badRequest;
            $error['message']  = $e->getMessage();
            return response()->json(['failure'=>$error]);
        }
    }

    /**
     * login via admin
     *
     * @return \Illuminate\Http\Response
     */
    public function loginAsAdmin(UserService $userService, Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'params' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>$this->validateInputs,'error'=>$validator->errors()], $this->validateInputs);
            }
            $responseData = $userService->loginViaAdmin($request->id, $request->params);
            return response()->json($responseData);
        } catch (\Exception $e) {
            $error['status'] = $this->badRequest;
            $error['message']  = $e->getMessage();
            return response()->json(['failure'=>$error]);
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(UserService $userService, Request $request)
    {
        try{
            $validator = Validator::make($request->all(), array(
                'email' => 'required|unique:users,email',
                'type' => 'required',
                //'referal_code' => 'required',
                'password' => 'required',
                //'c_password' => 'required|same:password',
            ));
            if ($validator->fails()) {
                return response()->json(['status'=>$this->validateInputs,'error'=>$validator->errors()], $this->validateInputs);
            }
            $input = $request->all();
            $responseData = $userService->register($input);
            return response()->json($responseData);
        } catch (\Exception $e) {
            $error['status'] = $this->badRequest;
            $error['message']  = $e->getMessage();
            return response()->json(['failure'=>$error]);
        }
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this-> successStatus);
    }


    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function compInfo(UserService $compService, Request $request)
    {
       try{
            $validator = Validator::make($request->all(), [
                'comp_email' => 'required|unique:comp_details,comp_email',
                'busi_name' => 'required',
                'contact_name' => 'required',
                'busi_number' => 'required',
                //'logo' => 'image:jpeg,png,jpg,gif,svg|max:2048'
            ],['comp_email.unique' => 'The email has already been taken.',
                'comp_email.require' => 'The email has been required.'
                ]);
            if ($validator->fails()) {
                return response()->json(['status'=>$this->validateInputs,'error'=>$validator->errors()], $this->validateInputs);
            }
            $input = $request->all();
            $responseData = $compService->compInfoService($input);
            return response()->json($responseData);
        } catch (\Exception $e) {
            $error['status'] = 500;
            $error['message']  = $e->getMessage();
            return response()->json(['failure'=>$error]);
        }
    }
    /**
    @OA\Post(
    path="/forgot-password",
    tags={"User"},
    summary="Send otp to user through mail in forget case",
    operationId="forget-otp",
    security={
    {"bearerAuth": {}},
    },

    @OA\Parameter(
    name="email",
    in="query",
    required=true,
    @OA\Schema(
    type="string"
    )
    ),
    @OA\Response(
    response=200,
    description="Success"
    ),
    @OA\Response(
    response=401,
    description="Unauthorized"
    ),
    @OA\Response(
    response=400,
    description="Invalid request"
    ),
    @OA\Response(
    response=404,
    description="not found"
    ),
    )
     */
    /**
     * Send ForgetPassword Otp.
     *
     * @return \Illuminate\Http\Response
     */

    public function forgotPasswordOtp(Request $request)
    {
        ini_set('max_execution_time', -1);
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|max:255',
            ]);

            if ($validator->fails()) {

                return response()->json(['status'=>$this->validateInputs,'error'=>$validator->errors()], $this->validateInputs);
            } else {

                $email = $request->email;
                $count = User::select('id')->where('email', '=', $email)->count();
                if ($count > 0) {
                   // $otpCode = Helper::randomOtp();
                    $otpCode = mt_rand(100000,999999);
                    $otpType = "forget-password";
                    $data = array(
                        'type' => $otpType,
                        'email' => $request->email,
                        'otp' => $otpCode,
                    );
                    // $otpId = Otp::create($data);
                    // $encryptedId = encrypt($otpId->id);
                    // $req_email = $request->email;
                    // $mailData = array('subject' => 'OTP', 'email' => $request->email, 'otp' => $otpCode);
                    // $this->sendSMTPMail('emails.sendOtp', $mailData);

                    try{
                    $otpId = Otp::create($data);
                    $encryptedId = encrypt($otpId->id);

                    //Mail for Otp

                   // $mailData = array('subject' => 'OTP', 'email' => $request->email, 'otp' => $otpCode,);

		          $req_email = $request->email;
                  $details = User::where('email' , $req_email)->first();
                  $username = $details->name;


                    Mail::send('emails.sendOtp', [ 'name' => $username ,'msg'=>$otpCode ],
                function ($m) use ($req_email){
                $m->from(config('mail.from.address'),config('app.name'));
                $m->to($req_email)->subject('OTP');
            });
                    } catch(\Exception $e){
			    //$responseData['success'] = $this->badRequest;


                        $responseData['message'] = 'Some thing wrong to send email.';
                        $responseData['user'] = array(
                            'otp_id' =>  $encryptedId,
                            'email' => $email,
                            'otp' => $otpCode
                        );
                        return response()->json(['failure'=> $responseData]);
                    }
                    $responseData['user'] = array(
                        'otp_id' =>  $encryptedId,
                        'email' => $email,
                    );
                    $responseData['success'] = $this->successStatus;
                    $responseData['message'] = 'Check your mail to get OTP';
                    return response()->json($responseData);
		} else {

		      return response()->json(['code'=>400, 'message' => 'This email address could not be found. Please try again.'],400);
                 //   $error['status'] = $this->badRequest;
                 //   $error['message']  = 'This email address could not be found. Please try again.';
                 //   return response()->json(['failure'=>$error]);
                }
            }
        } catch (\Exception $e) {
            $error['status'] = 500;
            $error['message']  = $e->getMessage();
            return response()->json(['failure'=>$error]);
        }
    }



    public function OtpVerify(Request $request){

	 try {
                    $decryptId = decrypt($request->otp_id);
                } catch (\Exception $e) {
                    $error['status'] = $this->badRequest;
                    $error['message']  = 'Please enter valid otp id';
                    return response()->json(['failure'=>$error]);

                }
                $email = $request->email;
		$otpCode = $request->otp;

                $data = Otp::where(array('id' => $decryptId, 'otp' => $otpCode, 'email' => $email))->first();

		if (isset($data->id)) {
		$response['status'] = 'Success';
		$response['code'] = $this->successStatus;
                $response['message'] = 'Otp verified successfully.';

                $delrecord = Otp::find($decryptId);
               // $delrecord->delete();
                return response()->json($response);
		}
		else{
			return response()->json(['code'=>400, 'message' => 'OTP is invalid.'],400);
			//$response['status'] = 'failure';
                       // $response['code'] = $this->badRequest;
                        //$response['message'] = 'OTP is expired.Please resend and try again.';
		//	return response()->json($response);
		}
    }

    /**
    @OA\Post(
    path="/forget-password/verify/otp",
    tags={"User"},
    summary="Change pawssword at forget pawssword case.",
    operationId="rest-forget-pass",
    security={
    {"bearerAuth": {}},
    },
    @OA\Parameter(
    name="otp_id",
    in="path",
    required=true,
    @OA\Schema(
    type="string"
    )
    ),
    @OA\Parameter(
    name="otp_code",
    in="path",
    required=true,
    @OA\Schema(
    type="string"
    )
    ),
    @OA\Parameter(
    name="email",
    in="path",
    required=true,
    @OA\Schema(
    type="string"
    )
    ),
    @OA\Parameter(
    name="newpassword",
    in="path",
    required=true,
    @OA\Schema(
    type="string"
    )
    ),
    @OA\Parameter(
    name="confirm_password",
    in="path",
    required=true,
    @OA\Schema(
    type="string"
    )
    ),
    @OA\Response(
    response=200,
    description="Success"
    ),
    @OA\Response(
    response=401,
    description="Unauthorized"
    ),
    @OA\Response(
    response=400,
    description="Invalid request"
    ),
    @OA\Response(
    response=404,
    description="not found"
    ),
    )
     */
    /**
     * Reset Pawssword for forget pws case api.
     *
     * @return \Illuminate\Http\Response
     */
    public function forgotPasswordOtpVerify(Request $request)
    {
        try {
              // Otp validate
                try {
                    $decryptId = decrypt($request->otp_id);
                } catch (\Exception $e) {
                    // return $this->returnJsonResponse('failure', [
                    //     'otp_id' => ["Please enter valid otp id"]
                    // ], 'Please enter valid otp id.', 'not_found');
    //                $error['status'] = $this->badRequest;
  //                  $error['message']  = 'Please enter valid otp id';
//		    return response()->json(['failure'=>$error]);
		    return response()->json(['code'=>400, 'message' => 'Please enter valid OTP ID.'],400);

                }
                $email = $request->email;

                $data = Otp::where(array('id' => $decryptId, 'email' => $email))->first();
                if (isset($data->id)) {
                    //Otp expair time Check.
                    $expire = Helper::expireOtp($data);
                    $response = array();
                    if ($expire == True) {

                        $newPws = bcrypt($request->password);
                        $updateData = array(
                            'password' => $newPws,
                            'is_verified' => 1,
                            'updated_at' => date('Y-m-d H:i:s'),
                        );
                        User::where(['email' => $email])->update($updateData);
                        $userId = User::select('id')->where(['email' => $email])->first();
                        //Mail for Pws Update.
                        $mailData = array('subject' => 'Change pwssword', 'email' => $email,);
                        //$this->sendSMTPMail('changepws', $mailData);
                        //LoginLog::where([['user_id', '=', $userId->id], ['wrong_atempt', '!=', null]])->delete();
                        $response['status'] = 'success';
                        $response['code'] = $this->successStatus;
                        $response['message'] = 'Password changed successfully.';
                    } else {
                     //   $response['status'] = 'failure';
                     //   $response['code'] = $this->badRequest;
		     //  $response['message'] = 'OTP is expired.Please resend and try again.';
			return response()->json(['code'=>400, 'message' => 'OTP is expired. Please try again.'],400);
                    }
                    $delrecord = Otp::find($decryptId);
                    $delrecord->delete();
                    return response()->json($response);
                } else {
                    $error['status'] = $this->badRequest;
                    $error['message']  = "Invalid inputs.";
		   // return response()->json(['failure'=>$error]);
		    return response()->json(['code'=>400, 'message' => 'Invalid inputs.'],400);
                }
        } catch (\Exception $e) {
            //$error['status'] = 500;
           // $error['message']  = $e->getMessage();
	   // return response()->json(['failure'=>$error]);
	    return response()->json(['code'=>400, 'message' => 'Internal server error.'],400);
        }
    }

    /**
    @OA\Post(
    path="/reset-password",
    tags={"User"},
    summary="Reset Pawssword by inter new and conform pws",
    operationId="rest-pwss",
    security={
    {"bearerAuth": {}},
    },
    @OA\Parameter(
    name="otp_id",
    in="path",
    required=true,
    @OA\Schema(
    type="string"
    )
    ),
    @OA\Parameter(
    name="otp_code",
    in="path",
    required=true,
    @OA\Schema(
    type="string"
    )
    ),
    @OA\Parameter(
    name="newpassword",
    in="path",
    required=true,
    @OA\Schema(
    type="string"
    )
    ),
    @OA\Parameter(
    name="confirm_password",
    in="path",
    required=true,
    @OA\Schema(
    type="string"
    )
    ),
    @OA\Response(
    response=200,
    description="Success"
    ),
    @OA\Response(
    response=401,
    description="Unauthorized"
    ),
    @OA\Response(
    response=400,
    description="Invalid request"
    ),
    @OA\Response(
    response=404,
    description="not found"
    ),
    )
     */
    /**
     * Reset Pawssword api.
     *
     * @return \Illuminate\Http\Response
     */

    public function resetPassword(Request $request)
    {
        //try {
            // Validate requests
            $validator = Validator::make($request->all(), [
                'old_pws' => [
                    'required',
                ],
                'newpassword' => [
                    'required',
                    'min:8',
                    'max:9',
                ],

                'confirm_password' => [
                    'required',
                    'min:8',
                    'max:9',
                    'same:newpassword',
                ],
            ]);

            if ($validator->fails()) {

                return response()->json(['status'=>$this->validateInputs,'error'=>$validator->errors()], $this->validateInputs);
            } else { //Hash::make($request->password)


                $userdetails = Auth::user();
                $email =  $userdetails['email'];
                $data = User::select('password')->where(['id' => $userdetails['id']])->first();

                if (!empty($data)) {

                    if (Hash::check($request->old_pws, $data->password)) {
                        $updateData = array(
                            'password' => bcrypt($request->confirm_password),
                            'updated_at' => date('Y-m-d H:i:s'),
                        );
                        User::where(['id' => $userdetails['id']])->update($updateData);
                        //Mail for Pws Update.
                        //$mailData = array('subject' => 'Change pwssword', 'email' => $email,);
                        //$this->sendSMTPMail('changepws', $mailData);
                        $response['status'] = 'success';
                        $response['code'] = 200;
                        $response['message'] = 'Password changed successfully.';
                    } else {
                        $response['status'] = $this->badRequest;
                        $response['message'] = 'Your old password was not correct. Please try again, or reset from the sign in page.';
                    }
                    return response()->json($response);
                } else {
                        $response['status'] = $this->badRequest;
                        $response['message'] = 'Invalid user.';
                    return response()->json($response);
                }
            }
        // } catch (\Exception $e) {
        //     $error['status'] = 500;
        //     $error['message']  = $e->getMessage();
        //     return response()->json(['failure'=>$error]);
        // }
    }
    /**
     * Resend Otp.
     * @param  int  $otp_id
     * @return \Illuminate\Http\Response`
     */

    public function resendOtp(Request $request){
        try{
            // Generate passport token for request authencation
            $validator = Validator::make($request->all(), [
                'otp_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->returnValidationIssues($validator->errors());
            } else {
                // Otp validate
                try {
                    $decryptId = decrypt($request->otp_id);
                } catch (Exception $e) {
                    return $this->returnJsonResponse('failure', [
                        'otp_id' => ["Please Enter Valid Otp Id"]
                    ], 'Please Enter Valid Otp Id.', 'not_found');
                }
                // update otp
                $otpCode = Helper::randomOtp();
                //$encryptedOtp = encrypt($otpCode);
                $data = Otp::where(array('id' => $decryptId))->first();
                if($data){
                    $data->otp=$otpCode;
                    $data->updated_at=date('Y-m-d H:i:s');
                    $data->save();

                }else{
                    $dataCreationRes['status'] = 'failure';
                    $dataCreationRes['code'] = 401;
                    $dataCreationRes['otp_id'] = $request->otp_id;
                    $dataCreationRes['message'] = 'OTP not found.';

                return $this->returnJsonResponse($dataCreationRes['status'], $dataCreationRes['otp_id'], $dataCreationRes['message'], $dataCreationRes['code']);
                }

                $userDetails = UserTemp::where(['otp_id' => $decryptId])->orderBy('id', 'DESC')->first();
                $otpType = "registration";
                //Mail for Otp
                $mailData = array('subject' => 'OTP', 'email' => $userDetails->email, 'otp' => $otpCode,);
                $this->sendSMTPMail('sendotp', $mailData);
                $dataCreationRes['status'] = 'success';
                    $dataCreationRes['code'] = 200;
                    $dataCreationRes['otp_id'] = array('otp_id'=>$request->otp_id);
                    $dataCreationRes['message'] = 'OTP has been send successfully.';
                return $this->returnJsonResponse($dataCreationRes['status'], $dataCreationRes['otp_id'], $dataCreationRes['message']);
            }

        }catch(\Exception $e){
            return $this->returnJsonResponse('failure', 500, $e->getMessage(), 'bad_request');
        }
    }

    /**
    @OA\Get(
    path="/user/logout",
    tags={"User"},
    summary="Use For User Logout",
    operationId="user-logout",
    security={
    {"bearerAuth": {}},
    },
    @OA\Response(
    response=200,
    description="Success"
    ),
    @OA\Response(
    response=401,
    description="Unauthorized"
    ),
    @OA\Response(
    response=400,
    description="Invalid request"
    ),
    @OA\Response(
    response=404,
    description="not found"
    ),
    )
     */
    /**
     * Logout api.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        try {
            //login user details
		$user = Auth::user();

//$Query = "SELECT L.login_log_id  FROM pro_login_log as L  WHERE L.user_id = $user->id AND login_log_id IN(SELECT MAX(login_log_id) FROM pro_login_log GROUP BY user_id) AND L.login_at IS NOT NULL";

            //$result = DB::select(DB::raw($Query));
            // $logData = array();
            // foreach ($result as $key => $value) {
            //     $logData['login_log_id'] = $value->login_log_id;
            // }

            // Revoke current token for the user
            Auth::user()->token()->revoke();

            // Delete current token for the user
            Auth::user()->token()->delete();
            //Logout log
            // $dataLogout = array(
            //     'login_till' => date('Y-m-d H:i:s'),
            // );
            //LoginLog::where(array('user_id' => $user->id, 'login_log_id' => $logData['login_log_id']))->update($dataLogout);

            if (true) {
                //return $this->returnJsonResponse('success', 2, 'You have been logged out successfully.');
                $responseData['success'] = $this->successStatus;
                $responseData['message'] = 'You have been logged out successfully.';
                return response()->json($responseData);
            } else {
                //return $this->returnJsonResponse('failure', 2, 'Something went wrong.', 'bad_request');
                $responseData['success'] = $this->badRequest;
                $responseData['message'] = 'Something went wrong.';
                return response()->json(['failure'=> $responseData]);

            }
        } catch (\Exception $e) {
            $error['status'] = 500;
            $error['message']  = $e->getMessage();
            return response()->json(['failure'=>$error]);
        }
    }

      /**
     * Send Mail Function.
     *
     */
    function sendSMTPMail($view, $mailData)
    {

        if (env('SEND_MAIL') == false) return true;


        Mail::send($view, ['mailData' =>$mailData], function ($message) use ($mailData) {
            $message->to($mailData['email'])
                ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                ->subject($mailData['subject'] . ' - ' . env('MAIL_FROM_NAME'));
        });
    }

    public function getBusinessDetails( Request $request )
    {
        try{
            $user = Auth::user();
            //check user is active (email is verified)
            if($user){
                $company = Company::where('user_id',$user->id)->first();
                if($company){
                    $city = City::where('id',$company->city)->select('id','name','state_id')->first();
                    $state = State::where('id',$company->state)->select('id','name','country_id')->first();
                    $country = Country::where('id',$company->country)->select('name','id')->first();
                    $user['city'] = $city;
                    $user['country'] = $country;
                    $user['state'] = $state;
                    $user['bussiness_name'] = $company->busi_name;
                    $user['contact_name'] = $company->contact_name;
                    $user['business_number'] = $company->busi_number;
                    $user['description'] = $company->description;
                    $user['comp_email'] = $company->comp_email;
                    $user['logo'] = $company->logo;
                    $user['building'] = $company->building;
                    $user['area'] = $company->area;
                    $user['pincode'] = $company->pincode;
                    return response()->json(['status' => true, 'data' => $user,'message'=>'Business details fetched successfully'], 200,[],JSON_NUMERIC_CHECK);

                }else{
                    return response()->json(['status' => true,'message'=>'No company found'], 400);
                }
            }else{
                return response()->json(['status' => false,'message'=>'No authorized user'], 401);
            }
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],401);
        }

    }

    public function editBusinessDetails(Request $request)
    {
        try{

            $data = $request->all();
            $validator = Validator::make($data, [
                'comp_busi_name' => 'required | string',
                'comp_contact_name' => 'required',
                'comp_busi_number' => 'required',
                'comp_logo' => 'required',
                'comp_country' => 'required',
                'comp_state' => 'required',
                'comp_city' => 'required',
                'comp_area' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()], 400);
            }
            $user_id = Auth::id();
            $comp_details = Company::where('user_id',$user_id)->first();
            if($comp_details){
                $comp_details->busi_name = $data['comp_busi_name'];
                $comp_details->contact_name = $data['comp_contact_name'];
                $comp_details->busi_number = $data['comp_busi_number'];
                $comp_details->logo = $data['comp_logo'];
                $comp_details->country = $data['comp_country'];
                $comp_details->state = $data['comp_state'];
                $comp_details->city = $data['comp_city'];
                $comp_details->area = $data['comp_area'];
                $comp_details->building = $data['comp_building'];
                $comp_details->description = $data['comp_description'];
                $comp_details->save();
                return response()->json(['status' => true, 'message'=>'Business details updated successfully'], 200,[],JSON_NUMERIC_CHECK);
            }else{
                return response()->json( array( 'status'=>false,'message'=>'Company not found' ), 404);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'error' => $e->getMessage(),500]);
        }
    }

    public function getSubscriptionsListing( Request $request )
    {
        try{
            $user = Auth::user();
            //check user is active (email is verified)
            if($user){
                $subscriptions = Subscription::all();
                if($subscriptions){

                    return response()->json(['status' => true, 'data' => $subscriptions,'message'=>'subscription fetched successfully'], 200,[],JSON_NUMERIC_CHECK);

                }else{
                    return response()->json(['status' => true,'message'=>'No subscriptions found'], 400);
                }
            }else{
                return response()->json(['status' => false,'message'=>'No authorized user'], 401);
            }
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],401);
        }

    }

    public function purchaseSubscriptionCopy( Request $request )
    {
        $user = Auth::user();
        if( $user ){
            try{
                $data = $request->all();
                $validator = Validator::make($data, [
                    'subscription_id'  => 'required',
                    'phone_number' => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                $subscription_details = Subscription::find($data['subscription_id']);
                if($subscription_details){

                    $user_old_subscription = UserSubscription::where('user_id',$user->id)->where('is_expiry',0)->first();
                    $new_subscription['subscription_id'] = $data['subscription_id'];
                    $new_subscription['user_id'] = $user->id;

                    $subscription_amount = $subscription_details->price;
                    $phone_number = $data['phone_number'];

                    /** Payment **/
                    $mpesaObj = new MpesaController;
                    $tansaction_details = $mpesaObj->customerMpesaSTKPush($subscription_amount,$phone_number);
                    $transactionResponse = json_decode($tansaction_details);
                    if(isset($transactionResponse->errorMessage)){
                        $error_message = array('status'=>false,'type'=>'transaction','message'=>$transactionResponse->errorMessage);
                        return response()->json($error_message,400);
                    }
                    /** End  **/

                    if($user_old_subscription){
                        $user_old_start_date = $user_old_subscription->start_date;
                        $user_old_end_date = $user_old_subscription->end_date;
                        $date1 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user_old_end_date);
                        $today = \Carbon\Carbon::now();
                        $date2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $today);
                        $result = $date1->gt($date2);
                        if($date1->gt($date2)){
                            $new_subscription['start_date'] = $user_old_start_date;
                            $dt = \Carbon\Carbon::create($user_old_end_date);
                            $end_date = $dt->addDays($subscription_details->no_of_days);
                        }else{
                            $new_subscription['start_date'] = $today;
                            $end_date = \Carbon\Carbon::now()->addDays($subscription_details->no_of_days);
                        }
                        $new_subscription['end_date'] = $end_date;
                        $user_old_subscription->is_expiry = 1;
                        $user_old_subscription->save();
                        $notification['description'] = 'Your subscription is renewed sucessfully';
                    }else{
                        $new_subscription['start_date'] = \Carbon\Carbon::now();
                        $new_subscription['end_date'] = \Carbon\Carbon::now()->addDays($subscription_details->no_of_days);
                        $notification['description'] = 'You buyed a new subscription';
                    }

                    $return = UserSubscription::create($new_subscription);
                    // Notification start
                    $notification['user_id'] = $user->id;
                    $notification['request_type'] = 2;
                    $notification['request_id'] = $return->id;
                    Notification::create($notification);
                    // Notification end
                    $transaction_data['user_id'] = $user->id;
                    $transaction_data['transaction_amount'] = $subscription_amount;
                    $transaction_data['tansaction_details'] = $tansaction_details;
                    $transaction_data['request_type'] = 1;
                    $transaction_data['request_id'] = $return->id;
                    $transaction_data['is_active'] = 1;
                    BusinessTransactionsHistory::create($transaction_data);

                    $success_message = array('status'=>true,'message'=>"You are subscribed successfully","data" => $return);
                    return response()->json($success_message,200);
                }else{
                    $error_message = array('status'=>false,'message'=>"No subscriptions found");
                    return response()->json($error_message,400);
                }
            }
            catch( \Exception $e)
            {
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }
        else{
            $error_message = array('status'=>false,'message'=>"Unauthorized");
            return response()->json($error_message,401);
        }

    }

    public function purchaseSubscription( Request $request )
    {
        $user = Auth::user();
        if( $user ){
            try{
                $data = $request->all();
                $validator = Validator::make($data, [
                    'subscription_id'  => 'required',
                    'phone_number' => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                $subscription_details = Subscription::find($data['subscription_id']);
                if($subscription_details){

                    $subscription_amount = $subscription_details->price;
                    $phone_number = $data['phone_number'];

                    $mpesaObj = new MpesaController;
                    $tansaction_details = $mpesaObj->customerMpesaSTKPush($subscription_amount,$phone_number);
                    $transactionResponse = json_decode($tansaction_details);
                    if(isset($transactionResponse->errorMessage)){
                        $error_message = array('status'=>false,'type'=>'transaction','message'=>$transactionResponse->errorMessage);
                        return response()->json($error_message,400);
                    }
                    $transaction_details['user_id'] = $user->id; 
                    $transaction_details['request_type'] = 1;// for subscription
                    $transaction_details['request_id'] = $data['subscription_id'];
                    $transaction_details['phone_number'] = $data['phone_number'];
                    $transaction_details['transaction_id'] = $transactionResponse->MerchantRequestID;
                    $transaction_details['transaction_amount'] = $subscription_amount;
                    $transaction_details['transaction_details'] = $tansaction_details;
                    Transaction::create($transaction_details);
                    return response()->json(['status' =>true, 'message'=>'Request Sent to user','merchant_request_id' => $transactionResponse->MerchantRequestID],200);
                }else{
                    $error_message = array('status'=>false,'message'=>"No subscriptions found");
                    return response()->json($error_message,400);
                }
            }
            catch( \Exception $e)
            {
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }
        else{
            $error_message = array('status'=>false,'message'=>"Unauthorized");
            return response()->json($error_message,401);
        }

    }

    public function executeSubscription($user_id,$subscription_id,$transaction_details){   
        try{
            $subscription_details = Subscription::find($subscription_id);
            if($subscription_details){
                $user_old_subscription = UserSubscription::where('user_id',$user_id)->where('is_expiry',0)->first();
                $new_subscription['subscription_id'] = $subscription_id;
                $new_subscription['user_id'] = $user_id;
                $subscription_amount = $subscription_details->price;

                if($user_old_subscription){
                    $user_old_start_date = $user_old_subscription->start_date;
                    $user_old_end_date = $user_old_subscription->end_date;
                    $date1 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user_old_end_date);
                    $today = \Carbon\Carbon::now();
                    $date2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $today);
                    $result = $date1->gt($date2);
                    if($date1->gt($date2)){
                        $new_subscription['start_date'] = $user_old_start_date;
                        $dt = \Carbon\Carbon::create($user_old_end_date);
                        $end_date = $dt->addDays($subscription_details->no_of_days);
                    }else{
                        $new_subscription['start_date'] = $today;
                        $end_date = \Carbon\Carbon::now()->addDays($subscription_details->no_of_days);
                    }
                    $new_subscription['end_date'] = $end_date;
                    $user_old_subscription->is_expiry = 1;
                    $user_old_subscription->save();
                    $notification['description'] = 'Your subscription is renewed sucessfully';
                }else{
                    $new_subscription['start_date'] = \Carbon\Carbon::now();
                    $new_subscription['end_date'] = \Carbon\Carbon::now()->addDays($subscription_details->no_of_days);
                    $notification['description'] = 'You buyed a new subscription';
                }
                $return = UserSubscription::create($new_subscription);
                // Notification start
                $notification['user_id'] = $user_id;
                $notification['request_type'] = 2;
                $notification['request_id'] = $return->id;
                Notification::create($notification);
                // Notification end
                $transaction_data['user_id'] = $user_id;
                $transaction_data['transaction_amount'] = $subscription_amount;
                $transaction_data['tansaction_details'] = $transaction_details;
                $transaction_data['request_type'] = 1;
                $transaction_data['request_id'] = $return->id;
                $transaction_data['is_active'] = 1;
                BusinessTransactionsHistory::create($transaction_data);
                return $return;
            }else{
                $error_message = array('status'=>false,'message'=>"No subscriptions found");
                return response()->json($error_message,400);
            }
        }
        catch( \Exception $e)
        {
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }

    }


    public function checkTransactionStatus( Request $request )
    {
        $user = Auth::user();
        if( $user ){
            try{
                $data = $request->all();
                $validator = Validator::make($data, [
                    'merchant_request_id'  => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                $user_transaction_detail = Transaction::where('user_id',$user->id)->where('transaction_id',$data['merchant_request_id'])->first();
                if($user_transaction_detail){
                    $transaction_status = $user_transaction_detail->status;
                    $success_message = array('status'=>true,'message'=>"Transaction status fetched","transaction_status" => $transaction_status);
                    return response()->json($success_message,200);
                }else{
                    $error_message = array('status'=>false,'message'=>'No transaction found');
                    return response()->json($error_message,401);
                }
            }
            catch( \Exception $e)
            {
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }
        else{
            $error_message = array('status'=>false,'message'=>"Unauthorized");
            return response()->json($error_message,401);
        }

    }

    public function paymentCallback(Request $request){
            try{

                Log::info("Here i m in call back");
                $data = $request->all();
                Log::info(json_encode($data));
                $result_code = $data['Body']['stkCallback']['ResultCode'];
                $merchant_request_id = $data['Body']['stkCallback']['MerchantRequestID'];
                $user_transaction_detail = Transaction::where('transaction_id',$merchant_request_id)->first();
                if($user_transaction_detail){
                    if($result_code == 1037){
                        $user_transaction_detail->status = 3;
                        $user_transaction_detail->transaction_details = $data;
                        $user_transaction_detail->save();
                        return response()->json(['status' => false, 'message'=>'Request timeout'],401);
                    }elseif($result_code == 0){
                        $subscription_id = $user_transaction_detail->request_id;
                        $user_id = $user_transaction_detail->user_id;
                        $transaction_details = $user_transaction_detail->transaction_details;
                        $return = $this->executeSubscription($user_id,$subscription_id,$transaction_details);
                        $user_transaction_detail->status = 1;
                        $user_transaction_detail->transaction_details = $data;
                        $user_transaction_detail->save();
                        $success_message = array('status'=>true,'message'=>"You are subscribed successfully","data" => $return);
                        return response()->json($success_message,200);
    
                    }elseif($result_code == 1032){
                        $user_transaction_detail->status = 2;
                        $user_transaction_detail->transaction_details = $data;
                        $user_transaction_detail->save();
                        return response()->json(['status' => false, 'message'=>'Request cancelled by user'],401);
                    }elseif($result_code == 1){
                        $user_transaction_detail->status = 4;
                        $user_transaction_detail->transaction_details = $data;
                        $user_transaction_detail->save();
                        return response()->json(['status' => false, 'message'=>'The balance is insufficient for the transaction'],401);
                    }else{
                        $user_transaction_detail->status = 5;
                        $user_transaction_detail->transaction_details = $data;
                        $user_transaction_detail->save();
                        return response()->json(['status' => false, 'message'=>'Case Not Detected'],401);
                    }
                }else{
                    $error_message = array('message'=>"Data not found",'status'=>false);
                    return response()->json($error_message,400);
                }
            }catch( \Exception $e){
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        
    }

    public function dashboard(Request $request){

        $user = Auth::user();
        if($user){
            try{
                $user->load(['businessDetails']);
                $userProduct = User::withCount('businessProducts')->where('id',$user->id)->first();
                if($userProduct){
                    $productsCount = $userProduct->business_products_count;
                }else{
                    $productsCount = 0;
                }
                $user_subscription = UserSubscription::with('subscriptionDetails')->where('user_id',$user->id)->where('is_expiry',0)->orderBy('id', 'DESC')->first();

                $total_earnings  =  DB::table('business_transactions_history')->where('user_id',$user->id)->sum('business_transactions_history.transaction_amount');

                $earning_this_month = DB::table('business_transactions_history')->where('user_id',$user->id)->whereMonth('created_at', \Carbon\Carbon::now()->month)->sum('business_transactions_history.transaction_amount');

                $sold_product_this_month = DB::table('orders_details')->where('business_id',$user->id)->where('status',3)->whereMonth('created_at', \Carbon\Carbon::now()->month)->sum('orders_details.quantity');

                $recent_orders = OrderDetails::with('prductDetails')->where('business_id',$user->id)->where('status',0)->orderBy('id','desc')->limit(5)->get();

                foreach($recent_orders as $order){
                    $actual_product = Product::with('productImages')->where('id',$order->prductDetails->product_id)->first();
                    $order->business_product_detail = $actual_product;
                }

                $top_five_selling_products = Product::with('productImages')->where('user_id',$user->id)->where('total_sale','>',0)->orderBy('total_sale','desc')->limit(5)->get();

                $dashboardDetails = array('user' => $user, 'user_subscription' => $user_subscription, 'products_count' => $productsCount,'total_earnings'=>$total_earnings,'earning_this_month'=>$earning_this_month,'sold_product_this_month'=>$sold_product_this_month,'recent_orders'=>$recent_orders,'top_five_selling_products'=>$top_five_selling_products);

                $success_message = array('message'=>"Dashboard details",'data' => $dashboardDetails,'status'=>true);
                return response()->json($success_message,200);
            }
            catch( \Exception $e)
            {
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function getPaymentHistory(Request $request){

        $user = Auth::user();
        if($user){
            try{
                $input = $request->all();
                $validator = Validator::make($input,[
                    'limit' => 'numeric | nullable',
                    'page'  => 'numeric | nullable',
                ]);

                if($validator->fails()) {
                    $ret = array('success'=>0, 'message'=> $validator->messages()->first());
                    return response()->json($ret);
                }
                $limit = $request->limit ? $request->limit : 10;
                $payment_history = BusinessTransactionsHistory::select('user_id','transaction_amount','request_type','request_id','created_at')->where('user_id',$user->id)->orderBy('id', 'DESC')->paginate($limit)->toArray();

                return response()->json(array('success'=>true,'data'=>$payment_history['data'],'current_page'=>$payment_history['current_page'],'last_page'=>$payment_history['last_page'],'total_results'=>$payment_history['total'],'message'=>"Payment history Listing") ,200,[],JSON_NUMERIC_CHECK);
            }
            catch( \Exception $e)
            {
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function getNotificationListing(Request $request){

        $user = Auth::user();
        if($user){
            try{
                $input = $request->all();
                $validator = Validator::make($input,[
                    'limit' => 'numeric | nullable',
                    'page'  => 'numeric | nullable',
                ]);

                if($validator->fails()) {
                    $ret = array('success'=>0, 'message'=> $validator->messages()->first());
                    return response()->json($ret);
                }
                $limit = $request->limit ? $request->limit : 10;
                $notifications = Notification::where('user_id',$user->id)->orderBy('id','desc')->paginate($limit)->toArray();

                return response()->json(array('success'=>true,'data'=>$notifications['data'],'current_page'=>$notifications['current_page'],'last_page'=>$notifications['last_page'],'total_results'=>$notifications['total'],'message'=>"Notification Listing") ,200,[],JSON_NUMERIC_CHECK);
            }
            catch( \Exception $e){
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function markNotification(Request $request)
    {
        $user = Auth::user();
        if($user){
            try{
                $data = $request->all();
                $validator = Validator::make($data, [
                    'notification_id'  => 'required | exists:notifications,id'
                ]);
                if($validator->fails()){
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                $notification = Notification::findOrFail($data['notification_id']);
                $notification->is_read = 1;
                $notification->save();
                $success_message = array('status'=>true,'message'=>"Notification marked read successfully");
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }else{
            $error_message = array('status'=>false,'message'=>"Unauthorized");
            return response()->json($error_message,401);
        }
    }

    public function getUnreadCount(){

        $user = Auth::user();
        if($user){
            try{
                $count = Notification::where('user_id',$user->id)->where('is_read',0)->count();
                $success_message = array('status'=>true,'message'=>"Unread Count fetched",'unread_count'=>$count);
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function makeOrderPaid(Request $request)
    {
        $user = Auth::user();
        if($user){
            try{
                $data = $request->all();
                $validator = Validator::make($data, [
                    'order_details_id'  => 'required'
                ]);
                if($validator->fails()){
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                $order_details = OrderDetails::findOrFail($data['order_details_id']);
                $order_details->is_paid = 1;
                $order_details->save();
                //business transaction history starts
                $amount = 80/100* $order_details->amount;
                $business_transactions_history['user_id'] = $order_details->business_id;
                $business_transactions_history['transaction_amount'] = $amount;
                $business_transactions_history['request_type'] = 3;
                $business_transactions_history['request_id'] = $order_details->id;
                BusinessTransactionsHistory::create($business_transactions_history);
                //business transaction history ends

                //influencer transaction history starts
                $influencer_transactions_history['user_id'] = $order_details->influencer_id;
                $influencer_transactions_history['transaction_amount'] = 20/100* $order_details->amount;
                $influencer_transactions_history['request_type'] = 3;
                $influencer_transactions_history['request_id'] = $order_details->id;
                InfluencerTransactionsHistory::create($influencer_transactions_history);
                //influencer transaction history ends

                //Notification for business starts
                $notification['user_id'] = $order_details->business_id;
                $notification['request_type'] = 3;
                $notification['request_id'] = $order_details->id;
                $notification['description'] = 'You recieved amount'.$amount;
                Notification::create($notification);
                //Notification for business ends
                $success_message = array('status'=>true,'message'=>"Order status changed successfully");
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }else{
            $error_message = array('status'=>false,'message'=>"Unauthorized");
            return response()->json($error_message,401);
        }
    }

    public function reportDetails(Request $request){
        $user = Auth::user();
        if($user){
            try{
                $data = $request->all();
                $validator = Validator::make($data, [
                    'type'  => 'required'
                ]);
                if($validator->fails()){
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }

                $type = $data['type'];

                if($type=='1'){ // best performing

                
//                    $top_best_products = Product::with('productImages')->where('user_id',$user->id)->where('total_sale','>','0')->orderBy('total_sale','desc')->limit(10)->get();


                    $top_best_products = Product::with('productImages')->where('user_id',$user->id)->where('total_sale','>','0')->orderBy('total_sale','desc')->limit(10)->get();


                    $success_message = array('status'=>true,'message'=>"Product Listing",'data'=>$top_best_products);
                    return response()->json($success_message,200);

                }else if($type=='2'){ // least persorming

                    



                    $least_selling_products = Product::with('productImages')->where('user_id',$user->id)->orderBy('total_sale','asc')->limit(10)->get();


                    $success_message = array('status'=>true,'message'=>"Product Listing",'data'=>$least_selling_products);

                    return response()->json($success_message,200);

                }else if($type=='3'){

                    $saleOrder  = array();
                    $revenue    = array();
                    $salePrice  = 0;
                    $totalOrder = 0;

                    $total_sale_places = OrderDetails::where('business_id',$user->id)->where('status',3)->select(DB::raw('sum(quantity * amount) as total'))->first();

                    if($total_sale_places->total){
                        $salePrice = $total_sale_places->total;
                    }

                    $total_order_places = OrderDetails::where('business_id',$user->id)->where('status','<',3)->select(DB::raw('sum(quantity * amount) as total'))->first();

                    if($total_order_places->total){
                        $totalOrder = $total_order_places->total;
                    }

                    $saleOrder = array('total_order'=>$totalOrder,'total_sale'=>$salePrice);

                    /** Revenue Section **/

                    $year =  date("Y");

                    for ($i = 1; $i<=12; $i++) {

                        $startDate = $year.'-'.$i.'-01';
                        $month = date('M', strtotime($startDate));

                        $totalOrder = 0;
                        $total_order_placed = OrderDetails::where('business_id',$user->id)->where('status','=',3)->select(DB::raw('sum(quantity * amount) as total'))->whereYear('created_at', '=', $year)->whereMonth('created_at', $i)->first();

                        if($total_order_placed->total){
                            $totalOrder = $total_order_places->total;
                        }

                        $data = array("month"=>$month,"income"=>$totalOrder);
                        array_push($revenue,$data);
                    }

                    $success_message = array('status'=>true,'message'=>"Yearly And Monthly Sale",'revenue'=>$revenue,'sale_order'=>$saleOrder);
                    return response()->json($success_message,200);

                }else{
                    $error_message = array('status'=>false,'message'=>"Please provide valid type");
                    return response()->json($error_message,401);
                }
            }catch( \Exception $e){
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }else{
            $error_message = array('status'=>false,'message'=>"Unauthorized");
            return response()->json($error_message,401);
        }
    }


    public function runCronJob(Request $request){
        try{
            $today = \Carbon\Carbon::now();
            $user_subscription = Company::whereDate('last_cron_run_date','!=',$today)->whereHas('userSubscriptions',function ($query) use($today){
                $query->whereDate('end_date',$today->addDays(7));
            })->with('userSubscriptions')->get();

            foreach($user_subscription as $value){
                $push = new PushNotificationController;
                $data['user_id'] = $value->user_id;
                $data['subscription_id']  = $value->userSubscriptions->subscription_id;
                $push->pushNotificationHandler($data,3);
            }
        }catch( \Exception $e){
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }
    }
    



}
