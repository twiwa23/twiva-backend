<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('request_type')->nullable();
            $table->integer('request_id')->comment('subscription id')->nullable();
            $table->string('phone_number')->comment('mpesa number')->nullable();
            $table->string('transaction_id')->nullable();
            $table->float('transaction_amount')->default(0)->nullable();
            $table->text('transaction_details')->nullable();
            $table->integer('payment_gateway_type')->default(1)->comment('1 for mpesa, 2 for other');
            $table->integer('status')->default(0)->comment('0 => processing, 1 => completed ,2 => cancelled ,3 => No response/no action')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
