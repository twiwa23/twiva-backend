<?php
namespace App\Http\Controllers\Api\v1\Buyer;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\InfluencerShopProducts;
use App\Models\InfluencerShop;
use App\Models\Reason;
use App\Models\ContactSupport;
use App\Models\ProductReviews;
use Illuminate\Support\Facades\Auth;
use Validator;


class BuyerProductController extends ApiController 
{ 
    public function __construct()
    {
        if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
            $this->middleware('auth:api');
        }
    }

    public function getProductDetails(Request $request, $infl_id,$prod_id)
    {
        $influencer_id = base64_decode(base64_decode($infl_id));
        $prod_id = base64_decode(base64_decode($prod_id));
        try{
            if(Auth::check()){
                $user_id = Auth::id();
            }else{
                $user_id = 0;
            }
            $input = $request->all();            
            $validator = Validator::make($input,[
                'limit' => 'numeric | nullable',
                'page'  => 'numeric | nullable',
            ]);
            
            if($validator->fails()) {
                $ret = array('success'=>0, 'message'=> $validator->messages()->first());
                return response()->json($ret);
            }
            $limit = $request->limit ? $request->limit : 10;
            $product_details =  InfluencerShopProducts::with('productDetails','influencerProductImages')
            ->withCount(['cart' => function($q) use($infl_id,$user_id){
                $q->where('buyer_id',$user_id);
                $q->where('influencer_id',$infl_id);
            }])
            ->where('id',$prod_id)
            ->where('influencer_id',$influencer_id)
            ->first();
            if($product_details){
                $product_reviews_and_ratings = ProductReviews::with('buyerName:buyer_id,name')->where('product_id',$product_details->product_id)->orderBy('id','desc')->paginate($limit)->toArray();
                $product_reviews_and_ratings = $product_reviews_and_ratings['data'];
            }else{
                $product_reviews_and_ratings = [];
            }

            $success_message = array('status'=>true, 'message'=>"Products details fetched.","data"=>$product_details,'product_reviews_and_ratings'=>$product_reviews_and_ratings);
            return response()->json($success_message,200);
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function viewShopProducts(Request $request ,$infl_id)
    {
        try{
            $ShopDetails = InfluencerShop::where('user_id',$infl_id)->first();
            $input = $request->all();               
            $validator = Validator::make($input,[
                'limit' => 'numeric | nullable',
                'page'  => 'numeric | nullable',
            ]);
            if($validator->fails()) {
                $ret = array('success'=>0, 'message'=> $validator->messages()->first());
                return response()->json($ret);
            }
            $limit = $request->limit ? $request->limit : 10;
            if(Auth::user()){
                $user_id = Auth::id();
            }else{
                $user_id = 0;
            }
            $userShopDetails = InfluencerShopProducts::where('influencer_id',$infl_id)
            ->with('productDetails','influencerProductImages')
            ->withCount(['cart' => function($q) use($infl_id,$user_id){
                $q->where('buyer_id',$user_id);
                $q->where('influencer_id',$infl_id);
            }])
            ->paginate($limit);
            $success_message = array('status'=>true, 'message'=>"Shops detail fetched.",'shop_details' => $ShopDetails,'shop_product_details' => $userShopDetails);
            return response()->json($success_message,200);
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function getAllReasons()
    {
        $user = Auth::user();
        if($user){
            try{
                $reasons = Reason::all(); 
                $success_message = array('status'=>true, 'message'=>"Reasons fetched successfully",'data' => $reasons);
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function contactSupport(Request $request)
    {
        $user = Auth::user();
        if($user){
            try{
                $data = $request->all();
                $validator = Validator::make($data,[
                    'reason_id'  => 'required | exists:reasons,id',
                    'description'  => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                $data['buyer_id'] = $user->id;
                contactSupport::create($data);
                $success_message = array('status'=>true, 'message'=>"Support added successfully");
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }




}