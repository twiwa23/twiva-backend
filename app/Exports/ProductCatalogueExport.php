<?php

namespace App\Exports;

use App\Models\InfluencerShopProducts;
use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductCatalogueExport implements FromCollection,
                                        // FromQuery
                                        WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $id;
    
    public function __construct($id) {
        $this->id = $id;
    }

    public function collection()
    {
        $details = InfluencerShopProducts::whereNotNull('product_price')->whereHas('actualDetail',function ($query){
            $query->where('status',1);
        })->whereHas('actualDetail.user',function ($query){
            $query->where('is_active',1);
        })->whereHas('actualDetail.userSubscription',function ($query){
            $query->whereDate('end_date','>=',\Carbon\Carbon::now());
        })->with('actualDetail','influencerProductImages','productMainImage')->where('influencer_id',$this->id)->get()->toArray();
        $export = [];
        foreach($details as $key => $val){
            $setImg = 0;
            $export[$key]['id'] = isset($val['product_id']) ? $val['product_id']: "";
            $export[$key]['user_id'] = isset($val['influencer_id']) ? $val['influencer_id']: "";
            $export[$key]['comp_id'] = isset($val['actual_detail']['comp_id']) ? $val['actual_detail']['comp_id']: "";
            $export[$key]['category'] = isset($val['actual_detail']['category']) ? $val['actual_detail']['category'] : "";
            $export[$key]['name'] = isset($val['actual_detail']['name']) ? $val['actual_detail']['name'] : "";
            $export[$key]['title'] = isset($val['product_title']) ? $val['product_title'] : "";
            $export[$key]['condition'] = 'New';
            $export[$key]['sku'] = isset($val['actual_detail']['sku']) ? $val['actual_detail']['sku'] : "";
            foreach($val['influencer_product_images'] as $newVal){
                if($newVal['is_cover_pic'] == 1){
                    $setImg = 1;
                    $export[$key]['image_link'] = 'https://api.twiva.co.ke/storage/images/products/'.$newVal['image_path'];
                }
            }
            if($setImg=='0'){
                $export[$key]['image_link'] = 'https://business.twiva.co.ke//images/logo/logo.svg';
            }

            $export[$key]['link'] = $this->generateLink($val['influencer_id'],$val['id']);
            $export[$key]['availability'] = isset($val['actual_detail']['stock']) ? $val['actual_detail']['stock'] : "";
            if($export[$key]['availability'] > 0){
                $export[$key]['availability'] = 'in stock';
            }else{
                $export[$key]['availability'] = 'out of stock';
            }
            $export[$key]['rating'] = isset($val['actual_detail']['rating']) ? $val['actual_detail']['rating'] : "0";
            $export[$key]['created_by'] = isset($val['actual_detail']['created_by']) ? $val['actual_detail']['created_by'] : "NA";
            $export[$key]['status'] = isset($val['actual_detail']['status']) ? $val['actual_detail']['status'] : "Not Active";
            $export[$key]['description'] = isset($val['actual_detail']['description']) ? $val['actual_detail']['description'] : "";
            $export[$key]['specification'] = isset($val['actual_detail']['specification']) ? $val['actual_detail']['specification'] : "";
            $export[$key]['color'] = '';
            if(isset($val['actual_detail']['color_details'])){
                foreach($val['actual_detail']['color_details'] as $newVal){
                    $export[$key]['color'] = $newVal['name'];
                }
            }if(isset($val['actual_detail']['size_details'])){
                foreach($val['actual_detail']['size_details'] as $newVal){
                    $export[$key]['size'] = $newVal['name'];
                }
            }else{
                $export[$key]['size'] = "";
            }
            $export[$key]['stock'] = isset($val['actual_detail']['stock']) ? $val['actual_detail']['stock'] : "0";
            $export[$key]['price'] = isset($val['product_price']) ? $val['product_price'] : "0";
            $export[$key]['price1'] = isset($val['product_price']) ? $val['product_price'] : "0";
            if(isset($val['product_main_image']['image'])){
                $export[$key]['main_image'] = 'https://api.twiva.co.ke/storage/images/products/'.$val['product_main_image']['image'];
            }else{
                $export[$key]['main_image'] = "https://business.twiva.co.ke/images/logo/logo.svg";
            }
            $export[$key]['created_at'] = $val['created_at'];
            $export[$key]['updated_at'] = $val['updated_at'];
            $export[$key]['brand'] = '';
            $export[$key]['gtin'] = '';
            $export[$key]['identifier_exists'] = 'No';
            $export[$key]['google_product_category'] = '';
        }
        return collect($export);
    }

    // public function query()
    // {
    //     return InfluencerShopProducts::with('actualDetail')->get()->pluck('actualDetail');
    // }

    public function generateLink($user_id,$product_id)
    {
        $userId = base64_encode(base64_encode($user_id));
        $productId = base64_encode(base64_encode($product_id));
        /*$server = $_SERVER['HTTP_HOST'] .'/api/v1/buyer/';
        $scheme = !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on' ? 'http' : 'https';
        $base = $scheme.'://'.$server; */

        $link = 'https://buyer.twiva.co.ke/buyer-product-detail.php?in='.$userId.'&pid='.$productId;
        
        //$link = $base.$userId.'/'.$productId.'/details';
        return $link;
    }

    public function headings(): array
    {
        return ["id","user_id", "comp_id", "category" ,"name","title","condition","sku","image_link","link","availability","rating","created_by","status","description","specification","color","size","stock","price","main_image","created_at","updated_at","brand","gtin","identifier_exists","google_product_category"];
    }
}
