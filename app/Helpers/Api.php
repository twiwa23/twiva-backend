<?php



namespace App\Helpers;

class Api
{
    // return error json
    public static function api_create_response($payload, $status, $message) {
        if (is_numeric($payload)) {

            if ($payload == 1)
                $payload = array();
            elseif ($payload == 2)
                $payload = new \stdClass();
        }

        $payload->status = $status;
        $payload->message = $message;
            
        return $payload;
    }
}