<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::group([
    
    'prefix' => 'v2/buyer',
    'namespace' => 'Api\v2\Buyer'
], function () {

    // Route::get('test','ProductController@testFun');

    Route::get('{user_id}/{product_id}/details','BuyerProductController@getProductDetails');
    Route::get('shop/products/{infl_id}','BuyerProductController@viewShopProducts');

    Route::group(['middleware' => 'auth:api'], function(){
        //Cart 
        Route::post('cart/add','CartController@addToCart');
        Route::get('cart/edit/{cart_id}','CartController@editCartItem');
        Route::put('cart/update/{cart_id}','CartController@updateCartItem');
        Route::delete('cart/delete/{cart_id}','CartController@deleteCartItem');
        Route::get('cart/items/listing','CartController@fetchCartItems');
        Route::get('cart/empty','CartController@emptyCart');
        //Shipping
        Route::post('shipping/add-address','ShippingController@addAddress');
        Route::get('shipping/address-listing','ShippingController@fetchAllAddress');
        Route::get('shipping/edit-address/{address_id}','ShippingController@editAddress');
        Route::put('shipping/update-address/{address_id}','ShippingController@updateAddress');
        Route::delete('shipping/delete-address/{address_id}','ShippingController@deleteAddress');
        //Order
        Route::post('order/place-order','OrderController@placeOrder');
        Route::get('order/listing','OrderController@orderListing');
        Route::get('order/details/{order_id}','OrderController@orderDetails');
        //Reviews & Ratings
        Route::post('reviews/add','ProductReviewsController@addReview');
        Route::get('reviews/edit/{review_id}','ProductReviewsController@editReview');
        Route::put('reviews/update/{review_id}','ProductReviewsController@updateReview');
        Route::get('reviews/fetch/{business_product_id}','ProductReviewsController@fetchReviews');
        // Route::get('reviews/list/{review_id}','ProductReviewsController@getAllReviews');
        //Contact Support
        Route::get('reasons/list','BuyerProductController@getAllReasons');
        Route::post('reasons/contact-support','BuyerProductController@contactSupport');

        //Payment Status Check
        Route::post('check-payment-status' , 'OrderController@checkPaymentStatus');
    });

    


});