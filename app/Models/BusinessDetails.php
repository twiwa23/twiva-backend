<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessDetails extends Model
{    
   // use SoftDeletes;

    protected $table = 'business_details';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id','business_detail','owner_name','created_at','updated_at'];
   
}
