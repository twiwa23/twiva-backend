<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfluencerDetail extends Model
{
    protected $table = "influencer_details";
    protected $primaryKey = 'id';   
    protected $fillable = ["dob","gender","user_id","about"];
    
}