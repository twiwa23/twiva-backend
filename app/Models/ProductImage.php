<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //
    protected $table = 'product_image';
    
    protected $primaryKey = 'id';

    protected $fillable = ['product_id', 'image', 'created_at', 'updated_at', 'is_cover_pic'];

    protected $hidden = ['created_at','updated_at'];
}
