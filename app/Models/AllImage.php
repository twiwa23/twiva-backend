<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AllImage extends Model
{
    //
    protected $table = 'all_images';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['prod_id','name','title','image', 'type','created_at','updated_at'];
}
