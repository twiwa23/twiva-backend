<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model
{
    //
    protected $table = 'product_size';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['name','value','status','sort','created_at','updated_at','product_id','size_id'];
}
