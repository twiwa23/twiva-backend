<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// use Mail;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v2',
    'namespace' => 'Api\v2'
], function () {

    Route::get('test-api', 'ProductController@testData');
    Route::post('login', 'UserController@login');

    Route::post('loginAsAdmin', 'UserController@loginAsAdmin');

    Route::post('register', 'UserController@register');
    Route::post('company-information', 'UserController@compInfo');
    Route::post('forgot-password/otp', 'UserController@forgotPasswordOtp');
    Route::post('transaction/confirmation', 'UserController@paymentCallback');
    Route::post('forgot-password/verify/otp', 'UserController@forgotPasswordOtpVerify');
	Route::post('forgot-password/Otp-verify', 'UserController@OtpVerify');
    //universal
    Route::get('country/list', 'WelcomeController@countryList')->name('country.list');
    Route::get('state/list', 'WelcomeController@stateList')->name('state.list');
    Route::get('city/list', 'WelcomeController@cityList')->name('city.list');
    Route::get('state/select', 'WelcomeController@stateSelect')->name('state.select');
    Route::get('city/select', 'WelcomeController@citySelect')->name('city.select');
    Route::get('categories', 'WelcomeController@categoryListing')->name('category.list');
    Route::get('categories-subcategory', 'WelcomeController@categorySubCategoryListing');
    Route::get('sub-categories/{id}', 'WelcomeController@subCategoryListing');
    Route::get('size-color', 'WelcomeController@SizeAndColorList')->name('size.list');
    //    Route::post('add-product', 'ProductController@addProduct');
    Route::post('upload-image', 'ProductController@uploadImage');
    Route::post('upload-images', 'ProductController@uploadImages');
    // all image
    Route::post('add-image', 'WelcomeController@addImages');

    //Cron job
    Route::get('cron/execute', 'UserController@runCronJob');
    



    Route::group(['middleware' => 'auth:api'], function(){
        //User
        Route::post('details', 'UserController@details');
        Route::post('basic-information', 'UserController@basicInfo');
        Route::post('reset-password', 'UserController@resetPassword');
        Route::get('user/logout', 'UserController@logout')->name('user.logout');
        //products
        Route::post('add-product', 'ProductController@addProduct');
        Route::post('add-product-copy', 'ProductController@addProductCopy');
        Route::post('add-product-images', 'ProductController@addProductImages');
        Route::get('download-product-sample', 'ProductController@downloadProductSample');
        Route::post('import-product', 'ProductController@productDataImport');
	    Route::post('import-product-images', 'ProductController@productImageImport');
	    Route::get('fetch-products', 'ProductController@fetchProducts')->name('product.list');
        Route::get('product/{id}', 'ProductController@product_detail')->name('size.list');
        Route::delete('product/{id}', 'ProductController@deleteProduct');
        Route::put('product/{id}', 'ProductController@editProduct');
        Route::put('publish-unpublish', 'ProductController@publishUnpublish');
        //business
        Route::get('get-business-details', 'UserController@getBusinessDetails');
        Route::put('edit-business-details', 'UserController@editBusinessDetails');
        //Subscription
        Route::get('subscriptions/listing', 'UserController@getSubscriptionsListing');
        Route::post('subscriptions/purchase', 'UserController@purchaseSubscription');
        Route::post('subscriptions/check-status', 'UserController@checkTransactionStatus');
        Route::post('subscriptions/purchase-copy', 'UserController@purchaseSubscriptionCopy');
        //Transaction
        Route::post('transaction/check-status', 'UserController@checkTransactionStatus');
        //Dashboard
        Route::get('business/dashboard', 'UserController@dashboard');
        Route::get('business/reports', 'UserController@reportDetails');
        //Orders
        Route::get('business/order-listing', 'ProductController@getOrderList');
        Route::get('business/order-details/{order_id}', 'ProductController@getOrderDetails');
        Route::post('business/orders/change-status', 'ProductController@changeOrderStatus');
        //Payment History
        Route::get('business/payment-history', 'UserController@getPaymentHistory');
        //Notification
        Route::get('business/notifications/listing', 'UserController@getNotificationListing');
        Route::put('business/notifications/mark-read', 'UserController@markNotification');
        Route::get('business/notifications/unread-count', 'UserController@getUnreadCount');
        //admin
        Route::get('admin/payment/delivered', 'UserController@makeOrderPaid');

    });
});

