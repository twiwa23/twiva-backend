<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfluencerShopProductImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_shop_product_images', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('shop_product_id')->comment('Personlized product\'s id');
            $table->foreign('shop_product_id')->references('id')->on('influencer_shop_product');

            $table->string('image_path')->nullable()->comment('Personalized product\'s images');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_shop_product_images');
    }
}
