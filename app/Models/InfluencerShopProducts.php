<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Colors;
use App\Models\Sizes;
use App\Models\Categories;
use App\Models\InfluencerShopProductImages;
use App\Models\ProductImage;

class InfluencerShopProducts extends Model
{
     use SoftDeletes;

    protected $table = "influencer_shop_product";
    protected $dates = ['deleted_at','created_at','updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = ['created_at','updated_at','deleted_at'];

    /**
     * The attributes that should be mass assignable.
     *
     * @var array
     */
    protected $fillable = ['influencer_id','product_id','total_sale','shop_id','product_cover_image','product_title','product_price','product_description'];

    protected $appends = [
        'color_details','size_details','category_details','subcategory_details'
    ];


    /**
     * Get actual product details
     *
     * @var array
     */
    public function actualDetail() {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    /**
     * Get product images
     *
     * @var array
     */
    public function productDetails() {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function productMainImage() {
        return $this->hasOne('App\Models\ProductImage', 'product_id', 'product_id');
    }

    public function influencerProductImages() {
        return $this->hasMany('App\Models\InfluencerShopProductImages', 'shop_product_id', 'id');
    }

    public function productReviewsAndRatings() {
        return $this->hasMany('App\Models\ProductReviews', 'product_id', 'product_id');
    }

    /*public function productStocks(){
        return $this->hasMany('App\Models\ProductStock', 'product_id', 'id');
    } */

    public function getColorDetailsAttribute(){
        if($this->color){
            $color = explode(',',$this->color);
            return Colors::whereIn( 'id',$color )->select('id', 'name', 'hexaval')->get();
        }else{
            return array();
        }
    }

    public function getSizeDetailsAttribute(){
        if($this->size){
            $size = explode(',',$this->size);
            return Sizes::whereIn('id',$size)->select('id', 'name')->get();
        }else{
            return array();
        }
    }

    public function getCategoryDetailsAttribute(){
        return Categories::where('id',$this->category)->first();
    }

    public function getSubcategoryDetailsAttribute(){
        if($this->sub_category){
            $sub_cat = explode(',',$this->sub_category);
            return SubCategories::whereIn('id',$sub_cat)->get();
        }else{
            return array();
        }
    }

    public function cart(){
        return $this->hasOne('App\Models\ShoppingCart','product_id','id');
    }


}