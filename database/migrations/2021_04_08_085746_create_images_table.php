<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('all_images', function (Blueprint $table) {
            $table->id();
            $table->integer('prod_id')->nullable();
            $table->string('title')->nullable();
            $table->string('name')->nullable();
            $table->string('image')->nullable()->unique();
            $table->enum('type', ['1', '2','3'])->nullable() ->comment = '1=logo,2=profile,3=product';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
