<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTemp extends Model
{    
    protected $table = 'user_temp';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['otp_id', 'email','name','surname','password','company','type','plainpws','temp_token','company_id','created_by','role_id','time_zone','country'];

    public function Country() {
        return $this->hasOne('App\Models\Country', 'country_id','country')->select('country_id','country_name');
    }
    public function RoleId() {
        return $this->hasOne('App\Models\Role', 'id','role_id')->select('id','name');
    }
    public function CreatedBy() {
        return $this->hasOne('App\Models\User', 'id','created_by')->select('id','name','surname');
    }
}