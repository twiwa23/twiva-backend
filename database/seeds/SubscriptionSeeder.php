<?php

use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscription')->insert([

            [
                'subscription_title' => 'Free Subscription',
                'type' => 1,
                'price' => 0.00,
                'note' => 'Max 10 products can be added',
                'no_of_days' => 7,
                'max_products' => 10
            ],
            [
                'subscription_title' => 'For one month',
                'type' => 2,
                'price' => 50,
                'note' => 'Max 150 products can be added',
                'no_of_days' => 30,
                'max_products' => 150
            ],
            [
                'subscription_title' => 'For one year',
                'type' => 3,
                'price' => 300,
                'note' => 'Max 2500 products can be added',
                'no_of_days' => 365,
                'max_products' => 2500
            ]

        ]);
    }
}
