
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([
    
    'prefix' => 'v1',
    'namespace' => 'Api\v1'
], function () {
    Route::post('email/status', 'Influencer\UserController@checkIfEmailExists');
    // Route::get('email/verify/{token}', 'UserController@emailVerification');

    // Route::get('email/verify/{id}', 'UserController@verify'); // Make sure to keep this as your route name
    // Route::get('email/resend', 'UserController@resend');   

    Route::post('upload/image','Influencer\UserController@uploadProfileImage');
    Route::get('interests','Influencer\UserController@getCategories');
    Route::get('influencer/api/details','Influencer\UserController@influencerDeatils');
    Route::post('verify/otp','Influencer\UserController@verifyOtp');
    Route::post('resend/otp','Influencer\UserController@resendOtp');
    Route::get('influencer/country-code/list','Influencer\UserController@getCountryCodeList');
    // Route::get('influencer/{user_id}/{product_id}/details','Influencer\UserController@getLinkDetails');

    Route::group( [ 'prefix'=>'register','namespace' => 'Influencer'],function(){
        //Influencer register routes
        // Route::get('influencer','UserController@registerInfluencer');
        Route::post('influencer','UserController@registerInfluencer');
        
        //Business register routes
        // Route::get('business','UserController@registerBusiness');
        // Route::post('business','UserController@registerBusiness');
    });

    Route::prefix('login')->namespace('Influencer')->group(function () {
        Route::post('influencer','UserController@login');
    });

    // Route::prefix('social/login')->namespace('Influencer')->group(function () {
    //     Route::post('influencer','UserController@socialLogin');
    // });

    Route::group( [ 'prefix'=>'influencer','namespace' => 'Influencer'],function(){
        // Route::get('products/export', 'InfluencerProductController@exportProducts');
        Route::get('products/export/{influencer_id}', 'InfluencerProductController@exportProducts');
        //Influencer API's
        Route::group(['middleware' => 'auth:api'], function(){
                //influencer details
                Route::get('details','UserController@influencerDetails');
                //products
                Route::get('products/list','InfluencerProductController@getListableProducts');
                Route::post('products/details','InfluencerProductController@getProductDetails');
                Route::post('products/delete','InfluencerProductController@deleteProduct');
                Route::post('products/personalise/view','InfluencerProductController@personaliseProductView');
                Route::post('products/personalise/edit','InfluencerProductController@personaliseProductEdit');
                Route::post('products/add','InfluencerProductController@addProduct');
                Route::post('products/upload/image','InfluencerProductController@uploadProductImage');

                //shops
                Route::get('shops/list','InfluencerShopController@getListableShops');
                Route::post('shops/add','InfluencerShopController@addShop');
                Route::get('shops/details','InfluencerShopController@viewShopDetails');
                Route::get('shops/view/products/{shop_id}','InfluencerShopController@viewShopProducts');
                Route::post('shops/edit','InfluencerShopController@editShop');
                Route::put('shops/edit/cover-image/{id}','InfluencerShopController@editCoverImage');

                Route::post('post/story','InfluencerShopController@userStoryPost');

                Route::get('stories/history','InfluencerShopController@fetchPostStories');

                
                
                //dashboard
                Route::get('dashboard','UserController@dashboard');
                
                //Social login
                Route::post('social/login','UserController@socialLogin');
                
                //bank Setup
                Route::post('bank/setup','UserController@bankSetup');
                Route::get('bank/details','UserController@bankDetails');
                Route::get('bank/list','UserController@bankList');

                //Change Password
                Route::post('change/password','UserController@changePassword');

                //Edit Profile
                Route::post('edit/profile','UserController@editProfile');
                Route::put('edit/interests','UserController@updateInterests');
                Route::put('update/profile-image','UserController@updateProfileImage');

                //Phone Verification
                //Route::get('country-code/list','UserController@getCountryCodeList');
                Route::post('phone/send-otp','UserController@sendOtpToPhone');
                Route::post('phone/verify-otp','UserController@verifyOtpForPhone');
                Route::post('phone/resend-otp','UserController@resendOtpForPhone');
                //Orders
                Route::get('orders/list','InfluencerProductController@getDeliveredOrdersList');
                //Payment history
                Route::get('payment-history', 'UserController@getPaymentHistory');
                //Earning history
                Route::get('earning-history', 'UserController@getEarningHistory');
        });
    });

    
});
