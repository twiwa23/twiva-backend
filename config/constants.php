<?php


return [

    //User Type
    'user_type' => [
        ['id' => 1, 'name' => 'Bussiness User'],
        ['id' => 2, 'name' => 'Influencer'],
        ['id' => 3, 'name' => 'Other'],
    ],
    //Image Type
    'image_type' => [
        ['id' => 1, 'name' => 'Logo/Profile'],
        ['id' => 2, 'name' => 'Product'],
        ['id' => 3, 'name' => 'Multiple Image'],
    ],
    'otpExpireTime' => 15,
    'tokenExpireTime' => 60,
    'DefaultTimeZone' => 'Asia/Calcutta',
    'DefaultTimeZone' => 'Asia/Calcutta',
    'influencer_points' => 20,
    'business_points' => 2000,

    //'SEND_REACT_BASE_URL' =>'#/set-password/',
    
];