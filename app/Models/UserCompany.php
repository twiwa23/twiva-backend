<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model
{
    // use SoftDeletes;

    protected $table = 'user_companies';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id','comp_id','status','created_at','updated_at'];
   
}
