<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppingCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_cart', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('buyer_id')->comment('Buyer id as foreign key on Users table');
            $table->foreign('buyer_id')->references('id')->on('users');
            $table->unsignedBigInteger('influencer_id')->comment('Influencers id as foreign key on Users table');
            $table->foreign('influencer_id')->references('id')->on('users');
            $table->unsignedBigInteger('business_id')->comment('Business id as foreign key on Users table');
            $table->foreign('business_id')->references('id')->on('users');
            $table->unsignedBigInteger('product_id')->comment('Product id as foreign key on influencer_shop_products table');
            $table->foreign('product_id')->references('id')->on('influencer_shop_product');
            $table->integer('quantity');
            $table->integer('size')->nullable();
            $table->integer('color')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_cart');
    }
}
