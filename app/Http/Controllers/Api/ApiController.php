<?php
/**
 * ApiController
 * 
 * Responsible for handlling api request and response
 * 
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use \App\Helpers\Api;

class ApiController extends Controller
{
    private $statusCodes, $responseStatusCode, $successText, $failureText;
    
    public function __construct() {
        $this->statusCodes = config('api.status_codes');
        $this->tokenName = config('api.TOKEN_NAME');
        $this->successText = config('api.SUCCESS_TEXT');
        $this->failureText = config('api.FAILURE_TEXT');
    }

    protected function returnValidationIssues($errorArr) {
        $response = Api::api_create_response($errorArr, $this->failureText, 'Please enter valid input.');
        return response()->json($response, $this->statusCodes->bad_request);
    }

    
    protected function returnJsonResponse($status, $payload, $message='', $statusCode='success') {
        $response = $this->createApiResponse($status, $payload, $message);  
        return response()->json($response, $this->statusCodes->{$statusCode});
    }


    protected function getFailureResponse($errorArr) {
        $response = Api::api_create_response($errorArr, $this->failureText, 'Please enter valid input.');
        return response()->json($response, $this->statusCodes->bad_request);
    }

    protected function getSuccessResponse($successArr) {
        $response = Api::api_create_response($successArr, $this->failureText, 'Success!.');
        return response()->json($response, $this->statusCodes->bad_request);
    }

    //private function createApiResponse($status, $payload, $message='') {
    private function createApiResponse($payload) {    
        if (is_numeric($payload)) {
            if ($payload == 1) $payload = array();
            elseif ($payload == 2) $payload = new \stdClass();
        }

        // switch ($status) {
        //     case 'success':
        //         $status = $this->successText;
        //         break;

        //     case 'failure':
        //         $status = $this->failureText;
        //         break;
            
        //     default:
        //         $status = $this->failureText;
        //         break;
        // }
        
        return [
            'payload' => $payload,
            // 'meta' => [
            //     'status' => $status,
            //     'message' => $message
            // ]
        ];
    }
}
