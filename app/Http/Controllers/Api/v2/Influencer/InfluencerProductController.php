<?php

namespace App\Http\Controllers\Api\v2\Influencer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InfluencerShop;
use App\Models\InfluencerShopProducts;
use App\Models\InfluencerShopProductImages;
use App\Models\Product;
use App\Models\OrderDetails;
use App\Models\InfluencerDetails;
use App\Models\ProductReviews;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Exports\ProductCatalogueExport;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

// use Validator,Mail,File,URL;

class InfluencerProductController extends Controller
{
    /**
     * Display a listing of the original products.
     *
     * @return \Illuminate\Http\Response
     */
    public function getListableProducts(Request $request)
    {
        $user = Auth::user();
        if( $user ){
            try{
                $input = $request->all();            
                $validator = Validator::make($input,[
                    'limit' => 'numeric | nullable',
                    'page'  => 'numeric | nullable',
                ]);
                if($validator->fails()) {
                    $ret = array('success'=>0, 'message'=> $validator->messages()->first());
                    return response()->json($ret);
                }
                $limit = $request->limit ? $request->limit : 10;
                $influencerShopProduct = InfluencerShopProducts::where('influencer_id',$user->id)->pluck('product_id')->toArray();
                if(count($influencerShopProduct)>0){
                    $products = Product::where('status',1)->whereNotIn('id',$influencerShopProduct);
                }else{
                    $products = Product::where('status',1);
                }
                if($request->has('categories') && $input['categories'] != ""){
                    $categories = explode(',',$input['categories']);
                    $products->whereIn('products.category',$categories);    
                    // $products->whereRaw("FIND_IN_SET($categories[0],products.category)");
                    // foreach($categories as $value){
                    //     $products->orWhereRaw("FIND_IN_SET($value,products.category)");
                    // }                
                }
                if(($request->has('categories') && $input['categories'] != "") && ($request->has('sub_categories') && $input['sub_categories'] != "")){
                    $sub_categories = explode(',',$input['sub_categories']);
                    $products->whereIn('sub_category' , $sub_categories);
                    // $products->whereRaw("FIND_IN_SET($sub_categories[0],products.sub_category)");
                    // foreach($sub_categories as $value){
                    //     $products->orWhereRaw("FIND_IN_SET($value,products.sub_category)");
                    // }
                }
                // if(($request->has('price_min') && $request->has('price_max')) && ($input['price_min'] !== "" && $input['price_max'] !== "")){
                //     $products->whereBetween('price',[$input['price_min'],$input['price_max']]);
                // }
                if($request->has('search') && $input['search'] != ""){
                    $products->where('name','like','%'.$input['search'].'%');
                }
                $products->with(['productImages']);
                if(($request->has('sort_by') && $input['sort_by'] != "")){
                    if($input['sort_by'] == 0){
                        $products->orderBy('price','asc');
                    }else{
                        $products->orderBy('price','desc');
                    }
                }else{
                    $products->orderBy('id','desc');
                }
                $products = $products->paginate($limit)->toArray();
                foreach ($products['data'] as $key => $product) {
                    $product_reviews_and_ratings = ProductReviews::with('buyerName:buyer_id,name')->where('product_id',$product['id'])->orderBy('id','desc')->paginate($limit)->toArray();
                    $products['data'][$key]['product_reviews_and_ratings'] = $product_reviews_and_ratings['data'];
                }
                
                return response()->json(array('success'=>true,'data'=>$products['data'],'current_page'=>$products['current_page'],'last_page'=>$products['last_page'],'total_results'=>$products['total'],'message'=>"Product Listing") ,200,[],JSON_NUMERIC_CHECK);
            }
            catch( \Exception $e)
            {
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }
        else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }

    }

    public function exportProducts($influencer_id) 
    {
        $infl_id = base64_decode(base64_decode($influencer_id));
        $infl_details = InfluencerDetails::where('influencer_id',$infl_id)->first();
        $infl_details->is_shop_listed = 1;
        $infl_details->save();
        return Excel::download(new ProductCatalogueExport($infl_id), 'products_catalogue.csv');
    }

    public function getProductDetails(Request $request)
    {
        $data = $request->all();
        try{
            $rules = array(
                'limit' => 'numeric | nullable',
                'page'  => 'numeric | nullable',
                'product_id' => 'required',
                'shop_id' => 'required'
            );
            $validator = Validator::make($data,$rules);
            if ($validator->fails())
                return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
            $limit = $request->limit ? $request->limit : 10;
            $product_details =  InfluencerShopProducts::with('productDetails','influencerProductImages')->where('product_id',$data['product_id'])->where('shop_id',$data['shop_id'])->first();
            if($product_details){
                $product_reviews_and_ratings = ProductReviews::with('buyerName:buyer_id,name')->where('product_id',$data['product_id'])->orderBy('id','desc')->paginate($limit)->toArray();
                $product_details->product_reviews_and_ratings = $product_reviews_and_ratings['data'];
            }else{
                $product_details->product_reviews_and_ratings = [];
            }
            
            $success_message = array('status'=>true,'message'=>"Products details fetched.","data"=>$product_details);
            return response()->json($success_message,200);
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function deleteProduct(Request $request)
    {
        $data = $request->all();
        try{
            $rules = array(
                "id" => "required|integer"
            );
            $validator = Validator::make($data,$rules);
            if ($validator->fails())
                return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
            $product = InfluencerShopProducts::find($data['id']);
            if($product){
                InfluencerShopProducts::where('id',$data['id'])->delete();
            }else{
                return response()->json(array('message'=> "No record found",'status'=>false) ,400);
            }
            $success_message = array('message'=>"Product deleted.",'status'=>true);
            return response()->json($success_message,200);
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function personaliseProductView(Request $request)
    {
        $data = $request->all();
        try{
            $rules = array(
                'product_id' => 'required'
            );
            $validator = Validator::make($data,$rules);
            if ($validator->fails())
                return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
            $shop = InfluencerShop::where('user_id',Auth::id())->first();
            if($shop){
                $productDetails = InfluencerShopProducts::with('productDetails','influencerProductImages')->where('product_id',$data['product_id'])->where('shop_id',$shop->id)->first();
                // if(empty($productDetails)){
                //     $productDetails = Product::with('productImages')->where('id',$data['product_id'])->first();
                // }
            }else{
                $productDetails = Product::with('productImages')->where('id',$data['product_id'])->first();
            }
            $success_message = array('status'=>true,'message'=>"Product detail fetched.","product_details" => $productDetails);
            return response()->json($success_message,200);
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }

    public function personaliseProductEdit(Request $request)
    {
        $user = Auth::user();
        $date = \Carbon\Carbon::now();
        if($user){
            try{
                $data = $request->all();
                $validator = Validator::make($data, [
                    'shop_id' => 'required',
                    'product_id'    => 'required | exists:products,id',
                    'products[*].image'    => 'required | array',
                    'product_title'    => 'required | string',
                    'product_price'    => 'required | integer',
                    'product_description'    => 'required | string'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
                }
                $influencerShopProduct = InfluencerShopProducts::where('shop_id',$data['shop_id'])->where('product_id',$data['product_id'])->first();
                $influencerShopProduct->product_title = $data['product_title'];
                $influencerShopProduct->product_price = $data['product_price'];
                $influencerShopProduct->product_description = $data['product_description'];
                $influencerShopProduct->save();
                $productImages = $data['product_images'];
                $productImgArray = array();
                foreach ($productImages as $productImage) {
                    InfluencerShopProductImages::where('shop_product_id',$influencerShopProduct->id)->delete();
                    $productImgArray[]= array("shop_product_id" => $influencerShopProduct->id ,'image_path'=>$productImage['image'],'is_cover_pic'=>$productImage['is_cover_pic'],'created_at'=>$date,'updated_at'=>$date);
                    if(count($productImgArray)>0){
                        InfluencerShopProductImages::insert($productImgArray);
                    }   
                }
                $success_message = array('status'=>true,'message'=>"Product Edited Successfully.");
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function addProduct(Request $request)
    {
        $user = Auth::user();
        $date = \Carbon\Carbon::now();
        if($user){
            try{
                $data = $request->all();
                $validator = Validator::make($data, [
                    'shop_id' => 'required',
                    'products'    => 'required | array | min:1',
                    'products[*].shop_id'    => 'required',
                    'products[*].product_id'    => 'required | exists:products,id',
                    'products[*].image'    => 'required | array',
                    'products[*].title'    => 'required | string',
                    'products[*].price'    => 'required | integer',
                    'products[*].description'    => 'required | string'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
                }
                $products = $data['products'];
                foreach($products as $product){
                        
                    $product['shop_id'] =$data['shop_id'];  
                    $product['influencer_id'] = $user->id;  
                    $productImages = $product['product_images'];
                    unset($product['product_images']);
                    $productImgArray = array();
                    $influencerShopProduct = InfluencerShopProducts::create($product);

                    foreach ($productImages as $productImage) {
                        if(isset($productImage['image'])){
                            $productImgArray[]= array("shop_product_id" => $influencerShopProduct->id ,'image_path'=>$productImage['image'],'is_cover_pic'=>$productImage['is_cover_pic'],'created_at'=>$date,'updated_at'=>$date);
                        }
                    }
                    if(count($productImgArray)>0){
                        InfluencerShopProductImages::insert($productImgArray);
                    }                    
                }
                $success_message = array('message'=>"Product added.",'status'=>true);
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function uploadProductImage(Request $request)
    {
        $data = $request->all(); 
        try{
            $rules = array(
                'shop_product_id' => 'required'
            );
            $validator = Validator::make($data,$rules);
            if ($validator->fails())
                return response()->json(['status'=>false,'error'=>$validator->errors()], 400);
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $fileext = $file->getClientOriginalExtension();
                $name = uniqid().'-'.time().'.'.$fileext;
                $destination_folder = public_path('importProduct/images/productImage');
                $file = $file->move($destination_folder, $name);
                $image_url = 'public/images'.$name;
                $res = DB::table('influencer_shop_product_images')->insert([
                    'shop_product_id' => $data['shop_product_id'], 
                    'image_path' => $image_url
                ]);
                $success_message = array('message'=>"Successfully Uploaded",'image_name'=> $name,'image_url' => $image_url,'status'=>true);
                return response()->json($success_message,200);
            }else{
                $error_message = array('message'=>"Please provide valid parameters",'status'=>false);
                return response()->json($error_message,400);
            }
        }
        catch( \Exception $e)
        {
            $error_message = array('error'=>$e->getMessage(),'status'=>false);
            return response()->json($error_message,500);
        }     
    }

    public function getDeliveredOrdersList()
    {
        $user = Auth::user();
        if($user){
            try{
                $orders = OrderDetails::where('influencer_id',$user->id)->where('status',3)->get();
                $success_message = array('status'=>true,'message'=>"Orders details fetched.","data"=>$orders);
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

}
