<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //
    protected $table = 'notifications_new';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = [ 'user_id', 'request_type', 'request_id', 'description', 'is_read',];
}
