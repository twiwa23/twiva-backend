<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetail extends Model
{    
   // use SoftDeletes;

    protected $table = 'user_details';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id','comp_id','country','state','city','area','building','created_at','updated_at'];
   
}
