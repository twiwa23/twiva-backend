<?php

namespace App\app\Services;

//use App\app\Services\BaseService;
use App\User;
use Illuminate\Support\Facades\Auth;

/**
 * Class NotificationService.
 */
class UserService extends BaseService {

    /**
     * User constructor.
     *
     * @param  Registration  $user
     */
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            
            $user = Auth::user();             
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['status'] = true;
            return $success; 
        }else{
            return $success['status'] = false;
        }
    }
}
