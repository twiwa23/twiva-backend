<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sizes extends Model
{
    //
    protected $table = 'sizes';
    protected $id = 'id';
    protected $fillable = ['name','category_id'];
}
