<?php
namespace App\Http\Controllers\Api\v2\Buyer;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\User;
use App\Models\ShippingAddress;
use Validator,Auth;


class ShippingController extends ApiController 
{

    public function addAddress(Request $request)
    {
        $user = Auth::user();
        if($user){
            try{
                $data = $request->all();
                $validator = Validator::make($data,[
                    'first_name'  => 'required',
                    'last_name'  => 'required',
                    'apartment'  => 'required',
                    'locality'  => 'required',
                    'city'  => 'required',
                    'country'  => 'required',
                    'state'  => 'required',
                    'phone_number'  => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                $data['buyer_id'] = $user->id;
                $address = ShippingAddress::create($data);
                $success_message = array('status'=>true, 'message'=>"Address added successfully","address_id"=>$address->id);
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function fetchAllAddress()
    {
        $user = Auth::user();
        if($user){
            try{
    
                $data = ShippingAddress::with('country','state','city')->where('buyer_id',$user->id)->orderBy('id','desc')->get();

                if($data){
                    $success_message = array('status'=>true, 'message'=>"Shipping address listed",'data' => $data);
                    return response()->json($success_message,200);
                }else{
                    return response()->json(['status' => false, 'message' => 'Data not found'],400);
                }
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function editAddress($address_id)
    {
        $user = Auth::user();
        if($user){ 
            try{
                $data = ShippingAddress::find($address_id);
                if($data){
                    $success_message = array('status'=>true, 'message'=>"Address fetched",'data' => $data);
                    return response()->json($success_message,200);
                }else{
                    return response()->json(['status' => false, 'message' => 'Data not found'],400);
                }
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function updateAddress(Request $request, $address_id)
    {
        $user = Auth::user();
        if($user){ 
            try{
                $input = $request->all();
                $validator = Validator::make($input, [
                    'first_name'  => 'required',
                    'last_name'  => 'required',
                    'apartment'  => 'required',
                    'locality'  => 'required',
                    'city'  => 'required',
                    'country'  => 'required',
                    'state'  => 'required',
                    'phone_number'  => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                ShippingAddress::where('id',$address_id)->update($input);
                $success_message = array('status'=>true, 'message'=>"Address updated successful");
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function deleteAddress($address_id)
    {
        try{
            $address = ShippingAddress::find($address_id);
            if($address){
                $address->delete();
            }else{
                return response()->json(['status' => false, 'message' => 'Data not found'],400);
            }
            
            $success_message = array('status'=>true, 'message'=>"Address deleted");
            return response()->json($success_message,200);
        }
        catch( \Exception $e){
            return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
        }
    }
}