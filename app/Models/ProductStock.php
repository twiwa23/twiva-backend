<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    //
    protected $table = 'product_stock';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['product_id','name','value','status','sort','created_at','updated_at'];
}
