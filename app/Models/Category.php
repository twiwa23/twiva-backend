<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'categories_new';
    protected $id = 'id';

    protected $fillable = ['name'];

}
