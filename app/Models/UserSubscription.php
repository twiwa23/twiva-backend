<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Subscription;

class UserSubscription extends Model
{
    protected $table = 'user_subscriptions';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id','subscription_id','start_date','end_date','is_expiry'];

    public function subscriptionDetails() {
        return $this->hasOne('App\Models\Subscription', 'id', 'subscription_id');
    }

    public function userDetails() {
        return $this->hasOne('App\Models\Company', 'user_id', 'user_id');
    }
}
