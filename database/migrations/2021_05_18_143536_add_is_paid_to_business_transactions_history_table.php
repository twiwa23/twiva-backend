<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsPaidToBusinessTransactionsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_transactions_history', function (Blueprint $table) {
            $table->integer('is_paid')->default(0)->after('request_id')->comment('o for unpaid , 1 for paid ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_transactions_history', function (Blueprint $table) {
            //
        });
    }
}
