<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Notifications\PasswordResetNotification;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'type', 'user_type', 'profile', 'referal_code', 'points', 'email_verification_token', 'avtar', 'dob', 'gender', 'description', 'email_verified_at', 'status', 'password', 'country_code', 'phone_number', 'phone_code', 'is_phone_verified', 'contact_person', 'expiry_time', 'is_verified', 'otp', 'is_active', 'remember_token', 'created_at', 'updated_at', 'profile_completed', 'facebook_id', 'instagram_id', 'twitter_id', 'device_id', 'device_type', 'emailStatus', 'refferal_code', 'email_verify', 'is_deleted', 'background_image', 'descriptions', 'business_address', 'country_name', 'other', 'login_status', 'discover_twiva', 'user_from', 'is_user_update_to_new_platform', 'reset_password_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_at','updated_at','deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

     /**
     * Get rest of influencer info
     *
     * @var array
     */
    function InfluencerDetail(){
        return $this->hasOne('App\Models\InfluencerDetail', 'influencer_id', 'id');  
    }
    public function influencer_details() {
        return $this->hasOne('App\Models\InfluencerDetails', 'influencer_id', 'id');
    }

    public function social_connection_details() {
        return $this->hasMany('App\Models\UserSocialConnection', 'influencer_id', 'id');
    }

    public function businessDetails() {
        return $this->hasOne('App\Models\Company', 'user_id', 'id');
    }

    public function businessProducts(){
        return $this->hasMany('App\Models\Product', 'user_id', 'id');
    }


     /**
     * Override canResetPaswword trait's function "sendPasswordResetNotification"
     *
     * @var array
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }
}
