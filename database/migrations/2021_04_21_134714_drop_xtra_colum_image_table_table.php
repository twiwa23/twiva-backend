<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropXtraColumImageTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('product_image', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('name');
            $table->dropColumn('user_id');
            $table->dropColumn('sku');
            $table->dropColumn('created_by');
            $table->integer('is_cover_pic')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_image', function (Blueprint $table) {
            //
        });
    }
}
