<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_reviews', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('buyer_id')->comment('buyer id as foreign key on users table');
            $table->foreign('buyer_id')->references('id')->on('users');
            $table->unsignedBigInteger('product_id')->comment('product id as foreign key on products table');
            $table->foreign('product_id')->references('id')->on('products');
            $table->integer('rating');
            $table->string('review');
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_reviews');
    }
}
