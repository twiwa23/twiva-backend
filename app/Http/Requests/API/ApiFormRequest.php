<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest as LaravelFormRequest;
use Illuminate\Contracts\Validation\Validator;
use \Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

abstract class ApiFormRequest extends LaravelFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules();

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize();

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $statusCode = config('api.status_codes');

        $errors = (new ValidationException($validator))->errors();
        
        $response = api_create_response(
            $errors,
            config('api.FAILURE_TEXT'),
            'Please enter valid input.'
        );
        
        throw new HttpResponseException(
            response()->json($response, $statusCode->bad_request)
        );
    }

    public function response(array $errors){

        $statusCode = config('api.status_codes');
        $response = api_create_response($errors, config('api.FAILURE_TEXT'), 'Please enter valid input.');
        return response()->json($response, $statusCode->bad_request);

        // return response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function expectsJson()
    {
        return true;
    }
}