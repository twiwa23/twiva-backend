<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id')->comment('order id as foreign key on orders table');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->unsignedBigInteger('product_id')->comment('product id as foreign key on influencer_shop_product table');
            $table->foreign('product_id')->references('id')->on('influencer_shop_product');
            $table->integer('quantity');
            $table->integer('size')->nullable();
            $table->integer('color')->nullable();
            $table->float('amount');
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_details');
    }
}
