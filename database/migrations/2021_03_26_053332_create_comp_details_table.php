<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comp_details', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('comp_email');
            $table->string('busi_name');
            $table->string('contact_name')->nullable();
            $table->string('busi_number')->nullable();
            $table->string('description')->nullable();
            $table->string('logo')->nullable()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comp_details');
    }
}
