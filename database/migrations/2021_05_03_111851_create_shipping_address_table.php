<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_address', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('buyer_id')->comment('Buyer id as foreign key on Users table');
            $table->foreign('buyer_id')->references('id')->on('users');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('apartment');
            $table->string('locality');
            $table->integer('city');
            $table->integer('country');
            $table->integer('state');
            $table->integer('postal_code');
            $table->integer('phone_number');
            $table->integer('is_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_address');
    }
}
