<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserStoryPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_story_post', function (Blueprint $table) {
            $table->id();
            $table->integer('influencer_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->string('platform')->nullable()->comment('1 for facebook,2 for twitter. 3 for instagram,4 for youtube');
            $table->integer('is_schedule')->default(0);
            $table->text('image')->nullable();
            $table->string('color')->nullable();
            $table->text('caption')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_story_post');
    }
}
