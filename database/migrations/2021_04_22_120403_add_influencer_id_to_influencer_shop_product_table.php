<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInfluencerIdToInfluencerShopProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('influencer_shop_product', function (Blueprint $table) {
            $table->integer('influencer_id')->nullable()->after('id');
            $table->integer('status')->default(1)->after('product_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('influencer_shop_product', function (Blueprint $table) {
            //
        });
    }
}
