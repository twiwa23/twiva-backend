<?php
namespace App\Http\Controllers\Api\v1\Buyer;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\ShoppingCart;
use App\Models\Product;
use App\Models\InfluencerShopProducts;
use App\User;
use Validator,Auth;


class CartController extends ApiController 
{ 

    public function addToCart(Request $request)
    {
        $user = Auth::user();
        if($user){
            try{
                $data = $request->all();
                $validator = Validator::make($data, [
                    // 'items'    => 'required | array',
                    // 'items[*].buyer_id'      => 'required',
                    'items[*].influencer_id' => 'required',
                    'items[*].business_id'   => 'required',
                    'items[*].product_id'      => 'required',
                    'items[*].quantity' => 'required',
                    'items[*].size'   => 'required',
                    'items[*].color'   => 'required',
               
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                $items = $data['items'];
                foreach($items  as $item){
                    $item['buyer_id'] = $user->id;
                    $product_id = InfluencerShopProducts::where('id',$item['product_id'])->value('product_id');
                    $product_existing_stock = Product::where('id',$product_id)->value('stock');
                    if($item['quantity'] > $product_existing_stock){
                        return response()->json(['status'=>false, 'message' => 'Requested quantity is larger than our stock'],400);
                    }
                    ShoppingCart::create($item);
                }
                
                $success_message = array('status'=>true, 'message'=>"Product added to cart successfully");
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function editCartItem($cart_id)
    {
        $user = Auth::user();
        if($user){
            try{
                $data = ShoppingCart::find($cart_id);
                if($data){
                    $success_message = array('status'=>true, 'message'=>"Item details fetched",'data' => $data);
                    return response()->json($success_message,200);
                }else{
                    return response()->json(['status' => false, 'message' => 'Data not found'],400);
                }
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function updateCartItem(Request $request, $cart_id)
    {
        $user = Auth::user();
        if($user){
            try{
                $input = $request->all();
                $validator = Validator::make($input, [
                    'quantity' => 'required',
                    'size' => 'required',
                    'color' => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                $cart_details = ShoppingCart::where('id',$cart_id)->first();
                $product_id = InfluencerShopProducts::where('id',$cart_details->product_id)->value('product_id');
                $product_existing_stock = Product::where('id',$product_id)->value('stock');
                if($input['quantity'] > $product_existing_stock){
                    return response()->json(['status'=>false, 'message' => 'Requested quantity is larger than our stock'],400);
                }
                ShoppingCart::where('id',$cart_id)->update($input);
                $success_message = array('status'=>true, 'message'=>"Cart item updated");
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
        
    }

    public function deleteCartItem($cart_id)
    {
        $user = Auth::user();
        if($user){
            try{
                $item = ShoppingCart::find($cart_id);
                if($item){
                    $item->delete();
                }else{
                    return response()->json(['status' => false, 'message' => 'Data not found'],400);
                }
                $success_message = array('status'=>true, 'message'=>"Cart item deleted");
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function emptyCart()
    {
        $user = Auth::user();
        if($user){
            try{
                $item = ShoppingCart::where('buyer_id',$user->id)->get();
                if($item){
                    ShoppingCart::where('buyer_id',$user->id)->delete();
                }else{
                    return response()->json(['status' => false, 'message' => 'Data not found'],400);
                }
                
                $success_message = array('status'=>true, 'message'=>"Cart emptied successfully");
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function fetchCartItems()
    {
        $user = Auth::user();
        if($user){
            try{
                $data = ShoppingCart::with('productSize','productColor')->where('buyer_id',$user->id)->get();
                if($data){
                    $success_message = array('status'=>true, 'message'=>"Cart Items fetched",'data' => $data);
                    return response()->json($success_message,200);
                }else{
                    return response()->json(['status' => false, 'message' => 'Data not found'],400);
                }
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }
   



}