<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfluencerShopProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_shop_product', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('shop_id')->comment('Influencer\'s shop id');
            $table->foreign('shop_id')->references('id')->on('influencer_shop');

            $table->unsignedBigInteger('product_id')->comment('Original product id');
            $table->foreign('product_id')->references('id')->on('products');

            $table->string('product_cover_image')->comment('Personalized product\'s cover image')->nullable();
            $table->string('product_title')->comment('Personalized product\'s title')->nullable();
            $table->integer('product_price')->comment('Personalized product\'s price')->nullable();
            $table->string('product_description')->comment('Personalized product\'s description')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_shop_product');
    }
}
