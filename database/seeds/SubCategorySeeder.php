<?php

use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_categories')->insert([

            [
                'name' => 'Bird Cage Bird Baths',
                'category_id' => 1
            ],
            [
                'name' => 'Bird Food',
                'category_id' => 1
            ],
            [
                'name' => 'Bird Ladders & Perches',
                'category_id' => 1
            ],
            [
                'name' => 'Bird Toys',
                'category_id' => 1
            ],
            [
                'name' => 'Cat Food',
                'category_id' => 1
            ],
            [
                'name' => 'Cat Toys',
                'category_id' => 1
            ],
            [
                'name' => 'Dog Apparel',
                'category_id' => 1
            ],
            [
                'name' => 'Dog Beds',
                'category_id' => 1
            ],
            [
                'name' => 'Dog Food',
                'category_id' => 1
            ],
            [
                'name' => 'Aquariums',
                'category_id' => 1
            ],
            [
                'name' => 'Fish Food',
                'category_id' => 1
            ],
            [
                'name' => 'Animals',
                'category_id' => 1
            ],
            [
                'name' => 'Pet Food Containers',
                'category_id' => 1
            ],
            [
                'name' => 'Bicycle Bibs',
                'category_id' => 2
            ],
            [
                'name' => 'Dance Dresses, Skirts & Costumes',
                'category_id' => 2
            ],
            [
                'name' => 'Boxing Shorts',
                'category_id' => 2
            ],
            [
                'name' => 'Martial Arts Shorts',
                'category_id' => 2
            ],
            [
                'name' => 'Baby & Toddler Dresses',
                'category_id' => 2
            ],
            [
                'name' => 'Baby One-Pieces',
                'category_id' => 2
            ],
            [
                'name' => 'Jumpsuits & Rompers',
                'category_id' => 2
            ],
            [
                'name' => 'Leotards & Unitards',
                'category_id' => 2
            ],
            [
                'name' => 'Coats & Jackets',
                'category_id' => 2
            ],
            [
                'name' => 'Vests',
                'category_id' => 2
            ],
            [
                'name' => 'Pants',
                'category_id' => 2
            ],
            [
                'name' => 'Shirts & Tops',
                'category_id' => 2
            ],
            [
                'name' => 'Shorts',
                'category_id' => 2
            ],
            [
                'name' => 'Skirts',
                'category_id' => 2
            ],
            [
                'name' => 'Nightgowns',
                'category_id' => 2
            ],
            [
                'name' => 'Pajamas',
                'category_id' => 2
            ],
            [
                'name' => 'Suits',
                'category_id' => 2 
            ],
            [
                'name' => 'Tuxedos',
                'category_id' => 2
            ],
            [
                'name' => 'Swimwear',
                'category_id' => 2
            ],
            [
                'name' => 'Bras',
                'category_id' => 2
            ],
            [
                'name' => 'Socks',
                'category_id' => 2
            ],
            [
                'name' => 'Undershirts',
                'category_id' => 2
            ],
            [
                'name' => 'Underwear',
                'category_id' => 2
            ],
            [
                'name' => 'Belts',
                'category_id' => 3
            ],
            [
                'name' => 'Sunglasses',
                'category_id' => 3
            ],
            [
                'name' => 'Masks',
                'category_id' => 3
            ],
            [
                'name' => 'Anklets',
                'category_id' => 5
            ],
            [
                'name' => 'Body Jewelry',
                'category_id' => 5
            ],
            [
                'name' => 'Bracelets',
                'category_id' => 5
            ],
            [
                'name' => 'Earrings',
                'category_id' => 5
            ],
            [
                'name' => 'Jewelry Sets',
                'category_id' => 5
            ],
            [
                'name' => 'Necklaces',
                'category_id' => 5
            ],
            [
                'name' => 'Watches',
                'category_id' => 5
            ],
            [
                'name' => 'Candle Making Kits',
                'category_id' => 6
            ],
            [
                'name' => 'Drawing & Painting Kits',
                'category_id' => 6
            ],
            [
                'name' => 'Fabric Repair Kits',
                'category_id' => 6
            ],
            [
                'name' => 'Incense Making Kits',
                'category_id' => 6
            ],
            [
                'name' => 'Jewelry Making Kits',
                'category_id' => 6
            ],
            [
                'name' => 'Toy Craft Kits',
                'category_id' => 6
            ],
            [
                'name' => 'Gift Cards & Certificates',
                'category_id' => 7
            ],
            [
                'name' => 'Fresh Cut Flowers',
                'category_id' => 7
            ],
            [
                'name' => 'Gift Cards & Certificates',
                'category_id' => 7
            ],
            [
                'name' => 'Gift Wrapping',
                'category_id' => 7
            ],
            [
                'name' => 'Gift Bags',
                'category_id' => 7
            ],
            [
                'name' => 'Balloons',
                'category_id' => 7
            ],
            [
                'name' => 'Alphabet Toys',
                'category_id' => 8
            ],
            [
                'name' => 'Baby Activity Toys',
                'category_id' => 8
            ],
            [
                'name' => 'Baby Bouncers & Rockers',
                'category_id' => 8
            ],
            [
                'name' => 'Baby Jumpers & Swings',
                'category_id' => 8
            ],
            [
                'name' => 'Baby Mobiles',
                'category_id' => 8
            ],
            [
                'name' => 'Play Yards',
                'category_id' => 8
            ],
            [
                'name' => 'Brochures',
                'category_id' => 9
            ],
            [
                'name' => 'Trade Show Counters',
                'category_id' => 9
            ],
            [
                'name' => 'Trade Show Displays',
                'category_id' => 9
            ],
            [
                'name' => 'Egg Incubators',
                'category_id' => 10
            ],
            [
                'name' => 'Cattle Feed',
                'category_id' => 10
            ],
            [
                'name' => 'Chicken Feed',
                'category_id' => 10
            ],
            [
                'name' => 'Goat & Sheep Feed',
                'category_id' => 10
            ],
            [
                'name' => 'Mixed Herd Feed',
                'category_id' => 10
            ],
            [
                'name' => 'Pig Feed',
                'category_id' => 10
            ],
            [
                'name' => 'Dental Cement',
                'category_id' => 11
            ],
            [
                'name' => 'Dental Tools',
                'category_id' => 11
            ],
            [
                'name' => 'Dappen Dishes',
                'category_id' => 11
            ],
            [
                'name' => 'Dental Mirrors',
                'category_id' => 11
            ],
            [
                'name' => 'Dental Tool Sets',
                'category_id' => 11
            ],
            [
                'name' => 'Prophy Cups',
                'category_id' => 11
            ],
            [
                'name' => 'Bakery Boxes',
                'category_id' => 12
            ],
            [
                'name' => 'Bus Tubs',
                'category_id' => 12
            ],
            [
                'name' => 'Check Presenters',
                'category_id' => 12
            ],
            [
                'name' => 'Concession Food Containers',
                'category_id' => 12
            ],
            [
                'name' => 'Hospital Curtains',
                'category_id' => 13
            ],
            [
                'name' => 'Automated External Defibrillators',
                'category_id' => 13
            ],
            [
                'name' => 'Patient Lifts',
                'category_id' => 13
            ],
            [
                'name' => 'Stethoscopes',
                'category_id' => 13
            ],
            [
                'name' => 'Clothing Display Racks',
                'category_id' => 14
            ],
            [
                'name' => 'Display Mannequins',
                'category_id' => 14
            ],
            [
                'name' => 'Money Handling',
                'category_id' => 14
            ],
            [
                'name' => 'cash Drawers & Trays',
                'category_id' => 14
            ],
            [
                'name' => 'Cash Registers',
                'category_id' => 14
            ],
            [
                'name' => 'Biochemicals',
                'category_id' => 15
            ],
            [
                'name' => 'Dissection Kits',
                'category_id' => 15
            ],
            [
                'name' => 'Laboratory Chemicals',
                'category_id' => 15
            ],
            [
                'name' => 'Pipettes',
                'category_id' => 15
            ],
            [
                'name' => 'Beakers',
                'category_id' => 15
            ],
            [
                'name' => 'Bullet Proof Vests',
                'category_id' => 16
            ],
            [
                'name' => 'Gas Mask & Respirator Accessories',
                'category_id' => 16
            ],
            [
                'name' => 'Hardhats',
                'category_id' => 16
            ],
            [
                'name' => 'Dust Masks',
                'category_id' => 16
            ],
            [
                'name' => 'Medical Masks',
                'category_id' => 16
            ],
            [
                'name' => 'Camera Lenses',
                'category_id' => 17
            ],
            [
                'name' => 'Lens & Filter Adapter Rings',
                'category_id' => 17
            ],
            [
                'name' => 'Lens Bags',
                'category_id' => 17
            ],
            [
                'name' => 'Lens Caps',
                'category_id' => 17
            ],
            [
                'name' => 'Lens Hoods',
                'category_id' => 17
            ],
            [
                'name' => 'Headphone & Headset Accessories',
                'category_id' => 18
            ],
            [
                'name' => 'Microphone Stands',
                'category_id' => 18
            ],
            [
                'name' => 'Speaker Bags, Covers & Cases',
                'category_id' => 18
            ],
            [
                'name' => 'Bluetooth Transmitters',
                'category_id' => 18
            ],
            [
                'name' => 'FM Transmitters',
                'category_id' => 18
            ],
            [
                'name' => 'Headphones',
                'category_id' => 18
            ],
            [
                'name' => 'Beer',
                'category_id' => 19
            ],
            [
                'name' => 'Liquor & Spirits',
                'category_id' => 19
            ],
            [
                'name' => 'Absinthe',
                'category_id' => 19
            ],
            [
                'name' => 'Brandy',
                'category_id' => 19
            ],
            [
                'name' => 'Bakery',
                'category_id' => 20
            ],
            [
                'name' => 'Bagels',
                'category_id' => 20
            ],
            [
                'name' => 'Bakery Assortments',
                'category_id' => 20
            ],
            [
                'name' => 'Breads & Buns',
                'category_id' => 20
            ],
            [
                'name' => 'Cakes & Dessert Bars',
                'category_id' => 20
            ],
            [
                'name' => 'Cookies',
                'category_id' => 20
            ],
            [
                'name' => 'Bed & Bed Frame Accessories',
                'category_id' => 21
            ],
            [
                'name' => 'Headboards & Footboards',
                'category_id' => 21
            ],
            [
                'name' => 'Mattresses',
                'category_id' => 21
            ],
            [
                'name' => 'Armoires & Wardrobes',
                'category_id' => 21
            ],
            [
                'name' => 'Dressers',
                'category_id' => 21
            ],
            [
                'name' => 'Nails',
                'category_id' => 22
            ],
            [
                'name' => 'Rivets',
                'category_id' => 22
            ],
            [
                'name' => 'Screws',
                'category_id' => 22
            ],
            [
                'name' => 'Washers',
                'category_id' => 22
            ],
            [
                'name' => 'Armatures, Rotors & Stators',
                'category_id' => 23
            ],
            [
                'name' => 'Ballasts & Starters',
                'category_id' => 23
            ],
            [
                'name' => 'Carbon Brushes',
                'category_id' => 23
            ],
            [
                'name' => 'Electrical Motors',
                'category_id' => 23
            ],
            [
                'name' => 'Breathalyzers',
                'category_id' => 24
            ],
            [
                'name' => 'Body Fat Analyzers',
                'category_id' => 24
            ],
            [
                'name' => 'Blood Glucose Meters',
                'category_id' => 24
            ],
            [
                'name' => 'Activity Monitors',
                'category_id' => 24
            ],
            [
                'name' => 'Cardboard Cutouts',
                'category_id' => 25
            ],
            [
                'name' => 'Clocks',
                'category_id' => 25
            ],
            [
                'name' => 'Door Mats',
                'category_id' => 25
            ],
            [
                'name' => 'Finials',
                'category_id' => 25
            ]

        ]);
    }
}
