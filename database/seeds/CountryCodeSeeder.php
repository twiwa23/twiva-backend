<?php

use Illuminate\Database\Seeder;

class CountryCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $path = base_path() . '/database/seeds/countries-1.sql';
        $sql = file_get_contents($path);
        DB::unprepared($sql);
    }
}
