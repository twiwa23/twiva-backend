<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfluencerTransactionsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_transactions_history', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->float('transaction_amount')->nullable();
            $table->integer('request_type')->comment('1 for order, 2 for subscription, 3 for payment')->nullable();
            $table->integer('request_id')->default(0)->comment('unique id for respective table');
            $table->integer('is_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_transactions_history');
    }
}
