<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InfluencerDetails extends Model
{
    use SoftDeletes;

    protected $table = "influencer_detail_new";
    protected $primaryKey = 'influencer_id';
    public $incrementing = false;
    protected $dates = ['deleted_at','created_at','updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ 'influencer_id','created_at','updated_at','deleted_at'];

    /**
     * The attributes that should be mass assignable.
     *
     * @var array
     */
    protected $fillable = ['influencer_id','name','dob','gender','description','interests','profile_image','is_shop_created'];
    
    /**
     * Get rest of user info
     *
     * @var array
     */
    public function user() {
        return $this->hasOne('App\User', 'id', 'influencer_id');
    }
}
