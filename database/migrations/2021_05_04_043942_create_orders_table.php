<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('buyer_id')->comment('Buyer id as foreign key on Users table');
            $table->foreign('buyer_id')->references('id')->on('users');
            $table->unsignedBigInteger('influencer_id')->comment('Influencers id as foreign key on Users table');
            $table->foreign('influencer_id')->references('id')->on('users');
            $table->unsignedBigInteger('business_id')->comment('Business id as foreign key on Users table');
            $table->foreign('business_id')->references('id')->on('users');
            $table->unsignedBigInteger('address_id');
            $table->integer('order_status')->default(1)->comment('1 for order placed, 2 for dispatched, 3 for shipped,4 for delivered');
            $table->float('order_amount')->nullable();
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
