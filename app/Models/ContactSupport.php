<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactSupport extends Model
{
    //
    protected $table = 'contact_support';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['buyer_id','reason_id','description'];
}
