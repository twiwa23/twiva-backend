<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProductReviews extends Model
{
     use SoftDeletes;

    protected $table = "product_reviews";
    protected $dates = ['deleted_at','created_at','updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = ['created_at','updated_at','deleted_at'];

    /**
     * The attributes that should be mass assignable.
     *
     * @var array
     */
    protected $fillable = ['buyer_id','product_id','rating','review'];

    public function buyerName(){
       
        return $this->hasOne('App\Models\BuyerDetails', 'buyer_id', 'buyer_id');
    }

}