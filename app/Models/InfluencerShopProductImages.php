<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfluencerShopProductImages extends Model
{
    //
    protected $table = 'influencer_shop_product_images';
    
    protected $primaryKey = 'id';

    protected $fillable = ['shop_product_id', 'image_path', 'created_at', 'updated_at', 'is_cover_pic'];

    protected $hidden = ['created_at','updated_at'];
}
