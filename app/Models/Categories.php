<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    //
    protected $table = 'categories';
    protected $id = 'id';

    protected $fillable = ['name'];

    //subcategories
    public function subcategories(){
        return $this->hasMany('App\Models\SubCategories', 'category_id', 'id');
    }

}
