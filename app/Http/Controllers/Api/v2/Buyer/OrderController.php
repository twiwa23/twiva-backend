<?php
namespace App\Http\Controllers\Api\v2\Buyer;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\MpesaController;
use App\Http\Controllers\PushNotificationController;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductReviews;
use App\Models\OrderDetails;
use App\Models\ShoppingCart;
use App\Models\ShippingAddress;
use App\Models\InfluencerShopProducts;
use App\Models\InfluencerShopProductImages;
use App\Models\Notification;
use Validator,Auth;
use App\Models\InfluencerTransactionsHistory;
use App\Models\BusinessTransactionsHistory;
use App\Models\Transaction;

class OrderController extends ApiController 
{ 

    public function placeOrder(Request $request)
    {
        $user = Auth::user();
        if($user){
            try{
                $data = $request->all();
                $validator = Validator::make($data, [
                    'phone_number' => 'required',
                    'address_id'   => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }

                $products = ShoppingCart::where('buyer_id',$user->id)->get();
                if(count($products)=='0'){
                    return response()->json(['status'=>false,'error'=>'Cart is Empty'], 400);
                }
                $address_id = $data['address_id'];
                if($address_id=='0'){
                    return response()->json(['status'=>false, 'message' => 'Invalid Address'],400);
                }
                $order['order_amount'] = 0;
                foreach($products as $key => $item){
                    // check stock availability
                    $product_id = InfluencerShopProducts::where('id',$item['product_id'])->value('product_id');
                    $product_existing_stock = Product::where('id',$product_id)->value('stock');
                    $product[$key]['product_id'] = $product_id;
                    $product[$key]['quantity'] = $item->quantity;
                    $product[$key]['product_existing_stock'] = $product_existing_stock;
                    if($item['quantity'] > $product_existing_stock){
                        return response()->json(['status'=>false, 'message' => 'Requested quantity is larger than our stock'],400);
                    }
                    $order['buyer_id'] = $item->buyer_id;
                    $order['influencer_id'] = $item->influencer_id;
                    $order['address_id'] = $address_id;
                    $product_price = InfluencerShopProducts::where('id',$item->product_id)->value('product_price');
                    $product_price = $product_price * $item->quantity;
                    $order['order_amount'] += $product_price;
                    $order['order_status'] = 0;

                }
                 /** Payment **/
                 $mpesaObj = new MpesaController;
                 $tansaction_details = $mpesaObj->customerMpesaSTKPush($order['order_amount'],$data['phone_number']);
                 $transactionResponse = json_decode($tansaction_details);
                 if(isset($transactionResponse->errorMessage)){
                     $error_message = array('status'=>false,'type'=>'transaction','message'=>$transactionResponse->errorMessage);
                     return response()->json($error_message,400);
		        }

                //Order Create
                $order_data = Order::create($order);
                //Transaction Creat
		         $transaction_details['user_id'] = $user->id;
                 $transaction_details['request_type'] = 2;// for buyer purchase
                 $transaction_details['request_id'] = $transactionResponse->MerchantRequestID;
                 $transaction_details['phone_number'] = $data['phone_number'];
                 $transaction_details['transaction_id'] = $transactionResponse->MerchantRequestID;
                 $transaction_details['transaction_amount'] = $order['order_amount'];
                 $transaction_details['transaction_details'] = $tansaction_details;
                 $transaction_details['order_id'] = $order_data->id;
                 Transaction::create($transaction_details);

                 /** End  **/
                
                $added_order = Order::find($order_data->id);
                $added_order->payment_status = 0;
                $added_order->transaction_details = $tansaction_details;
                $added_order->save();
                //update product stock
                // foreach($product as $val){
                //     $update_stock = Product::find($val['product_id']);
                //     $remaining_stock = $val['product_existing_stock'] - $val['quantity'];
                //     $update_stock->stock = $remaining_stock;
                //     $update_stock->save();
                // }
                $success_message = array('status'=>true, 'message'=>"Order placed successfully","tansaction_details"=>$transactionResponse);
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function orderListing()
    {
        $user = Auth::user();
        if($user){
            try{
    
                $data = Order::where('buyer_id',$user->id)->orderBy('id','desc')->get();
                if($data){
                    $success_message = array('status'=>true, 'message'=>"Orders listed",'data' => $data);
                    return response()->json($success_message,200);
                }else{
                    return response()->json(['status' => false, 'message' => 'Data not found'],400);
                }
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function orderDetails($order_id)
    {
        $user = Auth::user();
        // dd($user);
        if($user){
            try{
    
                $data = OrderDetails::where('order_id',$order_id)->get();
                $orderD = Order::where('id',$order_id)->first();
                $influencer_id = $orderD->influencer_id;
                if($data){
                    foreach($data as $val){
                        $product_id = $val->product_id;
                        $product_detail = InfluencerShopProducts::where('id',$product_id)->select('id','product_title','product_id')->first();
                        $product_name = $product_detail->product_title;
                        $shop_product_id =  $product_detail->id;
                        $product_image = InfluencerShopProductImages::where('shop_product_id',$shop_product_id)->where('is_cover_pic',1)->value('image_path');
                        $val->product_title = $product_name;
                        $val->product_image = $product_image;
                        $val->business_product_id = $product_detail->product_id;
                        $ratings_and_reviews = ProductReviews::where('buyer_id',$user->id)->where('product_id',$product_detail->product_id)->first();
                        $val->ratings_and_reviews = $ratings_and_reviews;
                    }

                    $success_message = array('status'=>true, 'message'=>"Orders listed",'data' => $data,'orderDetails'=>$orderD);
                    return response()->json($success_message,200);
                }else{
                    return response()->json(['status' => false, 'message' => 'Data not found'],400);
                }
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }


    function checkPaymentStatus(Request $request){
        ini_set('max_execution_time', -1);
        $user = Auth::user();
        if($user){
            $rules = [
                "merchant_id" => "required|exists:transactions,transaction_id",
            ];
            $validator = \Validator::make($request->all(),$rules);
            if($validator->fails()){
                $keys = array_keys($validator->getMessageBag()->toArray());
                $response['errors'] = $validator->getMessageBag()->toArray();
                $response['errors_keys'] = $keys;
                return Response()->json($response, 400);
            }
            $transaction = Transaction::where('transaction_id' , $request->merchant_id)->first();
	    $transaction_details = json_decode($transaction->transaction_details , true);
	   
	    $status = $transaction->status;
            if($status==1){
                $order_data = Order::where('id' , $transaction->order_id)->first();
                $order_id = $order_data->id;
                Order::where('id' , $order_id)->update(['payment_status'=>'1']);
                $products = ShoppingCart::where('buyer_id',$user->id)->get();
                $order_details = [];
                foreach($products as $key => $value){
                    $product_id = InfluencerShopProducts::where('id',$value['product_id'])->value('product_id');
                    $product_existing_stock = Product::where('id',$product_id)->value('stock');

                    $product_price = InfluencerShopProducts::where('id',$value->product_id)->value('product_price');
                    $order_details[$key]['order_id'] = $order_id;
                    $order_details[$key]['product_id'] = $product_id;
                    $order_details[$key]['influencer_id'] = $value->influencer_id;
                    $order_details[$key]['business_id'] = $value->business_id;
                    $order_details[$key]['amount'] = $product_price;
                    $order_details[$key]['quantity'] = $value->quantity;
                    $order_details[$key]['size'] = $value->size;
                    $order_details[$key]['color'] = $value->color;
                    $order_details[$key]['status'] = 0;
                    $order_details[$key]['product_existing_stock'] = $product_existing_stock;
                }
                
                foreach($order_details as $val){
                    $new_order = OrderDetails::create($val);
                    // notification start
                    $notification['user_id'] = $val['business_id'];
                    $notification['request_type'] = 1;
                    $notification['request_id']  = $order_id;
                    $notification['description'] = 'You recieved a new order';
                    Notification::create($notification);
                    //notification end
                }
                //Product Quantity Update
                foreach($order_details as $val){
                    $op = InfluencerShopProducts::where('id' , $val['product_id'])->first();
                    $update_stock = Product::find($op->product_id);
                    $remaining_stock = $val['product_existing_stock'] - $val['quantity'];
                    $update_stock->stock = $remaining_stock;
                    //$update_stock->total_sale = $op->total_sale + $val['quantity'];
                    $update_stock->save();
                }

                //Add Influencer Transaction 
                $ith = new InfluencerTransactionsHistory;
                $ith->user_id = $order_data->influencer_id;
                $ith->transaction_amount = $order_data->order_amount;
                $ith->request_type = 1;
                $ith->request_id = $order_data->id;
                $ith->save();

                //Add Business Transaction
                $bth = new BusinessTransactionsHistory;
                $bth->user_id = $products['0']['business_id'];
                $bth->transaction_amount = $order_data->order_amount;
                $bth->tansaction_details = $transaction->transaction_details;
                $bth->request_type = 2;
                $bth->request_id = $order_data->id;
                $bth->save();


                ShoppingCart::where('buyer_id',$user->id)->delete();
                $push = new PushNotificationController;
                $push->pushNotificationHandler($order_data,1);
            }
            if($status==0 || $status==1){
                $response['return'] = true;
                $response['message'] = 'Transaction status fetched successfully';
                $response['transaction_status']=$status;
                $response['merchant_id'] = $request->merchant_id;
                return response()->json($response,200);	 
            }else{
                Transaction::where('transaction_id' , $request->merchant_id)->delete();
                Order::where('id' , $transaction->order_id)->delete();
                $response['return'] = true;
                $response['message'] = 'Transaction Failed';
                $response['transaction_status']=$status;
                return response()->json($response,200);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
       
    }

}
