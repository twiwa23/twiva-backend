<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCompDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comp_details', function (Blueprint $table) {
            //
            $table->integer('country')->nullable()->after('logo');
            $table->integer('state')->nullable()->after('country');
            $table->integer('city')->nullable()->after('state');
            $table->string('area')->nullable()->after('city');
            $table->string('building')->nullable()->after('area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comp_details', function (Blueprint $table) {
            //
        });
    }
}
