<?php


namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\UserCompany;
use App\Models\Activity;
use App\Models\AccountUser;
use App\Models\User;
use Jenssegers\Agent\Agent;
use Session;  
use DB;   


class AdminHelper
{
    public static function adminListUser($id) {

       $user = Auth::user()->id;
       $userList = User::select('id','name','surname','email')->where([['type', '=', 'admin'],['id', '<>' , $user]])->get();
        
        $result = array();
        if (!empty($user == $id)) {
            $result[] = array(
                'user_id' => $id,
                'name' =>  "Myself",
            );
        }
        foreach ($userList as $key => $value) {
        $result[] = array(
            'user_id' => $value->id,
            'name' =>  empty($value->name) ? $value->surname : (empty($value->surname) ? $value->name : $value->name .' '. $value->surname),
        );
        }
        return $result;
    }
}
