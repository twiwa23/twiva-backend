<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSpecifications extends Model
{
	//
	protected $table = 'product_specifications';

	  protected $id = 'id';

    protected $fillable = ['product_id','title'];

}
