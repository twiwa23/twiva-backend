<?php
namespace App\Http\Controllers\Api\v1\Buyer;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\ProductReviews;
use App\Models\Product;
use Validator,Auth;


class ProductReviewsController extends ApiController 
{
    public function addReview(Request $request)
    {
        $user = Auth::user();
        if($user){
            try{
                $data = $request->all();
                $validator = Validator::make($data,[
                    'product_id'  => 'required',
                    'rating'  => 'required | integer',
                    'review'  => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                $data['buyer_id'] = $user->id;
                ProductReviews::create($data);
                $rating = ProductReviews::where('product_id',$data['product_id'])->pluck('rating')->avg();
                Product::where('id',$data['product_id'])->update(['rating' => $rating]);
                $success_message = array('status'=>true, 'message'=>"Review added successfully");
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function editReview($review_id)
    {
        $user = Auth::user();
        if($user){ 
            try{
                $data = ProductReviews::find($review_id);
                if($data){
                    $success_message = array('status'=>true, 'message'=>"Review fetched",'data' => $data);
                    return response()->json($success_message,200);
                }else{
                    return response()->json(['status' => false, 'message' => 'Data not found'],400);
                }
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function updateReview(Request $request, $review_id)
    {
        $user = Auth::user();
        if($user){ 
            try{
                $input = $request->all();
                $validator = Validator::make($input, [
                    'product_id'  => 'required',
                    'rating'  => 'required | integer',
                    'review'  => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status'=>false,'error'=>$validator->messages()->first()], 400);
                }
                $input['buyer_id'] = $user->id;
                ProductReviews::where('id',$review_id)->update($input);
                $rating = ProductReviews::where('product_id',$input['product_id'])->pluck('rating')->avg();
                Product::where('id',$input['product_id'])->update(['rating' => $rating]);
                $success_message = array('status'=>true, 'message'=>"Review updated successful");
                return response()->json($success_message,200);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

    public function fetchReviews(Request $request, $business_product_id)
    {
        $user = Auth::user();
        if($user){ 
            try{
                $input = $request->all();               
                $validator = Validator::make($input,[
                    'limit' => 'numeric | nullable',
                    'page'  => 'numeric | nullable',
                ]);
                if($validator->fails()) {
                    $ret = array('status'=>false, 'message'=> $validator->messages()->first());
                    return response()->json($ret,400);
                }
                $limit = $request->limit ? $request->limit : 10;
                $reviews = ProductReviews::where('buyer_id',$user->id)->where('product_id',$business_product_id)->first();

                return response()->json(array('status'=>true,'message'=>"Reviews fetched",'data'=>$reviews) ,200,[],JSON_NUMERIC_CHECK);
            }
            catch( \Exception $e){
                return response()->json(['status'=>false, 'error' => $e->getMessage()],500);
            }
        }else{
            $error_message = array('message'=>"Unauthorized",'status'=>false);
            return response()->json($error_message,401);
        }
    }

}