<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Colors;
use App\Models\Sizes;
use App\Models\Categories;
use App\Models\SubCategories;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id','comp_id','name','title','main_image','price','category','sub_category','stock','size','color','sku','rating','total_sale','description','status','specification','created_at','updated_at'];
    protected $hidden = ['created_at','updated_at'];

    protected $appends = [
            'color_details','size_details','category_details','subcategory_details'
    ];

     /**
     * Get product images
     *
     * @var array
     */

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function userSubscription() {
        return $this->hasOne('App\Models\UserSubscription', 'user_id', 'user_id');
    }

    public function productImages() {
        return $this->hasMany('App\Models\ProductImage', 'product_id', 'id')->where('image','!=',null);
    }

    public function productReviewsAndRatings() {
        return $this->hasMany('App\Models\ProductReviews', 'product_id', 'id');
    }

    /*public function productStocks(){
        return $this->hasMany('App\Models\ProductStock', 'product_id', 'id');
    } */

    public function getColorDetailsAttribute(){
        if($this->color){
            $color = explode(',',$this->color);
            return Colors::whereIn( 'id',$color )->select('id', 'name', 'hexaval')->get();
        }else{
            return array();
        }
    }

    public function getSizeDetailsAttribute(){
        if($this->size){
            $size = explode(',',$this->size);
            return Sizes::whereIn('id',$size)->select('id', 'name')->get();
        }else{
            return array();
        }
    }

    public function getCategoryDetailsAttribute(){
        return Categories::where('id',$this->category)->first();
    }

    public function getSubcategoryDetailsAttribute(){
        if($this->sub_category){
            $sub_cat = explode(',',$this->sub_category);
            return SubCategories::whereIn('id',$sub_cat)->get();
        }else{
            return array();
        }
    }

}
