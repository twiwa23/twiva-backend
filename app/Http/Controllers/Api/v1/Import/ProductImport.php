<?php

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Models\Product; 

//  maatwebsite excel import

class ProductImport implements ToCollection, WithStartRow
{
    /**
     * @param array $row
     *
     * @return Customer|null
     */
    public function collection(Collection $rows)
    {   
        
        foreach ($rows as $row) 
        {
            
            $imageFrom = public_path().'/importProduct/images/'.$row[9];
            if(file_exists($imageFrom)){
            $newImageName = time().$row[9];
            $imageTo = public_path().'/storage/Product/'.$newImageName;
            rename($imageFrom,$imageTo);
            }else{
                $newImageName = Null;
            }
            $customerData[] = array(
                'user_id'   =>  Auth::id(),
                'name'     => $row[0],
                'sku'    => $row[1], 
                'title' => $row[2],
                'category' => $row[3],
                'price' => $row[4],
                'stock' => $row[5],
                'size' => $row[6],
                'color' => $row[7],
                'description' => $row[8],
                'main_image' => $newImageName,
                'rating' => $row[10],
                'status' => $row[11],
                'created_by' => Auth::id(),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            
        }
        $prod = Product::insert($customerData);
    }
    public function startRow(): int
    {
        return 2;
    }
}