<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $table = 'otp';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['type', 'otp', 'email'];
    protected $hidden = ['created_at','updated_at']; 
}
