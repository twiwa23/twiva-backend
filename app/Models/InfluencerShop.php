<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\InfluencerShopProducts;
use App\Models\InfluencerShopProductImages;
use App\Models\Colors;
use App\Models\Sizes;
use App\Models\Categories;

class InfluencerShop extends Model
{
    use SoftDeletes;

    protected $table = "influencer_shop";
    protected $dates = ['deleted_at','created_at','updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at','updated_at','deleted_at'];

    /**
     * The attributes that should be mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','shop_cover_image_path','shop_logo_path','shop_name','shop_description','shop_font','shop_color','is_active'];


    /**
     * Get products
     *
     * @var array
     */
    public function products() {
        return $this->hasMany('App\Models\InfluencerShopProducts', 'shop_id', 'id');
    }

    public function influencerProducts(){
        return $this->hasMany('App\Models\InfluencerShopProducts', 'shop_id', 'id');
    }

    /**
     * Get product images
     *
     * @var array
     */
    public function productImages() {
        return $this->hasManyThrough(
            'App\Models\InfluencerShopProductImages',
            'App\Models\InfluencerShopProducts',
            'shop_id', // Foreign key on InfluencerShopProducts table...
            'shop_product_id', // Foreign key on InfluencerShopProductImages table...
            'id', // Local key on influencer_shop table...
            'id' // Local key on influencer_shop_product table...
        );
    }

}
