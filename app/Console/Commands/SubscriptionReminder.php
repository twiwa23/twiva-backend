<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\PushNotificationController;
use App\Models\Company;

class SubscriptionReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify users for subscription expiry';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = \Carbon\Carbon::now();
        $user_subscription = Company::whereDate('last_cron_run_date','!=',$today)->whereHas('userSubscriptions',function ($query) use($today){
            $query->whereDate('end_date',$today->addDays(7));
        })->with('userSubscriptions')->get();

        foreach($user_subscription as $value){
            $push = new PushNotificationController;
            $data['user_id'] = $value->user_id;
            $data['subscription_id']  = $value->userSubscriptions->subscription_id;
            $push->pushNotificationHandler($data,3);
        }
        $this->info('Reminder Notification has been Sent');
    }
}
