<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Order extends Model
{
     use SoftDeletes;

    protected $table = "orders";
    protected $dates = ['deleted_at','created_at','updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = ['created_at','updated_at','deleted_at'];

    /**
     * The attributes that should be mass assignable.
     *
     * @var array
     */
    protected $fillable = ['buyer_id','influencer_id','address_id','order_status','order_amount','payment_status','transaction_details'];

    public function orderList(){
        return $this->hasMany('App\Models\OrderDetails', 'order_id', 'id');
    }

    public function buyerInfo() {
        return $this->hasOne('App\Models\BuyerDetails', 'buyer_id', 'buyer_id');
    }

}