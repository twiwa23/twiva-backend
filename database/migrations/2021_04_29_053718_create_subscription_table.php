<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription', function (Blueprint $table) {
            $table->id();
            $table->string('subscription_title');
            $table->integer('type')->comment('1 for free, 2 for monthly, 3 for annually');
            $table->float('price',8,2);
            $table->text('note')->nullable();
            $table->integer('is_active')->comment('0 for inactive , 1 for active')->default(0);
            $table->integer('no_of_days')->nullable();
            $table->integer('max_products')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription');
    }
}
