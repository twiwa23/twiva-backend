<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfluencerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_details', function (Blueprint $table) {

            //user id
            $table->unsignedBigInteger('influencer_id')->comment('Influencers id as foreign key on Users table');
            $table->foreign('influencer_id')->references('id')->on('users');

            //user details
            $table->string('name')->comment('Influencer full name');
            $table->date('dob')->comment('Influencers date of birth');
            $table->smallInteger('gender')->comment('Influencers gender, can be 1=>Male, 2=>Female, 3=>Other');
            $table->string('description')->comment('Influencers self description text');
            $table->string('interests')->comment('Influencers selected interests');

            //timestamps
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_details');
    }
}
