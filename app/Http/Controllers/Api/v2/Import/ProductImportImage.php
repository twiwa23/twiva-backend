<?php
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Models\ProductImage; 

// import product image
class ProductImportImage implements ToCollection, WithStartRow
{
    /**
     * @param array $row
     *
     * @return Customer|null
     */
    public function collection(Collection $rows)
    {   
        
        foreach ($rows as $row) 
        {
            
            $imageFrom = public_path().'/importProduct/images/productImage/'.$row[3];
            if(file_exists($imageFrom)){
            $newImageName = time().$row[3];
            $imageTo = public_path().'/storage/Product/productImage/'.$newImageName;
            rename($imageFrom,$imageTo);
            }else{
                $newImageName = Null;
            }
            $product = Product::where('sku',$row[1])->first();
            if($product && $product->id){
            $productId = $product->id;
            $customerData[] = array(
                'user_id' => Auth::id(),
                'product_id'   =>  $productId,
                'name'     => $row[0],
                'sku'    => $row[1], 
                'title' => $row[2],
                'image' => $row[3],
                'created_by' => Auth::id(),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            }else{
                $productId = Null;
            }
            
        }
        $prod = ProductImage::insert($customerData);
    }
    public function startRow(): int
    {
        return 2;
    }
}
