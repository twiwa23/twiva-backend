<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{    
   // use SoftDeletes;

    protected $table = 'comp_details';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id','comp_email','busi_name','contact_name','busi_number','country_code','last_cron_run_date','description','logo','country','state','city','area','building','pincode ','created_at','updated_at'];

    public function userSubscriptions() {
        return $this->hasOne('App\Models\UserSubscription', 'user_id', 'user_id');
    }
   
}
