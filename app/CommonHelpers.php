<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\User;
use Auth,File,URL,Exception,Log;

class CommonHelpers
{   
    /**
     * 
     * Upload image
     *
    */
    public function uploadProfileImage( $folder, $file )
    {
        try{
            $file = $request->file('image');
            $fileext = $file->getClientOriginalExtension();
            $name = uniqid().'-'.time().'.'.$fileext;
            $filePath = "public/".$folder."/" . $name;
            $image_url = Storage::disk('local')->put($filePath, file_get_contents($file),'public');
            $image_path_for_db = Storage::url($folder.'/'.$name);
            return ['image_name'=>$name,'image_url'=>$image_path_for_db];
        }
        catch(Exception $e){
            Log::error($e);
            return $e;
        }    
    }
}