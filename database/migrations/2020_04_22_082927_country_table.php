<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country', function (Blueprint $table) {
            $table->bigIncrements('country_id');
            $table->char('country_code', 100)->nullable();
            $table->string('isd_code')->nullable();
            $table->string('country_name')->nullable();
            $table->decimal('vat', 10, 2)->nullable();
            $table->string('currency')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->string('currency_value_per_credit')->nullable();
            $table->string('credit_daily_limit')->nullable();
            $table->string('currency_daily_limit')->nullable();
            $table->string('payout_limit')->nullable();        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country');
    }
}
