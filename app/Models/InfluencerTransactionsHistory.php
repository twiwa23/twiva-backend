<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfluencerTransactionsHistory extends Model
{

    protected $table = 'influencer_transactions_history';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'transaction_amount', 'request_type', 'request_id'];
}
