<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyer_details', function (Blueprint $table) {
            $table->unsignedBigInteger('buyer_id')->comment('Buyer id as foreign key on Users table');
            $table->foreign('buyer_id')->references('id')->on('users');
            $table->string('name')->comment('Buyer full name');
            $table->date('dob')->nullable()->comment('Influencers date of birth');
            $table->smallInteger('gender')->nullable()->comment('Buyer gender, can be 1=>Male, 2=>Female, 3=>Other');
            $table->string('description')->nullable()->comment('Buyer self description text');
            $table->string('profile_image')->nullable();
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyer_details');
    }
}
