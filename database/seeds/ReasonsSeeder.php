<?php

use Illuminate\Database\Seeder;

class ReasonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reasons')->insert([

            [
                'reason' => 'Reason 1'
            ],
            [
                'reason' => 'Reason 2'
            ],
            [
                'reason' => 'Reason 3'
            ]

        ]);
    }
}
