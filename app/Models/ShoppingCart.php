<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Colors;
use App\Models\Sizes;
use App\Models\InfluencerShopProducts;

class ShoppingCart extends Model
{
    //
    protected $table = 'shopping_cart';
    protected $id = 'id';
    protected $primaryKey = 'id';
    protected $fillable = ['buyer_id','influencer_id','business_id','product_id','quantity','size','color'];

    protected $appends = [
        'product_price'
    ];

    public function productSize() {
        return $this->belongsTo('App\Models\Sizes', 'size', 'id');
    }

    public function productColor() {
        return $this->belongsTo('App\Models\Colors', 'color', 'id');
    }

    public function getProductPriceAttribute(){
        return InfluencerShopProducts::where('id',$this->product_id)->value('product_price');
    }
}
