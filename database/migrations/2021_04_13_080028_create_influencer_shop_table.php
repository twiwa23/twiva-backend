<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfluencerShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_shop', function (Blueprint $table) {

            $table->id();

            $table->unsignedBigInteger('user_id')->comment('Influencer\'s user id');
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->string('shop_cover_image_path')->comment('Influencer\'s shop cover image')->nullable();
            $table->string('shop_logo_path')->comment('Influencer\'s shop logo image')->nullable();
            $table->string('shop_name')->comment('Influencer\'s shop name');
            $table->string('shop_description')->comment('Influencer\'s shop description');
            $table->string('shop_font')->comment('Influencer\'s shop font name');
            $table->string('shop_color')->comment('Influencer\'s shop color name');
            $table->boolean('is_active')->default(1)->comment('Shop is active or not');

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_shop');
    }
}
